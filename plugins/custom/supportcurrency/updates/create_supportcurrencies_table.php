<?php namespace Custom\SupportCurrency\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateSupportcurrenciesTable extends Migration
{
    public function up()
    {
        Schema::create('custom_supportcurrency_supportcurrencies', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id')->nullable();
            $table->text('Currency');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('custom_supportcurrency_supportcurrencies');
    }
}
