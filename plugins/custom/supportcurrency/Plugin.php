<?php namespace Custom\SupportCurrency;

use Backend;
use System\Classes\PluginBase;

/**
 * SupportCurrency Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'SupportCurrency',
            'description' => 'No description provided yet...',
            'author'      => 'Custom',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Custom\SupportCurrency\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'custom.supportcurrency.some_permission' => [
                'tab' => 'SupportCurrency',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        
        return []; // Remove this line to activate
        return [
            'supportcurrency' => [
                'label'       => 'SupportCurrency',
                'url'         => Backend::url('custom/supportcurrency/Supportcurrencies'),
                'icon'        => 'icon-leaf',
                'permissions' => ['custom.supportcurrency.*'],
                'order'       => 500,
            ],
        ];
    }
}
