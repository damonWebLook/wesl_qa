<?php namespace Custom\Video\Models;

use Model;
use Custom\Agents\Models\Agent;
use Custom\Video\Models\VideoCategory;
use Custom\Video\Models\AdPackage;
use Backend\Models\User;


/**
 * video Model
 */
class Video extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'custom_video_videos';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Validation rules for attributes
     */
    public $rules = [];

    /**
     * @var array Attributes to be cast to native types
     */
    protected $casts = [];

    /**
     * @var array Attributes to be cast to JSON
     */
    protected $jsonable = [];

    /**
     * @var array Attributes to be appended to the API representation of the model (ex. toArray())
     */
    protected $appends = [];

    /**
     * @var array Attributes to be removed from the API representation of the model (ex. toArray())
     */
    protected $hidden = [];

    /**
     * @var array Attributes to be cast to Argon (Carbon) instances
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'adPoint' =>[
            'Custom\Video\Models\VideoPoint',
            'table' => 'custom_video_video_points',
            'key' => 'video_id',
        ],
        'advertisorData' => [
            
        ]
    ];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = ['video' => 'System\Models\File'];
    public $attachMany = [];

    public function getAdvertiserIdOptions(){
        $advertisers = User::select(['login','id'])->where('role_id', 5)->get()->toArray();
        $arr = array();
        foreach( $advertisers as $key => $value){
            $arr[$value['id']] = $value['login'];
        }
        return $arr;
    }
    public function getCategoryIdOptions(){
        $category = VideoCategory::all()->lists('name', 'id');
        return $category;
    }
    public function getPackageIdOptions(){
        $packages = AdPackage::all()->lists('name', 'id');
        return $packages;
    }

}
