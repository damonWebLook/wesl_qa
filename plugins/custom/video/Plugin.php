<?php namespace Custom\Video;

use Backend;
use System\Classes\PluginBase;
use BackendAuth;

/**
 * video Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'video',
            'description' => 'No description provided yet...',
            'author'      => 'custom',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Custom\Video\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
       

        return [
            
            'custom.video.videos.some_permission' => [
                'tab' => 'Videos',
                'label' => 'Some permission',
                'role' => ['developer']
    
            ],
            'custom.video.videopoints.view' => [
                'tab' => 'Videos',
                'label' => 'View Ad Tracking record for Advertisers',
                'role' => ['adv']
    
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        
       $id = BackendAuth::getUser()->id;
    //    return [];
        return [
            // 'videopoint' => [
            //     'label'       => 'Ad Tracking',
            //     'url'         => Backend::url('custom/video/videopoints'),
            //     'icon'        => 'icon-file-video-o',
            //     'permissions' => ['custom.video.videopoints.view'],
            //     'order'       => 500,
            // ],
            'myvideopoint' => [
                'label'       => 'My Ad',
                'url'         => url('/myad/' . BackendAuth::getUser()->id),
                'icon'        => 'icon-film',
                'permissions' => ['custom.video.videopoints.view'],
                'order'       => 500,
            ],
        ];
    }
}
