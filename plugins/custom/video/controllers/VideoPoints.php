<?php namespace Custom\Video\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Custom\Video\Models\VideoPoint;
use Custom\User\Models\User;

/**
 * Video Points Back-end Controller
 */
class VideoPoints extends Controller
{
    /**
     * @var array Behaviors that are implemented by this controller.
     */
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    /**
     * @var string Configuration file for the `FormController` behavior.
     */
    public $formConfig = 'config_form.yaml';

    /**
     * @var string Configuration file for the `ListController` behavior.
     */
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Custom.Video', 'video', 'videopoints');
    }
    /**
     * Get the Id of the Ad selected from Frontend and return data
     * UserName, IP, SeenAt time
     */
    public static function getAdData($adId){
        $data = VideoPoint::where('video_id', $adId)->get();
        $arr = array();
        $i = 1;
        foreach ($data as $d){
            $user = User::where('id', $d->user_id)->get()->toArray();
            $userName = $user[0]['First_name'] .' ' . $user[0]['Last_name'];
        
            $arr[$i] = [
                'no' => $i,
                'userName' => $userName,
                'ip' => $d->ip,
                'seenAt' => $d->seen_at,
            ];
            $i++;
        }
        return($arr);
        

    }
}
