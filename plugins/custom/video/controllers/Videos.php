<?php namespace Custom\Video\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use BackendAuth;
use Session;
use Backend\Models\User as BackendUser;
use Custom\Video\Models\Video;
use Custom\Agents\Models\Agent;
use Custom\Video\Models\VideoCategory;
use Custom\Video\Models\VideoPoint;
use Custom\Video\Models\AdPackage;
use Custom\User\Models\User;
use DB;
use Custom\Video\Controllers\VideoCategories;
/**
 * Videos Back-end Controller
 */
class Videos extends Controller
{
    /**
     * @var array Behaviors that are implemented by this controller.
     */
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    /**
     * @var string Configuration file for the `FormController` behavior.
     */
    public $formConfig = 'config_form.yaml';

    /**
     * @var string Configuration file for the `ListController` behavior.
     */
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Custom.Video', 'video', 'videos');
    }
    
    public function getAdvertiserName($id){
        $advertiserId = Video::find($id)->advertiser_id;
        $advertiserName = BackendUser::find($advertiserId)->first_name;
        $advertiserName .= " " . BackendUser::find($advertiserId)->last_name;
        return $advertiserName;
    }

    public function getCategoryName($id){
        $categoryId = Video::find($id)->category_id;
        $categoryName = VideoCategory::find($categoryId)->name;
        return $categoryName;
    }
    public function getPackageName($id){
        $packageId = Video::find($id)->package_id;
        $packageName = AdPackage::find($packageId)->name;
        return $packageName;
    }
    
    /**
     * Generate Random Ad link to be rendered on Front End
     */


    public static function generateAd(){
        $arr = Video::all()->lists('id');
        $index = rand(1, count($arr));
        $indexValue = in_array($index, $arr);
        if ($indexValue === true && $index < count($arr)){
            $id = $arr[$index];
        }else{
            $id = 1;
        }
        $video = Video::find($id)->video_path;
        $point = Video::find($id)->points;
        $time = Video::find($id)->time;
        
        return [
            'id' => $id,
            'path' => env('AD_VIDEO_PATH').$video,
            'point' => $point,
            'time' => $time,
        ];
        
        
    }
    /**
     * This gets the VideoId, VideoPoints, UserID and saves it to the User's Points
     * TODO:: Get USER ID
     */
    public static function savePoint($data){
        $ip = $_SERVER['REMOTE_ADDR'];
        $userId = (int)session()->get('user_id');
        $videoId = $_POST['id'];
        $videoId = (int)trim($videoId, "id=" );
        $point = $_POST['point'];
        $point = (int)trim($point, "finalPoints=");
        $isWatched = (new self)->checkVideoPointStatus($videoId, $userId, $ip);
        if( $isWatched === true){
            //save to Users>Points
            $user = User::find($userId);
            $user->point =  $user->point + $point;
            $user->save();
        }else{
            trigger_error("You have already watched the video");
        }
       
    } 
    /**
     * This checks the UserId and VIdeo id in VideoPoint Table 
     */
    public function checkVideoPointStatus($videoId, $userID, $ip){
        $result = VideoPoint::where('user_id', $userID)->where('video_id', $videoId)->get()->toArray();
        if(empty($result)){
            // $videPoint = new VideoPoint;
            // $videPoint->video_id = $videoId;
            // $videoPoint->user_id = $userID;
            // $videoPoint->ip = $ip;
            // $videoPoint->save();
            DB::table('custom_video_video_points')->insert([
                'user_id' => $userID,
                'video_id' => $videoId,
                'ip' => $ip,
                'seen_at' => date("Y-m-d h:i:sa"),
            ]);
            return true;
        }else{
            return false;
        }

    }
    //TODO
    /**
     * For Video Page filers
     */
    public static function getAllVideos(){
        return Video::all();
        // dump(Video::all());
    }

    public static function getVideoByCategory($id){
        return Video::where('category_id',$id)->get();
        // dump (Video::where('category_id',$id)->get());
    }

    /**
     * For Ad Data Page
     */

     public static function getNameByAdvertiser($data){
         dump($data);
     }
}
