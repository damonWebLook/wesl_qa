<?php namespace Custom\Video\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Custom\Video\Models\Video;
use Custom\Video\Models\VideoCategory;
use Backend\Models\User;

/**
 * Video Categories Back-end Controller
 */
class VideoCategories extends Controller
{
    /**
     * @var array Behaviors that are implemented by this controller.
     */
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    /**
     * @var string Configuration file for the `FormController` behavior.
     */
    public $formConfig = 'config_form.yaml';

    /**
     * @var string Configuration file for the `ListController` behavior.
     */
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Custom.Video', 'video', 'videocategories');
    }

    public static function getCategory(){
        return ( VideoCategory::all()->lists('name', 'id'));
    }

}
