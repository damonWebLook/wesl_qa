<?php namespace Custom\Video\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateVideoPointsTable extends Migration
{
    public function up()
    {
        Schema::create('custom_video_video_points', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('video_id')->unsigned();
            $table->foreign('video_id')->references('id')->on('custom_video_videos');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('custom_user_users');
            $table->ipAddress('ip');
            $table->text('seen_at');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('custom_video_video_points');
    }
}
