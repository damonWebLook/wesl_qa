<?php namespace Custom\Video\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateAdPackagesTable extends Migration
{
    public function up()
    {
        Schema::create('custom_video_ad_packages', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 255);
            $table->integer('quota');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('custom_video_ad_packages');
    }
}
