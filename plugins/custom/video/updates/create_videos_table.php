<?php namespace Custom\Video\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateVideosTable extends Migration
{
    public function up()
    {
        Schema::create('custom_video_videos', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('advertiser_id')->nullable()->unsigned();
            $table->foreign('advertiser_id')->references('id')->on('backend_users');
            $table->integer('category_id')->nullable()->unsigned();
            $table->foreign('category_id')->references('id')->on('custom_video_video_categories');
            $table->integer('package_id')->nullable()->unsigned();
            $table->foreign('package_id')->references('id')->on('custom_video_ad_packages');
            $table->text('name');
            $table->text('video_path');
            $table->integer('points');
            $table->text('time');
            $table->boolean('is_deleted')->default(0);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('custom_video_videos');
    }
}
