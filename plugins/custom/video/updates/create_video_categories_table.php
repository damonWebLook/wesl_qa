<?php namespace Custom\Video\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateVideoCategoriesTable extends Migration
{
    public function up()
    {
        Schema::create('custom_video_video_categories', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name',255);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('custom_video_video_categories');
    }
}
