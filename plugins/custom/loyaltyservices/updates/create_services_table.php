<?php namespace Custom\Loyaltyservices\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateServicesTable extends Migration
{
    public function up()
    {
        Schema::create('custom_loyaltyservices_services', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('agent_id')->nullable();
            $table->text('service_name')->nullable();
            $table->text('service_descripiton')->nullable();
            $table->text('service_points')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('custom_loyaltyservices_services');
    }
}
