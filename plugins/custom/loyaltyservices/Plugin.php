<?php namespace Custom\Loyaltyservices;

use Backend;
use System\Classes\PluginBase;

/**
 * loyaltyservices Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        
        return [
            'name'        => 'loyaltyservices',
            'description' => 'No description provided yet...',
            'author'      => 'custom',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Custom\Loyaltyservices\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        

        return [
            'custom.loyaltyservices.some_permission' => [
                'tab' => 'loyaltyservices',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [];

        return [
            'loyaltyservices' => [
                'label'       => 'Loyalty Services',
                'url'         => Backend::url('custom/loyaltyservices/Services'),
                'icon'        => 'icon-archive',
                'permissions' => ['custom.loyaltyservices.*'],
                'order'       => 500,
            ],
        ];
    }
}
