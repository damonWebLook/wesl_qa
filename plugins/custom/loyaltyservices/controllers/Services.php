<?php namespace Custom\Loyaltyservices\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Backend\Models\User;
use Custom\Loyaltyservices\Models\Service;
use Mail;
use BackendAuth;
use Session;

/**
 * Services Back-end Controller
 */
class Services extends Controller
{
    /**
     * @var array Behaviors that are implemented by this controller.
     */
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    /**
     * @var string Configuration file for the `FormController` behavior.
     */
    public $formConfig = 'config_form.yaml';

    /**
     * @var string Configuration file for the `ListController` behavior.
     */
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Custom.Loyaltyservices', 'loyaltyservices', 'services');
    }
    //Temp Solution
       public static function getStatusByCategory($id){

        return Service::where('category',$id)->get();

    }
    

}
