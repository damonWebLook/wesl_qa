<?php namespace Custom\Coupons;

use Backend;
use System\Classes\PluginBase;

/**
 * Coupons Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Coupons',
            'description' => 'No description provided yet...',
            'author'      => 'custom',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Custom\Coupons\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
       

        return [
            'custom.coupons.some_permission' => [
                'tab' => 'Coupons',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'coupons' => [
                'label'       => 'Coupons',
                'url'         => Backend::url('custom/coupons/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['custom.coupons.*'],
                'order'       => 500,
            ],
        ];
    }

    public function registerSettings()
    {
    
       return [
           'coupons'  => [
             'label' => 'Coupon',
             'description' => 'Manage Coupon',
             'category' => 'Coupon',
             'icon' => 'icon-globe',
             'url' => Backend::url('custom/coupons/coupons'),
           
           
           
            ]

           
           
        ];
    } 
}
