<?php namespace Custom\Coupons\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use System\Classes\SettingsManager;


/**
 * Coupons Back-end Controller
 */
class Coupons extends Controller
{
    /**
     * @var array Behaviors that are implemented by this controller.
     */
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    /**
     * @var string Configuration file for the `FormController` behavior.
     */
    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    

    /**
     * @var string Configuration file for the `ListController` behavior.
     */
    

    public function __construct()
    {
        parent::__construct();
        
        BackendMenu::setContext('October.System', 'system', 'settings');
        SettingsManager::setContext('Custom.Coupons', 'coupons');
    }
}
