<?php namespace Custom\Survey\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Custom\Survey\Models\Survey;


/**
 * Surveys Back-end Controller
 */
class Surveys extends Controller
{
    /**
     * @var array Behaviors that are implemented by this controller.
     */
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    /**
     * @var string Configuration file for the `FormController` behavior.
     */
    public $formConfig = 'config_form.yaml';

    /**
     * @var string Configuration file for the `ListController` behavior.
     */
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Custom.Survey', 'survey', 'surveys');
    }

    public function onConfigsave()
    {
        echo('testmodel');

    }
    
    public static function getSurveyById($id)
    {
        // dd($id);
        $survey = Survey::find($id);
        
        $survey_arr = [];
        $survey_arr['survey'] = $survey;
       

        return $survey_arr;
    
    }

    public static function checkField($fieldName) {
        //dd($fieldName);
        // if(count($field) != 0) {
            switch ($fieldName) {
                case '1':
                case '6':
                case '12':
                    return 'text';
                    break;
                case '2':
                    return 'checkbox';
                    break;
                case '7':
                case '8':
                case '11':
                    return 'select';
                    break;
                case '3':
                    return 'date';
                    break;
                case '4':
                    return 'color';
                    break;
                case '5':
                    return 'time';
                    break;
                case '9':
                    return 'email';
                    break;
                case '10':
                    return 'number';
                    break;
                default:
                    return 'text';
                    break;
            }
        // }
        
    }

    public static function getsurveyfields($id) {
        $getSurvey = self::getSurveyById($id);
        $surveyArray = [];
        $fields = $getSurvey['survey']->Survey_field;
        foreach($fields as $key => $surveyfield) {
            //dd($surveyfield['Answer']);
            $fieldName = Surveys::checkField($surveyfield['Answer']);
            array_push($surveyArray, $fieldName);
        }
        return $surveyArray;
        //dd($surveyArray);
    }
    
    
    public static function getpoints()
    {
       $surveypoint = Survey::find('survey_points');
       
    
    
    }
    
    
    
    
    
    // public static function getsurveyfields($id)
    // {
    //     $surveyfield = Survey::find($id);
       
  
    //     // return $item;
  
        
    
    
    // }
    
    
}
