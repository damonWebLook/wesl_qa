<?php namespace Custom\Survey\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use BackendAuth;
use Session;
use Custom\Survey\Controllers\Surveys;
use Custom\Survey\Models\Survey;
use Custom\Survey\Models\Surveyshow;
use Custom\Survey\Controllers\Surveyshows;
use Custom\User\Controllers\Users;
use Flash;

/**
 * Surveyshows Back-end Controller
 */
class Surveyshows extends Controller
{
    /**
     * @var array Behaviors that are implemented by this controller.
     */
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    /**
     * @var string Configuration file for the `FormController` behavior.
     */
    public $formConfig = 'config_form.yaml';

    /**
     * @var string Configuration file for the `ListController` behavior.
     */
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Custom.Survey', 'survey', 'surveyshows');
    }
    
   
    public static function getSurveyviewById($id)
    {
        $survey = Surveyshow::find($id);
        $survey_arr = [];
        $survey_arr['surveyview'] = $survey;
        // dd($survey);
       

        return $survey_arr;
    
    }
    
    
    public function showanswers()
    {  
        $surveys = Surveyshow::all();
        $survey_arr =[];

        foreach($surveys as $key=>$survey){
            $surveydata = Survey::find($survey->survey_id);
            // dd($surveydata);
            $survey_arr = $surveydata->Survey_field;
            
        }
        return $survey_arr;
    
    
    }
    
    
    
    
    

    
   
    
    public function getupdate($id)
    {
        $agentprofile = Surveyshow::find($id);
    
        $this->vars['survey'] = Surveyshows::showanswers();
        $this->vars['data'] = $agentprofile;
        return $this->makePartial('getupdate');

    }
    
    

    
   
    
    
    public static function getsubmitsurvey($data)
    {   
        

        $surveyview = new Surveyshow();
        $surveyview->user_id = $data['user_id'];
        $surveyview->user_login = $data['user_login'];
        $surveyview->survey_name = $data['survey_name'];
        $surveyview->survey_id = $data['survey_id'];
        $jj = json_encode($data);
        // dd($jj);
        $surveyview->survey_data = $jj;
        $surveyview->save();
        

        
        
    }
  
}
