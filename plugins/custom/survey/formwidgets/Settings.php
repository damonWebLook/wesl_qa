<?php namespace Custom\Survey\FormWidgets;

use Backend\Classes\FormWidgetBase;

/**
 * settings Form Widget
 */
class Settings extends FormWidgetBase
{
    /**
     * @inheritDoc
     */
    protected $defaultAlias = 'custom_survey_settings';

    /**
     * @inheritDoc
     */
    public function init()
    {
    }

    /**
     * @inheritDoc
     */
    public function render()
    {
        $this->prepareVars();
        return $this->makePartial('settings');
    }

    /**
     * Prepares the form widget view data
     */
    public function prepareVars()
    {
        $this->vars['name'] = $this->formField->getName();
        $this->vars['value'] = $this->getLoadValue();
        $this->vars['model'] = $this->model;
    }

    /**
     * @inheritDoc
     */
    public function loadAssets()
    {
        $this->addCss('css/settings.css', 'Custom.Survey');
        $this->addJs('js/settings.js', 'Custom.Survey');
    }

    /**
     * @inheritDoc
     */
    public function getSaveValue($value)
    {
        return $value;
    }

    
}
