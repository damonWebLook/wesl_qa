<?php namespace Custom\Survey\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateSurveyshowsTable extends Migration
{
    public function up()
    {
        Schema::create('custom_survey_surveyshows', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('survey_id');
            $table->text('survey_name');
            $table->string('survey_process');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('custom_survey_surveyshows');
    }
}
