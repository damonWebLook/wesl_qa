<?php namespace Custom\Survey\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateSurveysTable extends Migration
{
    public function up()
    {
        Schema::create('custom_survey_surveys', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('title_name');
            $table->string('Survey_field');
            $table->text('defans');
            $table->text('Survey_name');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('custom_survey_surveys');
    }
}
