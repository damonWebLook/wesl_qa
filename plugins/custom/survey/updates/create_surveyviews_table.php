<?php namespace Custom\Survey\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateSurveyviewsTable extends Migration
{
    public function up()
    {
        Schema::create('custom_survey_surveyviews', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('survey_name');
            $table->string('process_status');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('custom_survey_surveyviews');
    }
}
