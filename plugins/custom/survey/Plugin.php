<?php namespace Custom\Survey;

use Backend;
use System\Classes\PluginBase;

/**
 * Survey Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Survey',
            'description' => 'No description provided yet...',
            'author'      => 'Custom',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }


    public function registerFormWidgets()
{
    return [
        'Custom\FormWidgets\Settings' => 'Settings'
        
    ];
}

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Custom\Survey\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        

        return [
            'custom.survey.some_permission' => [
                'tab' => 'Survey',
                'label' => 'Some permission'
            ],
            
            'custom.survey.surveyshows.some_permission' => [
                'tab' => 'Achieve survey ',
                'label' => 'Some permission'
            ],
            

            'custom.loyaltyservices.services.some_permission' => [
                'tab' => 'Listing',
                'label' => 'Some permission'
    
            ],

            'custom.services.services.some_permission' => [
                'tab' => 'Services',
                'label' => 'Some permission'
    
            ],

        ];

    }
    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        

        return [
            'survey' => [
                'label'       => 'Marketing',
                'url'         => Backend::url('custom/survey/Surveys'),
                'icon'        => 'icon-file-text-o',
                'permissions' => ['custom.survey.*'],
                'order'       => 500,

                'sideMenu' => [
                    'Loyalty Services' => [
                        'label'       => 'Listing',
                        'icon'        => 'icon-list',
                        'url'         => Backend::url('Custom\Loyaltyservices\Services'),
                        'permissions' => ['custom.loyaltyservices.*'],
                    ],
             
                    'Videos' => [
                        'label'       => 'Videos',
                        'icon'        => 'icon-film',
                        'url'         => Backend::url('Custom\Video\Videos'),
                        'permissions' => ['custom.video.*']
                    ],
                      'AdPackage' =>[
                        'label'       => 'Ad Packages',
                        'icon'        => 'icon-audio-description',
                        'url'         => Backend::url('Custom\Video\AdPackages'),
                        'permissions' => ['custom.video.*']
                    ],
                    
                    'survey_show ' => [
                        'label'       => 'Survey Archive',
                        'url'         => Backend::url('custom/survey/Surveyshows'),
                        'icon'        => 'icon-file-text-o',
                        'permissions' => ['custom.survey.*'],
                        'order'       => 500,
                    ],
                    'approverating' => [
                        'label'       => 'Rating',
                        'url'         => Backend::url('custom/rating/ratings'),
                        'icon'        => 'icon-commenting',
                        'permissions' => ['custom.rating.viewrate'],
                        'order'       => 500,
                    ],
                ]
                
                
                
            
            ],
            
           
        ];
    }
}
