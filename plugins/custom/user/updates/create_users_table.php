<?php namespace Custom\User\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateUsersTable extends Migration
{
    public function up()
    {
        Schema::create('custom_user_users', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('First_name');
            $table->text('Last_name');
            $table->text('email');
            $table->text('birth');
            $table->string('password');
            $table->text('contactno');
          
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('custom_user_users');
    }
}
