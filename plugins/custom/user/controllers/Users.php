<?php namespace Custom\User\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Session;
use Custom\User\Models\User;
use Hash;
use Flash;
use Mail;


/**
 * Users Back-end Controller
 */
class Users extends Controller
{
    /**
     * @var array Behaviors that are implemented by this controller.
     */
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    /**
     * @var string Configuration file for the `FormController` behavior.
     */
    public $formConfig = 'config_form.yaml';

    /**
     * @var string Configuration file for the `ListController` behavior.
     */
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Custom.User', 'user', 'users');
    }


    public static function addUserToSession($data){
        $user = User::where('email',$data['email'])->first();
        // dd($user);


        if($user){
            if(Hash::check($data['password'], $user->password)){
                Session::put('user_id',$user->id);
                Flash::success('You Are Loged Out Successfully!');
                return true;
            }else{
                
                Flash::success('Password Did Not Match!');
                return false;
            }
        }else{

            Flash::error('User Not Found!');
            return false;
        }
    }

    public static function saveUser($data){
 

        $check_is_registered = User::where('email',$data['email'])->first();
        if($check_is_registered){
            return false;
        }

        $user = new User();
        $user->login = $data['login'];
        $user->email = $data['email'];
        $user->birth  = $data['birth'];
        $user->contactno = $data['contactno'];
        $user->country = $data['country'];
        if($data['password'] == $data['password']){
        $user->password = Hash::make($data['password']);
        // $user->confirm_password = Hash::make($data['password']);
        $user->save();

        $user_id = $user->id;

        $vars = [
            'id' => $user->id,
            'email' => $user->email,
            'password' => $user->password
            
        ];

        Mail::send('custom.user::mail.userdetails_mail',$vars, function($message) use ($vars){

            $message->to($vars['email']);
            $message->subject('User Login Details');

        });

        Flash::success('You Have Successfully created Account. Please Log In !');


        return true;
        }
        else
        {
        Flash::error('Please Enter Same Password For Both Fields');
            return false;
        }
       
    }


    public static function checkUserByToken($user_id,$token)
    {
        $user = User::find($user_id);
        if($user->token == $token){
            return true;
        }else{
            return false;
        }
    

    }



    
    public static function updateProfile($data)
    {
       
        $user_id = Session::get('user_id');
        $user = User::find($user_id);
        
        $user->contactno    = $data['contactno'];
        $user->email        = $data['email'];
        $user->birth        = $data['birth'];
        $user->save();
        Flash::success('Profile updated');
        
    }

    public static function savepassword($data)
    {

        $user_id = Session::get('user_id');
        $user = User::find($user_id);
        if($data['password'] == $data['confirm_password']){
            $user->password = Hash::make($data['password']);
           
        $user->save();
        }

        else
        {
         Flash::error('Please Enter Same Password For Both Fields');
            return false;
        }

        
        

        

    }


    public static function changepassword($data)
    {
       
        $user = User::find($data['id']);
        if($user->reset_token == $data['reset_token']) {
            if($data['password'] == $data['confirm_password']){
                $user->password = Hash::make($data['password']);
                $user->reset_token      = '';
             $user->save();
             }
             else
             {
              Flash::error('Please Enter Same Password For Both Fields');
             return false;
             }
        } else {
            Flash::error('wrong token try again');
             return false;
        }
        // dd($user);

      
        // $user->id           = $data['id'];
        // $user->reset_token      = $data['reset_token'];
        



    }


    public static function forgetpassword($data)
    {
        // dd($data);
        $user = User::where('email', $data['email'])->first();
        if($user)
        {
            $token = bin2hex(random_bytes(12));
            $user->reset_token = $token;
            $user->save();
            $mail_data = [
                'id' => $user->id,
                'token' => $token,
                'email'  => $user->email,
                'name'   => $user->first_name
            ];

            Mail::send('custom.user::mail.reset_password', $mail_data, function ($message) use ($mail_data) {
                $message->to($mail_data['email']);
            });
            Flash::success('We sent you a reset link. Please follow that.');
        }
        else
        {
            Flash::error('User not found. May be you are not registered with us !');
        }
    }

  



}
