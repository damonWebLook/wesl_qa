<?php namespace Custom\Services;

use Backend;
use System\Classes\PluginBase;

/**
 * services Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'services',
            'description' => 'No description provided yet...',
            'author'      => 'custom',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Custom\Services\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        

        return [
            'custom.services.some_permission' => [
                'tab' => 'services',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        

        
        return [
            'services' => [
                'label'       => 'Services',
                'url'         => Backend::url('custom/services/services'),
                'icon'        => 'icon-plus',
                'permissions' => ['custom.services.*'],
                'order'       => 400,
            ],
        ];
    }
}
