<?php namespace Custom\Services\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use BackendAuth;
use Session;
use Backend\Models\User;
use Custom\Services\Models\Service;
use Custom\Agents\Models\Agent;
/**
 * Services Back-end Controller
 */
class Services extends Controller
{
    /**
     * @var array Behaviors that are implemented by this controller.
     */
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    /**
     * @var string Configuration file for the `FormController` behavior.
     */
    public $formConfig = 'config_form.yaml';

    /**
     * @var string Configuration file for the `ListController` behavior.
     */
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Custom.Services', 'services', 'services');
    }
    
    public function listExtendQuery($query)
    {
    
        $logged_user = BackendAuth::getUser();
        $user_id = self::getBackenduserId();
        if($user_id)
        {
            $query->where('user_id',$user_id);
        }
        else
        {
            $usersessions =  User::all();
            foreach ($usersessions as $usersession)
            {
                $users = explode(",",$usersession['assigned_user']);
                foreach ($users as $user)
                {
                    if($user == $logged_user->id)
                    {
                        $query->where('user_id',$usersession->id);
                    }
                }
            }
        }
        
    
       
       
    }
    
    public function formAfterCreate($model)
    {
        $id = Self::getBackenduserId();
        $user_id = $id['id'];
        $service = Service::find($model->id);
        $service->user_id = $user_id;
        $service->save();
    }
    
    
    
    public static function getBackenduserId()
    {
        $user_id= Session::get('backend_user_id');
        return $user_id;
    }

    public static function getfrontservices($id)
    { 
        $agent = Agent::find($id);
        $services = Service::where('user_id',$agent->user_id)->get();
        return $services;

    }


}
