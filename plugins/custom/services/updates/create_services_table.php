<?php namespace Custom\Services\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateServicesTable extends Migration
{
    public function up()
    {
        Schema::create('custom_services_services', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id')->nullable();
            $table->text('service_name');
            $table->text('service_category');
            $table->text('service_descriptions');
            
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('custom_services_services');
    }
}
