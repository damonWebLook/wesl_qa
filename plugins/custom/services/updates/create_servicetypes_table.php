<?php namespace Custom\Services\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateServicetypesTable extends Migration
{
    public function up()
    {
        Schema::create('custom_services_servicetypes', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('service_type_name');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('custom_services_servicetypes');
    }
}
