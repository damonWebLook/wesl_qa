<?php namespace Custom\Agents\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Mail;
use BackendAuth;
use Custom\Agents\Models\Agent;
use Custom\Agents\Models\City;
use Custom\Agents\Models\Country;
use Custom\Services\Models\Service;
use Backend\Models\User;
use Custom\Rating\Models\Rating;
use Custom\Rating\Controllers\Ratings;
use Custom\Currency\Models\AgentCurrency;
use Custom\Currency\Models\Currency;
use System\Models\File as File;
use Session;
use Flash;
use Backend;
use Crypt;
use Illuminate\Support\Facades\Input;
use Validation;
use ValidationException;
use Model; 
use Illuminate\Support\Str;

/**
 * Agents Back-end Controller
 */
class Agents extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Custom.Agents', 'agents', 'agents');
    }


    public static function onSignin($data)
    {
       
        $userlog = BackendAuth::authenticate([
            'login' => post('login'),
            'password' => post('password')
        ]);
        $loggedIn = BackendAuth::check($userlog);
        
        $id = [
           'id'    => $userlog->id
        
        ];
        
    
        $user = User::where('email', $data['login'])->where('id', 1)->first();
        if($user)
        {
            if($user->password == md5($data['password']))
            {
                $session_data = [
                    'id'    => $user->id,
                    'fname' => $user->first_name,
                    'lname' => $user->last_name,
                    'email' => $user->email,
                    'user' =>  $user->login
                ];

                Session::put('id', $session_data);
                return Backend::redirect('backend');
                
                
            }
            else
            {
                Flash::error('Password not matched !!!');
            }
        }
        else
            {
              Flash::error('User not found !!!');
            }
        
        if ($loggedIn = 1) {
            Session::put('backend_user_id', $id);
            Flash::success('Welcome to the Dashboard');
            return back();
        }
        else
        {
          Flash::error("you cannot Log In check the Username and Password");
        }
    

    
    }
    
 
    public static function onInquryAgent($data)
    {
        $agent = new Agent();
        $agent->first_name = $data['first_name'];
        $agent->last_name = $data['last_name'];
        $agent->login = $data['login'];
        $agent->email = $data['email'];
        $agent->agent_contact = $data['agent_contact'];
        $agent->country = $data['country'];
        $agent->city = $data['city'];
        $agent->save();
        return $agent->id;
    }

    public function formAfterSave($data)
    { 
    // dd($data);
        // if($data->id){
        //     $agent = Agent::find($data->id);
        //     $users = User::find($agent->user_id);
        //     // dd($users->login);
        //     $agent->is_activated = $data->is_active;
        // }else{
            
            $users  = new User();
            $users->is_activated = 1;
        // }
        $random = Str::random(6);
        // dd($data->login);
        $users->login = $data->login;
        $users->email = $data->email;
        $users->password = $random;
        $users->password_confirmation = $random;
        $users->role_id = 3;
        
        $user_id = $data['id'];
        $agent = Agent::find($user_id);   
        $users->save();
        // dd($data->id);
        $agent = Agent::find($data->id);
        $agent->user_id = $users->id;
        $agent->save();

        $vars = [
            'email' => $users->email,
            'login' => $users->login,
            'password' => $random
        ];
        Mail::send('custom.agents::mail.useremail_mail', $vars, function($message) use ($vars) { 
            $message->to($vars['email']);
            $message->subject('Agent User  Credentials');      
        }); 
    }  
    
    
    
    
    
    public static function getAllRateList()
    {
        $rates = Rating::all();
       
       
        $rate_arr =[];

        foreach($rates as $key=>$rate){
            $rate = Rating::find($rate->id);
            $rate_arr[$key]['rate'] = $rate;
            $rate_arr[$key]['agent'] = Agent::find($rate->agent_id);
            $rate_arr[$key]['agent_user'] = User::find($rate->user_id);
        }
      
        return $rate_arr;
        

    }

    public  static function topagent()
    {
    
        $rates = Rating::where('rating',3)->get();
        
        
        
 
        foreach ($rates as $rate) {
        // dd($rate);
            $user = $rate->user_id;
                    }
        return $rates;
 

    
    
    }
    
    public function getcountryagent()
    {
        $agentid = Agent::find('country');
        
        
    
    }
    
    

  

    public function onPayment()
    {

        foreach($_POST['checked'] as $agent_id){
            $agent = Agent::find($agent_id);
            $vars = [
                'email' => $agent->email,
                'user' => $agent->Name
            ];

          
            Mail::send('custom.agents::mail.payment_mail', $vars, function($message) use ($vars) {
                
                $message->to($vars['email']);
                $message->subject('Payment Message');
                
            });    
        }


    }

    public function getStatus()
    {
        $payment = Agents::onPayment();
        if($payment == $_POST['checked']){
            echo "Payment Send";
        } else{
            echo "Disapproved";
        }        
    }


    public function getcityStatuses($city_id)
    {      
        return City::find($city_id)->name;
       

    }

    public function getcountryStatuses($country_id)
    {
     
        return Country::find($country_id)->name;

    }

  
    
    
    public function onEdit($data)
    {
        
        $agent = Agent::find($data);
        $user = User::find($agent->user_id);
        // $user00 = new User;
        
        $user00 ->first_name = $agent->first_name;
        dd($agent->first_name);
      
        $user00 ->last_name = $agent->last_name;
        $user00 ->login = $agent->login;
        $user00 ->email = $agent->email;
      
        $user00 ->save();
    
      
    
        Flash::success('Information have edited Successfully');
    }

    
    public function onCheckRate()
    {
    
    
    
        Flash::success('Information have edited Successfully');
    }
    
    public static function onRegister()
    {
    
        Flash::success('Information have edited Successfully');
    
    }

    public function onUpload($data)
    {
       
        $agent = Agent::find($data);
        $profile = User::find($agent->user_id);
        $profile->avatar = Input::file('profile_image');
        
        $profile->save();
    

      

        Flash::success('Image Upload Successfully');


    }

    public static function getAllAgentList()
    {
        $agents = Agent::where('is_active', 1)->get();
       
       
        $agent_arr =[];

        foreach($agents as $key=>$agent){
            $agent = Agent::find($agent->id);
            $agent_arr[$key]['agent'] = $agent;
            $agent_arr[$key]['country'] = Country::find($agent->country);
            $agent_arr[$key]['city'] = City::find($agent->city);
            $agent_arr[$key]['agent_user'] = User::find($agent->user_id);
            $agent_arr[$key]['currencies'] = self::getCurrenciesFromAgentId($agent->id);
        }
      
        return $agent_arr;
        

    }
    
    public static function getCurrenciesFromAgentId($agent_id)
    {
        $id = Agent::find($agent_id)->user_id;

        $currencies = AgentCurrency::where('backend_user_id',$id)->get();
       
        $currency_arr = [];
        
        foreach($currencies as $currency){
            $currency_arr['currency'] = Currency::find($currency['base_currency_id']);
        }
        return $currency_arr;
    }


    public function onPassword($data)
    {
        $agent = Agent::find($data);
        $user = User::find($agent->user_id);
        $random = Str::random(6);
        $user->password = $random;
        $user->password_confirmation = $random;
        $user->save();

        Flash::success('Password was reset Successfully');

        $vars = [
            'email' => $user->email,
            'login' => $user->login,
            'password' => $random
        ];
        Mail::send('custom.agents::mail.passwordreset_mail', $vars, function($message) use ($vars) { 
            $message->to($vars['email']);
            $message->subject('Password Reset Successfully');      
        }); 
      
       

    }

    public static function getAgentListByAttributes($data){

        
        $agentsByCurrency = AgentCurrency::distinct()->where('base_currency_id', $data['currency_id'])->lists('backend_user_id');
        if (empty($agentsByCurrency )){
             $agents = Agent::where(['country'=>$data['country_id'],'city'=>$data['city_id']])->get();
        }else{
             $agents = Agent::where(['country'=>$data['country_id'],'city'=>$data['city_id']])->orWhere('user_id', $agentsByCurrency)->get();
        }
        // $agents = Agent::where(['country'=>$data['country_id'],'city'=>$data['city_id']])->orWhere('user_id', $agentsByCurrency)->get();
        $agent_arr = [];
 
        foreach($agents as $key=>$agent){
            $agent = Agent::find($agent->id);
            
            $agent_arr[$key]['agent'] = $agent;
            $agent_arr[$key]['country'] = Country::find($agent->country);
            $agent_arr[$key]['city'] = City::find($agent->city);
            $agent_arr[$key]['agent_user'] = User::find($agent->user_id);
        }
     
        return $agent_arr;
    }

    public static function getAgentById($id){
        $agent = Agent::find($id);
        $agent_arr = [];
        $agent_arr['agent'] = $agent;
        $agent_arr['country'] = Country::find($agent->country);
        $agent_arr['city'] = City::find($agent->city);
        $agent_arr['agent_user'] = User::find($agent->user_id);

        return $agent_arr;
    }
    
    
    /**
    *Function to get New Agents in Front end
    */
    public static function getNewAgents(){
        return Agent::orderBy('created_at','desc')->limit(5)->get();
    }
    
    


}
