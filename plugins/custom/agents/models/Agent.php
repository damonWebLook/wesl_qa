<?php namespace Custom\Agents\Models;

use Model;
use Custom\Agents\Models\Country;
use Custom\Agents\Models\City;

/**
 * Agent Model
 */
class Agent extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'custom_agents_agents';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];
    
    protected $purgeable = ['password_confirmation'];

    /**
     * @var array Validation rules for attributes
     */
    public $rules = [];

    /**
     * @var array Attributes to be cast to native types
     */
    protected $casts = [];

    /**
     * @var array Attributes to be cast to JSON
     */
    protected $jsonable = ['agent_social_contacts','agent_social_media'];

    /**
     * @var array Attributes to be appended to the API representation of the model (ex. toArray())
     */
    protected $appends = [];

    /**
     * @var array Attributes to be removed from the API representation of the model (ex. toArray())
     */
    protected $hidden = [];

    /**
     * @var array Attributes to be cast to Argon (Carbon) instances
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'rating' => [
            'Custom\Rating\Models\Rating',
            'table' => 'custom_rating_ratings',
            'key' => 'agent_id'
        ]
    ];
    public $belongsTo = [];
    public $belongsToMany = [
        'rating' => [
            'Custom\Rating\Models\Rating',
            'table' => 'custom_rating_ratings',
            'key' => 'agent_id'
        ]
    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [

        'avater_upload' => 'System\Models\File'
    ];
    public $attachMany = [];
    
    
    public function getSocialprovideOptions()
    { 
    
        return [
			0 => 'Imo',
			1 => 'Whatsapp',
			2 => 'Viber'
		];
        
    
    }
    
    public function getSocialproviderOptions()
    {
        return [
			0 => 'Facebook',
			1 => 'Twitter',
			2 => 'Google',
            3 => 'Pinerest',
            4 => 'Instagram',
            5 => 'Likendin'
		];
    
    }

    public function getcountryOptions()
    {  
        
        return Country::lists('name', 'id');
    }

    public function getCityOptions() 
    {

        return City::lists('name', 'id');
    }

    public function getIsActiveOptions(){
        return [
            0 => 'Suspend',
            1 => 'Active'
        ];
    }

  
}
