<?php namespace Custom\Agents\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateAgentsTable extends Migration
{
    public function up()
    {
        Schema::create('custom_agents_agents', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('country_id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('backend_users');
            $table->text('first_name');
            $table->text('last_name');    
            $table->text('login')->nullable();    
            $table->text('country')->nullable();
            $table->text('city')->nullable();
            $table->text('agent_company')->nullable();
            $table->text('agent_contact')->nullable();
            $table->text('email')->nullable();
            $table->text('agent_social_contacts')->nullable();
            $table->text('agent_social_media')->nullable();
            $table->text('agent_moneytransfers')->nullable();
            $table->text('agent_rating')->nullable();
            $table->text('status')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('custom_agents_agents');
    }
}
