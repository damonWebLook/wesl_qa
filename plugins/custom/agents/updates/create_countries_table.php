<?php namespace Custom\Agents\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateCountriesTable extends Migration
{
    public function up()
    {
        Schema::create('custom_agents_countries', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('iso');
            $table->string('name');
            $table->text('nicename');
            $table->string('iso3');
            $table->string('numcode');
            $table->string('phonecode');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('custom_agents_countries');
    }
}
