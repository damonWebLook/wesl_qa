<?php namespace Custom\Agents\ReportWidgets;

use Backend\Classes\ReportWidgetBase;
use Exception;
use Custom\Agents\Models\Agent;
use Custom\Agents\Controllers\Agents;
use BackendAuth;

/**
 * AgentDetails Report Widget
 */
class AgentDetails extends ReportWidgetBase
{
    
    /**
     * @var string The default alias to use for this widget
     */
    protected $defaultAlias = 'AgentDetailsReportWidget';

    /**
     * Defines the widget's properties
     * @return array
     */
    public function defineProperties()
    {
        return [
            'title' => [
                'title'             => 'backend::lang.dashboard.widget_title_label',
                'default'           => 'Agent Details Report Widget',
                'type'              => 'string',
                'validationPattern' => '^.+$',
                'validationMessage' => 'backend::lang.dashboard.widget_title_error',
            ],
        ];
    }
    
    /**
     * Adds widget specific asset files. Use $this->addJs() and $this->addCss()
     * to register new assets to include on the page.
     * @return void
     */
    protected function loadAssets()
    {
    }
    
    /**
     * Renders the widget's primary contents.
     * @return string HTML markup supplied by this widget.
     */
    public function render()
    {
       $agents = BackendAuth::getUser();
    //    dd($agents->id);
       $arr['fname'] = $agents->first_name;
       $arr['lname'] =  $agents->last_name;
       $arr['login'] = $agents->login;
       $arr['email'] = $agents->email;
        $agentInfo = Agent::find($agents->id);
        $arr['country']  = $agentInfo->country;
        $arr['city']  = $agentInfo->city;
        $arr['company']  = $agentInfo->agent_company;
        // dump($arr);

        return $this->makePartial('agentdetails', ['agentdetail' => $arr]);
    }

    /**
     * Prepares the report widget view data
     */
    public function prepareVars()
    {
    }
    public function onEdit($data)
    {
        
        $agent = Agent::find($data);
        dd($agent);
        $user = User::find($agent->user_id);
        // $user00 = new User;
        
        $user->first_name = $agent->first_name;
        dd($agent->first_name);
      
        $user->last_name = $agent->last_name;
        $user->login = $agent->login;
        $user->email = $agent->email;
    -
        $user->save();
    
      
    
        Flash::success('Information have edited Successfully');
    }

    public function onSend(){
        $data = post();
        dd($data);
    }
}
