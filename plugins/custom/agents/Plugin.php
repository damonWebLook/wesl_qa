<?php namespace Custom\Agents;

use Backend;
use System\Classes\PluginBase;

/**
 * Agents Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Agents',
            'description' => 'No description provided yet...',
            'author'      => 'custom',
            'icon'        => 'icon-leaf'
        ];
    }



    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    public function registerReportWidgets()
    {

        return [
            'Custom\Agents\ReportWidgets\AgentDetails' => [
                'label' => 'User Profile',
                'context' => 'dashboard'

            ]
        ];

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
    
    
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Custom\Agents\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
       

        return [
            'custom.agents.some_permission' => [
                'tab' => 'Agents',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        

        return [
            'agents' => [
                'label'       => 'Agents',
                'url'         => Backend::url('custom/agents/Agents'),
                'icon'        => 'oc-icon-user',
                'permissions' => ['custom.agents.*'],
                'order'       => 500,
            ],
        ];
    }

    
}
