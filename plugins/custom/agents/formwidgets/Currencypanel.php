<?php namespace Custom\Agents\FormWidgets;

use Backend\Classes\FormWidgetBase;

/**
 * currencypanel Form Widget
 */
class Currencypanel extends FormWidgetBase
{
    /**
     * @inheritDoc
     */
    protected $defaultAlias = 'custom_agents_currencypanel';

    /**
     * @inheritDoc
     */
    public function init()
    {
    }

    /**
     * @inheritDoc
     */
    public function render()
    {
        $this->prepareVars();
        return $this->makePartial('currencypanel');
    }

    /**
     * Prepares the form widget view data
     */
    public function prepareVars()
    {
        $this->vars['name'] = $this->formField->getName();
        $this->vars['value'] = $this->getLoadValue();
        $this->vars['model'] = $this->model;
    }

    /**
     * @inheritDoc
     */
    public function loadAssets()
    {
        $this->addCss('css/currencypanel.css', 'Custom.Agents');
        $this->addJs('js/currencypanel.js', 'Custom.Agents');
    }

    /**
     * @inheritDoc
     */
    public function getSaveValue($value)
    {
        return $value;
    }
}
