<?php namespace Custom\Agents\FormWidgets;

use Backend\Classes\FormWidgetBase;

/**
 * Serviceshow Form Widget
 */
class Serviceshow extends FormWidgetBase
{
    /**
     * @inheritDoc
     */
    protected $defaultAlias = 'custom_agents_serviceshow';

    /**
     * @inheritDoc
     */
    public function init()
    {
    }

    /**
     * @inheritDoc
     */
    public function render()
    {
        $this->prepareVars();
        return $this->makePartial('serviceshow');
    }

    /**
     * Prepares the form widget view data
     */
    public function prepareVars()
    {
        $this->vars['name'] = $this->formField->getName();
        $this->vars['value'] = $this->getLoadValue();
        $this->vars['model'] = $this->model;
    }

    /**
     * @inheritDoc
     */
    public function loadAssets()
    {
        $this->addCss('css/serviceshow.css', 'Custom.Agents');
        $this->addJs('js/serviceshow.js', 'Custom.Agents');
    }

    /**
     * @inheritDoc
     */
    public function getSaveValue($value)
    {
        return $value;
    }
}
