<?php namespace Custom\Rating\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateRatingsTable extends Migration
{
    public function up()
    {
        Schema::create('custom_rating_ratings', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('custom_user_users');
            $table->integer('agent_id')->unsigned();
            $table->foreign('agent_id')->references('id')->on('custom_agents_agents');
            $table->integer('rating');
            $table->text('comment')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('custom_rating_ratings');
    }
}
