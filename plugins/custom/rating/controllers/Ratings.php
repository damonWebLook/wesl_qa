<?php namespace Custom\Rating\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Custom\Rating\Models\Rating;
/**
 * Ratings Back-end Controller
 */
class Ratings extends Controller
{
    /**
     * @var array Behaviors that are implemented by this controller.
     */
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    /**
     * @var string Configuration file for the `FormController` behavior.
     */
    public $formConfig = 'config_form.yaml';

    /**
     * @var string Configuration file for the `ListController` behavior.
     */
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Custom.Rating', 'rating', 'ratings');
    }

    public static function checkPermissionRate($userId, $agentId){
        $result = Rating::where('user_id', $userId)->where('agent_id', $agentId)->first();
        if (is_null($result)){
            //User can Rate
            return 0;
        }else{
            //User already rated
            return 1;
        }
    }

    public static function getAvgRating($agentId){
        // $count = Rating::where('agent_id', $agentId)->count();
        $result = Rating::where('agent_id', $agentId)->avg('rating');
        return round($result);
    }

    public static function saveRating($userId, $agentId, $data){
        $rating = $data['rating'];
        $comment  =$data['comment'];
        $rate = new Rating;
        $rate->user_id = $userId;
        $rate->agent_id = $agentId;
        $rate->rating = $rating;
        $rate->comment = $comment;
        $rate->save();

    }
    
    /**
     * The functions get the Agent Id and returns the avg rating, total no. of rates along
     * with %rated for each star
     */
    public static function showRating($id){
        
        //gets the avg rating for the agent
        $avg = (int) round(Rating::where('agent_id', $id)->where('is_approved',1)->avg('rating'));
        //Gets total number of users rated the agent
        $count = (int) Rating::where('agent_id', $id)->where('is_approved',1)->count();
        $one = Rating::where('agent_id', $id)->where('rating',1)->where('is_approved',1)->count();
        $two = Rating::where('agent_id', $id)->where('rating',2)->where('is_approved',1)->count();
        $three = Rating::where('agent_id', $id)->where('rating',3)->where('is_approved',1)->count();
        $four = Rating::where('agent_id', $id)->where('rating',4)->where('is_approved',1)->count();
        $five = Rating::where('agent_id', $id)->where('rating',5)->where('is_approved',1)->count();
        
        return(
            [
                'avg' => $avg,
                'count' => $count,
                'one' => self::getPercentage($one, $count),
                'two' => self::getPercentage($two, $count),
                'three' => self::getPercentage($three, $count),
                'four' => self::getPercentage($four, $count),
                'five' => self::getPercentage($five, $count),
            ]
            );

    }
    public static function getPercentage($value, $total){
        if ( $total === 0 && $value === 0){
            return 0;
        }else{
            return (int) round($value/$total * 100, 0);
        }
        
       
    }
    
    public static function hideRating($status){
        if ($status == true){
            return 1;
        }else{
            return 0;
        }
    }

    public function listExtendQuery($query){
        $id = \BackendAuth::getUser()->id;
        $query->where('agent_id', $id)->get();        
    }    
    
}
