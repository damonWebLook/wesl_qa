<?php namespace Custom\Rating;

use Backend;
use System\Classes\PluginBase;

/**
 * Rating Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Rating',
            'description' => 'This contains rating for agents provided by users',
            'author'      => 'Custom',
            'icon'        => 'icon-commenting'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Custom\Rating\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'custom.rating.some_permission' => [
                'tab' => 'Rating',
                'label' => 'Some permission'
            ],
            'custom.rating.viewrate' => [
                    'tab' => 'Rating',
                    'label' => 'Approve Rating for Agents',
                    'role' => ['AGT']
                ]
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        // return []; // Remove this line to activate

        return [
            'rating' => [
                'label'       => 'Rating',
                'url'         => Backend::url('custom/rating/ratings'),
                'icon'        => 'icon-commenting',
                'permissions' => ['custom.rating.viewrate'],
                'order'       => 500,
            ],
        ];
    }
}
