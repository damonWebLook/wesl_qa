<?php namespace Custom\Payment\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreatePaymentsTable extends Migration
{
    public function up()
    {
        Schema::create('custom_payment_payments', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('Payment_Name');
            $table->text('Action_Page');
            $table->text('Payment_Price');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('custom_payment_payments');
    }
}
