<?php namespace Custom\Payment;

use Backend;
use System\Classes\PluginBase;

/**
 * Payment Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Payment',
            'description' => 'No description provided yet...',
            'author'      => 'Custom',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Custom\Payment\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'custom.payment.some_permission' => [
                'tab' => 'Payment',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'payment' => [
                'label'       => 'Payment',
                'url'         => Backend::url('custom/payment/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['custom.payment.*'],
                'order'       => 500,
            ],
        ];
    }

    public function registerSettings()
    {
        return [
           
            'Payment'  => [
                'label' => 'Payment',
                'description' => 'Manage Payment',
                'category' => 'Payment',
                'icon' => 'icon-globe',
                'url' => Backend::url('custom/payment/payments'),   
              
            ],
              
    
        ];


    }
}
