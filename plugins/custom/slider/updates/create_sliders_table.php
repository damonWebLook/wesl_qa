<?php namespace Custom\Slider\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateSlidersTable extends Migration
{
    public function up()
    {
        Schema::create('custom_slider_sliders', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('name');
            $table->text('link');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('custom_slider_sliders');
    }
}
