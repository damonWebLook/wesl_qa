<?php namespace Custom\Point;

use Backend;
use System\Classes\PluginBase;

/**
 * Point Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Point',
            'description' => 'No description provided yet...',
            'author'      => 'custom',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Custom\Point\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        

        return [
            'custom.point.some_permission' => [
                'tab' => 'Point',
                'label' => 'Some permission'
            ],
        ];


       
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'point' => [
                'label'       => 'Point',
                'url'         => Backend::url('custom/point/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['custom.point.*'],
                'order'       => 500,
            ],
        ];
    }


    public function registerSettings()
    {
    
       return [
           'Videopoint'  => [
             'label' => 'Video Point',
             'description' => 'Manage Video Point',
             'category' => 'Points',
             'icon' => 'icon-globe',
             'class' => 'Custom\Point\Models\Videopoint',
   
           ],

           'Surveypoint'  => [
            'label' => 'Survey Point',
            'description' => 'Manage Video Point',
            'category' => 'Points',
            'icon' => 'icon-globe',
            'class' => 'Custom\Point\Models\Surveypoint',

          
          ]




           
           
        ];
    } 
}
