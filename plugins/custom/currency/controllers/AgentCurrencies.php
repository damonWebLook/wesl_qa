<?php 
namespace Custom\Currency\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Custom\Currency\Models\AgentCurrency;
use Custom\Currency\Models\Currency;
use Custom\Currency\Models\Currencyrate;

/**
 * Agent Currencies Back-end Controller
 */
class AgentCurrencies extends Controller
{
    /**
     * @var array Behaviors that are implemented by this controller.
     */
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    /**
     * @var string Configuration file for the `FormController` behavior.
     */
    public $formConfig = 'config_form.yaml';

    /**
     * @var string Configuration file for the `ListController` behavior.
     */
    public $listConfig = 'config_list.yaml';

    /**
     * Registering Permissions
     */

    public $requiredPermissions = ['custom.currency.addAgentCurrency','custom.currency.addCurrency','custom.currency.addSellerCurrency'];

    public function __construct()
    {
        parent::__construct();

        // BackendMenu::setContext('October.System', 'system', 'settings');
        BackendMenu::setContext('Custom.Currency', 'currency', 'agentcurrency');
        // BackendMenu::setContext('Custom.Currency', 'currency', 'assignsellingcurrency');
        // SettingsManager::setContext('Custom.Currency', 'AgentCurrency');
    }
    public function checkAdmin(){
        $id = \BackendAuth::getUser()->role['id'];
        if ($id == 3){
            
            $id = \BackendAuth::getUser()->id;
            
        }else{
            // 0 for admins
            $id = 0;
        }
        return $id;
    }
    public function getBaseCurrencyOptions(){
        $id = $this->checkAdmin();
         
        return AgentCurrency::getBaseCurrency($id);
    }

    public function getCurrencyOptions(){
        return AgentCurrency::getCurrency();
    }



    public function getAgentName(){
        return AgentCurrency::getAgentName();
    }
    
        public function listExtendQuery($query){
        $id = \BackendAuth::getUser()->id;
        $query->where('backend_user_id', $id)->get();        
    }


    // public function formExtendFields($form){
    //     if (!$this->user->hasAccess('custom.currency.addSellerCurrency')){
    //         $form->addFields([
    //             'currency_id' => [
    //                 'label' => 'Currency',
    //                 'type' => 'partial'
    //             ]
    //         ]);
    //     }
    // }
}
