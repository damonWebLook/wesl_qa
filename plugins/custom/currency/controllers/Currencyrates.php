<?php namespace Custom\Currency\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Custom\Currency\Models\AgentCurrency;
use Custom\Currency\Models\Currencyrate;
use Custom\Currency\Models\Currency;

use Flash;
use BackendAuth;
use Session;
use Backend\Models\User;
use DB;


/**
 * Currencyrates Back-end Controller
 */
class Currencyrates extends Controller
{
    /**
     * @var array Behaviors that are implemented by this controller.
     */
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    /**
     * @var string Configuration file for the `FormController` behavior.
     */
    public $formConfig = 'config_form.yaml';

    /**
     * @var string Configuration file for the `ListController` behavior.
     */
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Custom.Currency', 'currency', 'currencyrates');
    }
    
    // public function listExtendQuery($query)
    // {
    //     $logged_user = BackendAuth::getUser();
    //     $user_id = self::getBackenduserId();     
    //     if($user_id)
    //     {
    //         $query->where('user_id',$user_id);
            
    //     }else
    //     {
    //         $usersessions =  User::all();
    //         foreach ($usersessions as $usersession)
    //         {
    //             $users = explode(",",$usersession['assigned_user']);
    //             foreach ($users as $user)
    //             {
    //                 if($user == $logged_user->id)
    //                 {
    //                     $query->where('user_id',$usersession->id);
    //                 }
    //             }
    //         }
    //     }          
    // }
    
    
    public function formAfterCreate($model)
    {
        $id = Self::getBackenduserId();
        if($id == NULL){
            $user_id = '1';
        }else{
            $user_id = $id;
        }
        $currentrates = Currencyrate::find($model->id);
        $currentrates->user_id = $user_id;
        $currentrates->save();
    }
    
    
    
    public static function getBackenduserId()
    {
        $user_id= Session::get('backend_user_id');
      
        return $user_id;
      
    }

    public function onBuyingRate()
    {

               
        $buyingrate = Currencyrate::find($_POST['id']);
        
        $buyingrate->buy_rate = $_POST['buy_rate'];
        
        // $logged_user = BackendAuth::getUser();
        // $user_id = self::getBackenduserId();     
        
        // if($user_id)
        // {
        //     $query->where('user_id',$user_id);
        // }
        // else
        // {
        //     $usersessions =  User::all();
        //     foreach ($usersessions as $usersession)
        //     {
        //         $users = explode(",",$usersession['assigned_user']);
        //         foreach ($users as $user)
        //         {
        //             if($user == $logged_user->id)
        //             {
        //                 $query->where('user_id',$usersession->id);
        //             }
        //         }
        //     }
        // }   
          
       
        $buyingrate->save();

        
        Flash::success('Buying Rate Updated');
        
         

    }

    public function onSellingRate()
    {
        
        $sellingrate = Currencyrate::find($_POST['id']);
       
        $sellingrate->sell_rate = $_POST['selling_rate'];
        
        // if($user_id)
        // {
        //     $query->where('user_id',$user_id);
            
        // }else
        // {
        //     $usersessions =  User::all();
        //     foreach ($usersessions as $usersession)
        //     {
        //         $users = explode(",",$usersession['assigned_user']);
        //         foreach ($users as $user)
        //         {
        //             if($user == $logged_user->id)
        //             {
        //                 $query->where('user_id',$usersession->id);
        //             }
        //         }
        //     }
        // }          
        
        $sellingrate->save();
        Flash::success('Selling Rate Updated');



    }
    
    public function onSelect()
    {   
        $agent = BackendAuth::getUser()->id;

        $baseCurrencies = DB::table('custom_currency_currencies')
                        ->join('custom_currency_agent_currencies','custom_currency_agent_currencies.base_currency_id','=','custom_currency_currencies.id')
                        ->select('custom_currency_currencies.id','custom_currency_currencies.currency_name')
                        ->distinct()
                        ->where('custom_currency_agent_currencies.backend_user_id',$agent)
                        ->get()
                        ->toJson();
        $baseCurrencies = json_decode($baseCurrencies, true);
        $count = count($baseCurrencies);
        $currencyList = array();
        for ($i = 0; $i <= $count-1; $i++){
                $currencyList[$baseCurrencies[$i]['id']] =$baseCurrencies[$i]['currency_name'];
        }
        
        return ['results' => $currencyList];
        // dump (['results' => $currencyList]);

    }

    public function onGetCurrency(){
        dump($this->onSelect());
    }

    public function getCurrencyOptions($id){
        $agentCurrencyId = Currencyrate::find($id)->agent_currency_id;
        $currencyId = AgentCurrency::find($agentCurrencyId)->currency_id;
        return Currency::find($currencyId)->currency_name;

    }

    public function listExtendQuery($query){
        $user = \BackendAuth::getUser();
        $code = $user->id;
        $query->whereHas('agent_currency', function($q) use ($code){
            $q->where('backend_user_id', '=', $code);
        });
    }


    //Frontend Function

    /**
     * The below function gets currencyrate to be loaded in Agentprofile by filtering the base currency and agentId
     */
    public static function getFrontEndCurrencyRate($id, $baseCurrencyId){
        $agentId = AgentCurrency::where([['backend_user_id',$id], ['base_currency_id', $baseCurrencyId]])->get();
        $count =0;
        $arr = array();
        foreach ($agentId as $agent){
            $currencyId = Currency::find($agent->currency_id);
            $name = $currencyId->currency_name;
            
            $buy =  Currencyrate::select('buy_rate')->where('agent_currency_id', $agent->id)->get()->toArray();
            $sell = Currencyrate::select('sell_rate')->where('agent_currency_id', $agent->id)->get()->toArray();
            // array_push($arr, $count);
            $arr[$count] = [
                'name' => $name,
                'buy' => $buy[0]['buy_rate'],
                'sell' => $sell[0]['sell_rate'],
            ];
            $count += 1;
        }
        return $arr;
    }
    /**
     * The below function gets base currency for the agent and loads into  
     */
    public static function getAgentBaseCurrency($id){
        $currency = AgentCurrency::select('base_currency_id')->distinct()->where('backend_user_id', $id)->get()->toArray();
        $arr = array();
        $i =0;
        foreach ($currency as $name){
            $nameId = $name['base_currency_id'];
            $currencyName = Currency::select('currency_name')->where('id', $nameId)->get()->toArray();
            // dd($currencyName[0]['currency_name']);
            $arr[$i] = [
                'id' => $nameId,
                'name' => $currencyName[0]['currency_name'],
            ];
            $i += 1;

        }
        return $arr;
        // dump($arr);
        
    }
    //Currency Calc Dropdown Currency List
    public static function getAgentCurrency($id){
        $currency = AgentCurrency::select('base_currency_id')->distinct()->where('backend_user_id',$id)->get()->toArray();
        $secondaryCurrency = AgentCurrency::select('currency_id')->distinct()->where('backend_user_id',$id)->get()->toArray();

        $currencyList = Currency::all()->lists('currency_name', 'id');
        $arr = array();
        foreach($currency as $key=>$value){
            $name = $currencyList[$value['base_currency_id']];
            $arr[$value['base_currency_id']] = $name;
        }
        foreach($secondaryCurrency as $key=>$value){
            $name = $currencyList[$value['currency_id']];
            $arr[$value['currency_id']] = $name;
        }
        return ($arr);
    }

    public static function calculateCurrency($data, $id){
        $from = $data['from'];
        $to = $data['to'];

        $amount = (float)$data['amount'];
        //Check if From is a base currency
        $isBase = Currencyrate::where('agent_id', $id)->where('base_currency_id', $from)->get()->toArray();
        $isBaseAvailable = Currencyrate::where('agent_id', $id)->where('base_currency_id', $from)->where('secondary_currency_id',$to)->get()->toArray();
        $isSecondAvailable = Currencyrate::where('agent_id', $id)->where('base_currency_id', $to)->where('secondary_currency_id',$from)->get()->toArray();
        // dump($isSecondAvailable);
        

        if (empty($isBaseAvailable) && empty($isSecondAvailable)){
        
            return 1;
            
        }else{
          
                if (empty($isBase)){
        
                    $rate = CurrencyRate::select('buy_rate')->where('agent_id', $id)->where('base_currency_id', $to)->where('secondary_currency_id' , $from)->get()->toArray();
                    $value = (float)$rate[0]['buy_rate'];
                    $name = Currency::find($to);
    
                    return ([ 0 => ['value' => number_format($amount / $value,2, '.', ''),
                                    'name' => $name->currency_name]]);
                }else{
                    
                    $rate = CurrencyRate::select('buy_rate')->where('agent_id', $id)->where('base_currency_id', $from)->where('secondary_currency_id' , $to)->get()->toArray();
                    $value = (float)$rate[0]['buy_rate'];
                    $name = Currency::find($to);
             
                    return ([ 0 => ['value' => \number_format ($amount * $value, 2, '.', ''),
                                    'name' => $name->currency_name]]);
                    
                }
        }

        
    }

}
