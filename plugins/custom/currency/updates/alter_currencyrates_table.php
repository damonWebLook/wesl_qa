<?php namespace Custom\Currency\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class AlterCurrencyratesTable extends Migration
{
    public function up()
    {
        Schema::table('custom_currency_currencyrates', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->text('buyJson')->after('sell_rate')->nullable();
            $table->text('sellJson')->after('buyJson')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('custom_currency_currencyrates');
    }
}
