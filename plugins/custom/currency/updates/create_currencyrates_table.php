<?php namespace Custom\Currency\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateCurrencyratesTable extends Migration
{
    public function up()
    {
        Schema::create('custom_currency_currencyrates', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('agent_currency_id')->unsigned();
            $table->foreign('agent_currency_id')->references('id')->on('custom_currency_agent_currencies')->onDelete('cascade');
            $table->integer('base_currency_id')->unsigned()->nullable();
            $table->integer('secondary_currency_id')->unsigned()->nullable();
            $table->integer('agent_id')->unsigned()->nullable();
            $table->decimal('buy_rate', 5,2)->default(0.00);
            $table->decimal('sell_rate', 5,2)->default(0.00);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('custom_currency_currencyrates');
    }
}
