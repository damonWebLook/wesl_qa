<?php namespace Custom\Currency\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateAgentCurrenciesTable extends Migration
{
    public function up()
    {
        Schema::create('custom_currency_agent_currencies', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('currency_id')->unsigned()->nullable();
            $table->foreign('currency_id')->references('id')->on('custom_currency_currencies');
            $table->integer('backend_user_id')->unsigned()->nullable();
            $table->foreign('backend_user_id')->references('id')->on('backend_users');
            $table->integer('base_currency_id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('custom_currency_agent_currencies');
    }
}
