<?php namespace Custom\Currency;

use Backend;
use System\Classes\PluginBase;

/**
 * currency Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'currency',
            'description' => 'No description provided yet...',
            'author'      => 'custom',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Custom\Currency\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        

        return [
            'custom.currency.some_permission' => [
                'tab' => 'currency',
                'label' => 'Some permission'
            ],

            'custom.currency.currencyrate.some_permission' => [
                'tab' => 'Currency Rate',
                'label' => 'Some permission'

            ],
            
            'custom.currency.currencyrate.new_button' => [
                'tab' => 'Currency Rate Hide Button',
                'label' => 'Hide the button to New button'
            
            ],
            
            'custom.currency.currencyrate.delete_button' => [
                'tab' => 'Currency Rate Delete Button',
                'label' => 'Hide the button to Delete button'
            
            ],
            
            'custom.currency.currencyrate.hidebutton.buying' => [
                'tab' => 'Currency Rate Buying Field',
                'label' => 'Hide the button to Delete button'
            
            ],
            
            'custom.currency.currencyrate.hidebutton.selling' => [
                'tab' => 'Currency Rate selling Field',
                'label' => 'Hide the button to Delete button'
            
            ],

            'custom.currency.addAgentCurrency' => [
                'label' => 'Add Agent Currency',
                'tab' => 'Currency'
            ],
            'custom.currency.addCurrency' => [
                'label' => 'Add Currency',
                'tab' => 'Currency',
                'roles' => ['none']
            ],

            'custom.currency.addSellerCurrency' => [
                'label' => 'Add Selling Currency',
                'tab' => 'Currency',
                'roles' => ['AGT']
            ]

            
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
     

        return [

            'currencyrate' => [
                'label'       => 'Currency Rate',
                'url'         => Backend::url('custom/currency/Currencyrates'),
                'icon'        => 'icon-eur',
                'permissions' => ['custom.currency.*'],
                'order'       => 600,
            ]

            
        ];

    }

    public function registerSettings()
    {
    
       return [
           'Currency'  => [
             'label' => 'Currency',
             'description' => 'Manage Currency',
             'category' => 'Currency',
             'icon' => 'icon-globe',
             'url' => Backend::url('custom/currency/currencies'),  
             'permissions' => ['custom.currency.addCurrency'],           
           ],
           'AgentCurrency'  => [
             'label' => 'Assign Currency',
             'description' => 'Assign Base Currency and Selling Currency',
             'category' => 'Currency',
             'icon' => 'icon-globe',
             'url' => Backend::url('custom/currency/agentcurrencies'),  
             'permissions' => ['custom.currency.addSellerCurrency'] ,
           
           ],
        //    'AssignSellingCurrency' => [
        //        'label' => 'Assign Selling Currency',
        //        'description' => 'Using this utility you can assign currencies for the available base currencies',
        //        'category' => 'Currency',
        //        'icon' => 'icon-money',
        //        'url' => Backend::url('custom/currency/agentcurrencies'),
        //        'permissions' => ['custom.currency.addSellerCurrency']
        //    ]

         
  
        ];
    } 


}
