<?php namespace Custom\Currency\Models;

use Model;
use  Custom\Currency\Models\Currency;
use Custom\Currency\Models\Currencyrate;
use Custom\Agents\Models\Agent;


/**
 * AgentCurrency Model
 */
class AgentCurrency extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'custom_currency_agent_currencies';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Validation rules for attributes
     */
    public $rules = [];

    /**
     * @var array Attributes to be cast to native types
     */
    protected $casts = [];

    /**
     * @var array Attributes to be cast to JSON
     */
    protected $jsonable = [];

    /**
     * @var array Attributes to be appended to the API representation of the model (ex. toArray())
     */
    protected $appends = [];

    /**
     * @var array Attributes to be removed from the API representation of the model (ex. toArray())
     */
    protected $hidden = [];

    /**
     * @var array Attributes to be cast to Argon (Carbon) instances
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [
        'currencyrate' =>
                    ['Custom\Currency\Currencyrate',
                    'table' => 'custom_currency_currencyrates',
                    'key' => 'agent_currency_id',     
     ]
    ];
    public $hasMany = [];
    public $belongsTo = [];

    // public $belongsToMany = [
    //     'currency' => [
    //         'Custom\Currency\Models\Currency',
    //         'table' => 'custom_currency_currencies',
    //         'key' => 'currency_id'
    //     ],
    //             ];

    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
    
    /**
     * Custom Function
     */
    public function getBaseCurrencyIdOptions(){
        /**
         * The list returns the currency array in the following format
         * [
         *   '1' => 'USD',
         *  '2' => 'EUR',
         *   '3' => 'LKR',
         *]
         */
        // $baseCurrencies = Currency::select('id','currency_name')->get()->toArray();
        // $count = count($baseCurrencies);
        // $currencyList = array();
        // for ($i = 0; $i <= $count-1; $i++){
        //         $currencyList[$baseCurrencies[$i]['id']] =$baseCurrencies[$i]['currency_name'];
        // }
        // return($currencyList);


        $baseCurrencies = Currency::all()->lists('currency_name','id');
        return $baseCurrencies;
     
    }
    public function getCurrencyIdOptions(){

        $baseCurrencies = Currency::all()->lists('currency_name','id');
        return $baseCurrencies;
     
    }
    public function getBackendUserIdOptions(){
        $agents = Agent::all()->lists('agent_name','user_id');
        
        return $agents;
    }

    public static function getBaseCurrency($id){
            /**
             * The list returns the currency array in the following format
             * [
             *   '1' => 'USD',
             *  '2' => 'EUR',
             *   '3' => 'LKR',
             *]
            */
        // $baseCurrencies = Currency::select('id','currency_name')->get()->toArray();
        // $count = count($baseCurrencies);
        // $currencyList = array();
        // for ($i = 0; $i <= $count-1; $i++){
        //         $currencyList[$baseCurrencies[$i]['id']] =$baseCurrencies[$i]['currency_name'];
        // }
        // return($currencyList);
        
        // $list = AgentCurrency::where('backend_user_id','=',$id)->get()->toArray();
        // $list = AgentCurrency::all()->where('backend_user_id','=',$id)->toArray();
            // $list = AgentCurrency::firstOrFail()->where('backend_user_id', $id)->currency;
            // dump($list);

        // foreach ($list->currency as $currency){
        //     echo $currency;
        // }

        $baseCurrencies = Currency:: all()->lists('currency_name','id');
        // dump($baseCurrencies);
        return $baseCurrencies;
    } 

    public static function getCurrency(){
        $baseCurrencies = Currency::all()->lists('currency_name','id');
        return $baseCurrencies;
    }

    public static function getAgentName(){
        return [
            '1' => 'Admin',
            '2' => 'Agent 2',
            '9' => 'Jhon',
            '10' => 'Sam'
        ];
    }
    /**
     * The below function 
     * 1. Update the table with backend_user_id
     * 2. Automatically adds record to CurrencyRate Table  by fetching agent_currency table 
     * latest id and base_currency_id
     * 
     */
    public function afterSave(){

        $data = self::latest('updated_at')->first()->toArray();   
        //Update Backend User ID
        $agentCurrency = self::where('id','=',$data['id'])->update(['backend_user_id' => \BackendAuth::getUser()->id]);
       
        $agentId = Agent::select('id')->where('user_id', $data['backend_user_id'])->get()->toArray();
        //Update CurrencyRate Table
        $table = new Currencyrate;
        $table->agent_currency_id = $data['id'];
        $table->base_currency_id = $data['base_currency_id'];
        $table->secondary_currency_id = $data['currency_id'];
        $table->agent_id = $agentId[0]['id'];
        $table->save();
    }

    
}
