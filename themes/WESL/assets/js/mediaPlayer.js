

// Wait for the DOM to be loaded before initialising the media player
document.addEventListener("DOMContentLoaded", function() { initialiseMediaPlayer(); }, false);

// Variables to store handles to various required elements
var mediaPlayer;
var playPauseBtn;
var muteBtn;
var progressBar;


function initialiseMediaPlayer() {
	// Get a handle to the player
	mediaPlayer = document.getElementById('media-video');
	
	// Get handles to each of the buttons and required elements
	// playPauseBtn = document.getElementById('play-pause-button');
	playPauseBtn = document.getElementById('playButton');


	// Hide the browser's default controls
	mediaPlayer.controls = false;

	

}

function togglePlayPause() {
	mediaPlayer.muted
	mediaPlayer.play();
	setTimeout(function(){$($("#proceed").show());}, getLength());
	document.getElementById('playButton').setAttribute("class", "hide");
}

function getLength(){
    var vid = document.getElementById("media-video");
    var time = document.getElementById("gettime").value /100;
	var length = vid.duration;
	var videoTimeOut  = (length*time)*1000;
	return videoTimeOut;
}
function getPoint(){
    var point = document.getElementById("getpoint").value;
    return point;

}
function addPoint(){
	document.getElementById("overlay").style.display = "block";
	mediaPlayer.pause();
	document.getElementById("point").setAttribute("value", getPoint()); 
	$.ajax({
		url: 'https://demo.wesl.lk/weslnew2/savePoint',
		method: 'post',
		data:{point : $('#point').serialize(), id: $('#getId').serialize()},
		// data:$('#getId').serialize(),
		success:function (data) {
			document.getElementById("textSuccess").style.display = "block";
			// setTimeout(window.location.reload(),9000000);
		},
		error: function (data){
			document.getElementById("textFail").style.display = "block";
			// setTimeout(window.location.reload(),9000000);
		}
	});
	
	document.getElementById("proceed").setAttribute("disabled","disabled");
	xHttp.open("POST", 'https://demo.wesl.lk/weslnew2/savePoint', true);
	xHttp.send();
	
 
	// lockVideo()

}

function showSuccessPopUp(){
	alert("You earned " + getPoint() + " points!");
}

function lockVideo(){
	document.getElementById("video1").setAttribute("class", "lock");
}

// Toggles the media player's mute and unmute status
function toggleMute() {
	var status = document.getElementById("mute-button").value;

	if (status == "false"){
		mediaPlayer.muted = true;
		document.getElementById("mute-button").setAttribute("value", "true");
		document.getElementById("mute-button").style.color = "#ef9231";

	}else{
		mediaPlayer.muted = false;
		document.getElementById("mute-button").setAttribute("value", "false");
		document.getElementById("mute-button").style.color = "";

	}
}

// Updates a button's title, innerHTML and CSS class to a certain value
function changeButtonType(btn, value) {
	btn.title = value;
	btn.innerHTML = value;
	btn.className = value;
}

// Loads a video item into the media player
function loadVideo() {
	for (var i = 0; i < arguments.length; i++) {
		var file = arguments[i].split('.');
		var ext = file[file.length - 1];
		// Check if this media can be played
		if (canPlayVideo(ext)) {
			// Reset the player, change the source file and load it
			resetPlayer();
			mediaPlayer.src = arguments[i];
			mediaPlayer.load();
			break;
		}
	}
}

// Checks if the browser can play this particular type of file or not
function canPlayVideo(ext) {
	var ableToPlay = mediaPlayer.canPlayType('video/' + ext);
	if (ableToPlay == '') return false;
	else return true;
}
