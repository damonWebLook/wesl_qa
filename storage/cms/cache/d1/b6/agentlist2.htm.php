<?php 
use Custom\Agents\Controllers\Agents;
use Custom\Agents\Models\Agent;
use Custom\Agents\Models\City;
use Custom\Currency\Models\Currency;
use Custom\Agents\Models\Country;
use Custom\Services\Models\Service;
class Cms5fb4abc36b00c033172980_4d54115662216936e453657de2f7f9f5Class extends Cms\Classes\PageCode
{


public function OnStart()
{

  $this['agents'] = Agents::getAllAgentList();
  $this['city'] = City::all();
  $this['country'] = Country::all();
  $this['service'] = Service::all();
  $this['currencies'] = Currency::all();
  $this['dummyData'] = $_GET;
}
public function onFilterSearch(){
  $agents = Agents::getAgentListByAttributes($_POST);
  if($agents){
    $this['agents'] = $agents;
  }else{
    $this['agents'] = Agents::getAllAgentList();
  }
}
}
