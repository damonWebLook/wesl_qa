<?php 
use Custom\Video\Controllers\VideoPoints;
use Custom\Video\Controllers\Videos;
use Custom\Video\Models\Video;
use October\Rain\Exception\ApplicationException;
class Cms5fb4b9f5922f0876126455_e3802c5885c8bc04e909cc1cdba9b9a3Class extends Cms\Classes\PageCode
{

public function onStart(){
    if (empty( $_SERVER['HTTP_REFERER'])){
        //trigger_error("You are not Authorized Please Go to Backend");
        //throw new ApplicationException('You must be logged in to do that!');
        App::abort(403, 'Unauthorized action.');
        
    }else{
        $id = $this->param('id');
        $this['ad'] = Video::where('advertiser_id', $id)->lists('name', 'id');
    }


}

public function onFilterSearch(){
    $adData = VideoPoints::getAdData($_POST['ad_id']);
    if($adData){
      $this['adData'] = $adData;
    }else{
      $this['adData'] = "Plase Select a Video";
    }
}
}
