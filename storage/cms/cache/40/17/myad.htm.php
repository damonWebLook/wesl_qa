<?php 
use Custom\Video\Controllers\VideoPoints;
use Custom\Video\Controllers\Videos;
use Custom\Video\Models\Video;
use October\Rain\Exception\ApplicationException;
class Cms5fb39e8c8876e684323070_22f49eb4096fa0dd0191dc68b4c9159aClass extends Cms\Classes\PageCode
{

public function onStart(){
    if (empty( $_SERVER['HTTP_REFERER'])){
        //trigger_error("You are not Authorized Please Go to Backend");
        //throw new ApplicationException('You must be logged in to do that!');
        App::abort(403, 'Unauthorized action.');
        
    }else{
        $id = $this->param('id');
        $this['ad'] = Video::where('advertiser_id', $id)->lists('name', 'id');
    }


}

public function onFilterSearch(){
    $adData = VideoPoints::getAdData($_POST['ad_id']);
    if($adData){
      $this['adData'] = $adData;
    }else{
      $this['adData'] = "Plase Select a Video";
    }
}
}
