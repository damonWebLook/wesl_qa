<?php 
use Custom\Video\Controllers\Videos;use Custom\Video\Controllers\VideoCategories;class Cms5fb33b4a25511972705109_73e671ec21140ce71e8431d4c6bd7a7aClass extends Cms\Classes\PageCode
{




public function onStart(){
    $this['categories'] = VideoCategories::getCategory();
    $this['videos'] = Videos::getAllVideos();
}
public function onFilterSearch(){
  $videos = Videos::getVideoByCategory($_POST['category_id']);
  if($videos){
    $this['videos'] = $videos;
  }else{
    $this['videos'] = Videos::getAllVideos();
  }
}
}
