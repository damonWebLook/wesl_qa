<?php 
use Custom\Agents\Controllers\Agents;use Custom\Agents\Models\Agent;use Custom\Agents\Models\country;use Custom\Agents\Models\city;use Custom\Currency\Models\Currencyrate;use Custom\Currency\Models\AgentCurrency;use Custom\Currency\Models\Currency;use Custom\Currency\Controllers\Currencyrates;use Custom\Services\Models\Service;use Custom\Services\Controllers\Services;use Custom\Rating\Controllers\Ratings;use Custom\Video\Controllers\Videos;class Cms5fb4b8ef08d25292651333_01c8e898ebe25336be04678de9553510Class extends Cms\Classes\PageCode
{

















public function onStart(){


  $id = $this->param('id');
    $agentId = Agent::find($id)->user_id;
  
  $userId = session()->get('user_id');
  $this['agent'] =  Agents::getAgentById($id);

  $this['baseCurrency'] = Currencyrates::getAgentBaseCurrency($agentId); 

  $this['calcurrency'] = Currencyrates::getAgentCurrency($agentId);  
  $this['services'] = Services::getfrontservices($id);
  //TODO: Get UserId Dynamically
  $this['ratingPermission'] = Ratings::checkPermissionRate($userId,$id);
  $this['avgRating'] = Ratings::getAvgRating($id);
  $this['showRating'] = Ratings::showRating($id);
  $this['currencies'] = Currencyrates::getFrontEndCurrencyRate($agentId, 1);
  $this['video'] = Videos::generateAd();
}

public function onFilterSearch(){
  $id = $this->param('id');
    $agentId = Agent::find($id)->user_id;
  $this['currencies'] = Currencyrates::getFrontEndCurrencyRate($agentId, $_POST['currency_id']); 

}

public function onChange(){
    $userId = session()->get('user_id');
  $id = $this->param('id');
  //TODO: Get UserId Dynamically
  $this['rating'] = Ratings::saveRating($userId, $id, $_POST['stars']);
  $this['hideRating'] = 1;
}

public function onSearch(){
  $id = $this->param('id');
   $agentId = Agent::find($id)->user_id;
  $this['calcresult'] = Currencyrates::calculateCurrency($_POST, $id);
  //dump(  $this['calcresult']);
}
public function onSubmit(){
    $userId = session()->get('user_id');
  $id = $this->param('id');
  //TODO: Get UserId Dynamically 1 is hardcoded user id
  $this['rating'] = Ratings::saveRating($userId,$id,$_POST);
}



}
