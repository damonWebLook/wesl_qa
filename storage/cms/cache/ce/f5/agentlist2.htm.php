<?php 
use Custom\Agents\Controllers\Agents;
use Custom\Agents\Models\Agent;
use Custom\Agents\Models\City;
use Custom\Currency\Models\Currency;
use Custom\Agents\Models\Country;
use Custom\Services\Models\Service;
class Cms5fb3590f9f42a677470132_625a00d9550de446609adb97e0f85c21Class extends Cms\Classes\PageCode
{


public function OnStart()
{

  $this['agents'] = Agents::getAllAgentList();
  $this['city'] = City::all();
  $this['country'] = Country::all();
  $this['service'] = Service::all();
  $this['currencies'] = Currency::all();
  $this['dummyData'] = $_GET;
}
public function onFilterSearch(){
  $agents = Agents::getAgentListByAttributes($_POST);
  if($agents){
    $this['agents'] = $agents;
  }else{
    $this['agents'] = Agents::getAllAgentList();
  }
}
}
