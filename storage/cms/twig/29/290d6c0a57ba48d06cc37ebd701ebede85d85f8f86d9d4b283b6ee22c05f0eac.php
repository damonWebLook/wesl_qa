<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /home/weslquicksite/public_html/weslnew/themes/WESL/pages/player.htm */
class __TwigTemplate_a56a0c477aafaa7b3f464be5f3e8a41274cac902b1998504ae86c665bae5c73f extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = array();
        $filters = array("escape" => 14, "media" => 18, "page" => 50, "theme" => 56);
        $functions = array();

        try {
            $this->sandbox->checkSecurity(
                [],
                ['escape', 'media', 'page', 'theme'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<style>
  #overlay{
    height: 100% !important;
  }
</style>
<div class=\"container\">
  <div class=\"col-lg-10 col-md-10 col-xs-12\">


  <div class=\"row\">

    <div class=\"media-wrapper-solo\" id=\"video1\">
      <div id=\"overlay\">
        <div id=\"textSuccess\">You earned ";
        // line 14
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["video"] ?? null), "point", [], "any", false, false, true, 14), 14, $this->source), "html", null, true);
        echo " points for the video</div>
        <div id=\"textFail\">You already earned maximum points for the video</div>
      </div>
      <div class=\"media-player\">
         <video src=\"..";
        // line 18
        echo $this->extensions['System\Twig\Extension']->mediaFilter($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["video"] ?? null), "video_path", [], "any", false, false, true, 18), 18, $this->source));
        echo "\" id=\"media-video\"></video>
      </div>
      <div id='media-controls'>
          <!-- <button id='play-pause-button' class='button play' title='play'
          onclick='togglePlayPause();' >Play</button> -->
          <div class=\"row\">        
              <div class=\"hideButton\">
              <button class=\"ad\" id=\"playButton\" onclick='togglePlayPause();'>
                <i class=\"fas fa-play-circle\"></i>
              </button>
              <button id='mute-button' class='mute' title='mute'
              onclick='toggleMute();' value=\"false\"><i class=\"fas fa-volume-mute\"></i></button>
 
              <div style=\"padding-top: 5px;\">
                <button type=\"submit\" id=\"proceed\" style=\"display: none;\" onclick=\"addPoint()\">Skip</button>
              </div>

          </div>
          
          </div>
          
          
       </div>
  </div>
  <input type=\"hidden\" name=\"id\" id=\"getId\" value=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["video"] ?? null), "id", [], "any", false, false, true, 42), 42, $this->source), "html", null, true);
        echo "\" name=\"videoId\">
  <input type=\"hidden\" id=\"getpoint\"  value=\"";
        // line 43
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["video"] ?? null), "point", [], "any", false, false, true, 43), 43, $this->source), "html", null, true);
        echo "\">
  <input type=\"hidden\" id=\"gettime\" value=\"";
        // line 44
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["video"] ?? null), "time", [], "any", false, false, true, 44), 44, $this->source), "html", null, true);
        echo "\">
  <input type=\"hidden\" id=\"point\" name=\"finalPoints\">

    </div>
  </div>
  <div class=\"col-lg-2 col-md-2 col-xs-12\">
    <a  href=\"";
        // line 50
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("video");
        echo "\"  class=\"button\">Return to Videos</a>
  </div>
</div> 



    <script src=\"";
        // line 56
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/mediaPlayer.js");
        echo "\"></script>";
    }

    public function getTemplateName()
    {
        return "/home/weslquicksite/public_html/weslnew/themes/WESL/pages/player.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  137 => 56,  128 => 50,  119 => 44,  115 => 43,  111 => 42,  84 => 18,  77 => 14,  62 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<style>
  #overlay{
    height: 100% !important;
  }
</style>
<div class=\"container\">
  <div class=\"col-lg-10 col-md-10 col-xs-12\">


  <div class=\"row\">

    <div class=\"media-wrapper-solo\" id=\"video1\">
      <div id=\"overlay\">
        <div id=\"textSuccess\">You earned {{ video.point }} points for the video</div>
        <div id=\"textFail\">You already earned maximum points for the video</div>
      </div>
      <div class=\"media-player\">
         <video src=\"..{{ video.video_path | media}}\" id=\"media-video\"></video>
      </div>
      <div id='media-controls'>
          <!-- <button id='play-pause-button' class='button play' title='play'
          onclick='togglePlayPause();' >Play</button> -->
          <div class=\"row\">        
              <div class=\"hideButton\">
              <button class=\"ad\" id=\"playButton\" onclick='togglePlayPause();'>
                <i class=\"fas fa-play-circle\"></i>
              </button>
              <button id='mute-button' class='mute' title='mute'
              onclick='toggleMute();' value=\"false\"><i class=\"fas fa-volume-mute\"></i></button>
 
              <div style=\"padding-top: 5px;\">
                <button type=\"submit\" id=\"proceed\" style=\"display: none;\" onclick=\"addPoint()\">Skip</button>
              </div>

          </div>
          
          </div>
          
          
       </div>
  </div>
  <input type=\"hidden\" name=\"id\" id=\"getId\" value=\"{{ video.id}}\" name=\"videoId\">
  <input type=\"hidden\" id=\"getpoint\"  value=\"{{ video.point }}\">
  <input type=\"hidden\" id=\"gettime\" value=\"{{ video.time }}\">
  <input type=\"hidden\" id=\"point\" name=\"finalPoints\">

    </div>
  </div>
  <div class=\"col-lg-2 col-md-2 col-xs-12\">
    <a  href=\"{{'video' |page}}\"  class=\"button\">Return to Videos</a>
  </div>
</div> 



    <script src=\"{{'assets/js/mediaPlayer.js'|theme}}\"></script>", "/home/weslquicksite/public_html/weslnew/themes/WESL/pages/player.htm", "");
    }
}
