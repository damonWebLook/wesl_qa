<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /home2/demowesl/public_html/weslnew2/themes/WESL/pages/survey.htm */
class __TwigTemplate_2799e7d63d9f357325c614a778b517b32f5be0db5cf50818102fee06b208b2df extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = array("for" => 35);
        $filters = array("escape" => 38);
        $functions = array();

        try {
            $this->sandbox->checkSecurity(
                ['for'],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"clearfix\"></div>
<!-- Header Container / End --> 

<!-- Titlebar -->
<div class=\"parallax titlebar\" data-background=\"images/listings-parallax.jpg\" data-color=\"rgba(48, 48, 48, 1)\" data-color-opacity=\"0.8\" data-img-width=\"800\" data-img-height=\"505\">
  <div id=\"titlebar\">
    <div class=\"container\">
      <div class=\"row\">
        <div class=\"col-md-12\">
          <h2>Survey</h2>
          <!-- Breadcrumbs -->
          <nav id=\"breadcrumbs\">
            <ul>
              <li><a href=\"index.html\">Home</a></li>
              <li>Agents Listings</li>
            </ul>
          </nav>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Content -->
<div class=\"container\">
  <div class=\"row\">
    <div class=\"col-md-12\">
      <div class=\"row\"> 
        <div class=\"utf-agents-container-area\"> 
          <!-- Agent -->
        
          <!-- Agent / End --> 
          
          <!-- Agent -->
          ";
        // line 35
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($context["survey"]);
        foreach ($context['_seq'] as $context["_key"] => $context["survey"]) {
            // line 36
            echo "          <div class=\"grid-item col-lg-3 col-md-4 col-sm-6 col-xs-12\">
            <div class=\"agent\">
              <div class=\"utf-agent-avatar\"> <a href=\"surveyview/";
            // line 38
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["survey"], "id", [], "any", false, false, true, 38), 38, $this->source), "html", null, true);
            echo "\">  <span class=\"view-profile-btn\">View Profile</span> </a> </div>
              <div class=\"utf-agent-content\">
                <div class=\"utf-agent-name\">
                  <h4><a href=\"surveyview/";
            // line 41
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["survey"], "id", [], "any", false, false, true, 41), 41, $this->source), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["survey"], "title_name", [], "any", false, false, true, 41), 41, $this->source), "html", null, true);
            echo "</a></h4>
            
                </div>                  
              </div>
            </div>
          </div>
          ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['survey'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 48
        echo "          <!-- Agent / End --> 
          
      
          <!-- Agent / End -->  \t
        </div>
        <!-- Agents Container / End -->           
      </div>
    </div>
    <div class=\"col-md-12\">
      <div class=\"clearfix\"></div>
      <!-- Pagination -->
      <!-- <div class=\"utf-pagination-container margin-top-20\">
        <nav class=\"pagination\">
          <ul>
            <li><a href=\"#\"><i class=\"fa fa-angle-left\"></i></a></li>
            <li><a href=\"#\" class=\"current-page\">1</a></li>
            <li><a href=\"#\">2</a></li>
            <li><a href=\"#\">3</a></li>
            <li class=\"blank\">...</li>
            <li><a href=\"#\">10</a></li>
            <li><a href=\"#\"><i class=\"fa fa-angle-right\"></i></a></li>
          </ul>
        </nav>          
      </div> -->
      <!-- Pagination / End --> 
    </div>
  </div>
</div>";
    }

    public function getTemplateName()
    {
        return "/home2/demowesl/public_html/weslnew2/themes/WESL/pages/survey.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  127 => 48,  112 => 41,  106 => 38,  102 => 36,  98 => 35,  62 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "/home2/demowesl/public_html/weslnew2/themes/WESL/pages/survey.htm", "");
    }
}
