<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /home/weslquicksite/public_html/weslnew/themes/WESL/pages/agent_login.htm */
class __TwigTemplate_8524079e23706691d1711ef35a83b2e885a833c0d84526417ac74404beb036b8 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = array();
        $filters = array("page" => 28);
        $functions = array();

        try {
            $this->sandbox->checkSecurity(
                [],
                ['page'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"clearfix\"></div>
<!-- Header Container / End --> 

<!-- Titlebar -->
<div class=\"parallax titlebar\" data-background=\"images/listings-parallax.jpg\" data-color=\"rgba(48, 48, 48, 1)\" data-color-opacity=\"0.8\" data-img-width=\"800\" data-img-height=\"505\">
  <div id=\"titlebar\">
    <div class=\"container\">
      <div class=\"row\">
        <div class=\"col-md-12\">
          <h2>Agent Log In</h2>
          <!-- Breadcrumbs -->
         
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Contact --> 
<div class=\"container\">
  <div class=\"row\">
    <div class=\"col-md-6 col-md-offset-3\">
      <div class=\"my-account\">
        <div class=\"tabs-container\"> 
          <!-- Login -->
      <div class=\"utf-welcome-text-item\">
      <h3>Welcome Back Sign in to Continue</h3>
      <span> Want to be Agent <a href=\"";
        // line 28
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("agentregister");
        echo "\">Click Here</a></span> 
      </div>
            <form method=\"post\" data-request=\"onSign\" class=\"login\">
              <div class=\"form-row form-row-wide\">
                  <input type=\"text\" class=\"input-text\" name=\"login\" id=\"email\" placeholder=\"Login\" value=\"\" />
              </div>
              <div class=\"form-row form-row-wide\">
                  <input class=\"input-text\" type=\"password\" name=\"password\" placeholder=\"Password\" id=\"password\"/>
              </div>
              <div class=\"form-row\">
                <div class=\"checkbox margin-top-10 margin-bottom-10\">
        <input type=\"checkbox\" id=\"two-step\">
        <label for=\"two-step\"><span class=\"checkbox-icon\"></span> Remember Me</label>
        </div>
        <a class=\"#\" href=\"#\">Forgot Password?</a>\t
              </div>
      <input type=\"submit\" class=\"button full-width border margin-top-10\" name=\"login\" value=\"Login\" />
            </form>
        </div>
      </div>
    </div>
  </div>
</div>
<div>
  <div class=\"utf-welcome-text-item\">

  </div>
 
</div>

<!-- Container / End -->";
    }

    public function getTemplateName()
    {
        return "/home/weslquicksite/public_html/weslnew/themes/WESL/pages/agent_login.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  91 => 28,  62 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"clearfix\"></div>
<!-- Header Container / End --> 

<!-- Titlebar -->
<div class=\"parallax titlebar\" data-background=\"images/listings-parallax.jpg\" data-color=\"rgba(48, 48, 48, 1)\" data-color-opacity=\"0.8\" data-img-width=\"800\" data-img-height=\"505\">
  <div id=\"titlebar\">
    <div class=\"container\">
      <div class=\"row\">
        <div class=\"col-md-12\">
          <h2>Agent Log In</h2>
          <!-- Breadcrumbs -->
         
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Contact --> 
<div class=\"container\">
  <div class=\"row\">
    <div class=\"col-md-6 col-md-offset-3\">
      <div class=\"my-account\">
        <div class=\"tabs-container\"> 
          <!-- Login -->
      <div class=\"utf-welcome-text-item\">
      <h3>Welcome Back Sign in to Continue</h3>
      <span> Want to be Agent <a href=\"{{'agentregister'|page}}\">Click Here</a></span> 
      </div>
            <form method=\"post\" data-request=\"onSign\" class=\"login\">
              <div class=\"form-row form-row-wide\">
                  <input type=\"text\" class=\"input-text\" name=\"login\" id=\"email\" placeholder=\"Login\" value=\"\" />
              </div>
              <div class=\"form-row form-row-wide\">
                  <input class=\"input-text\" type=\"password\" name=\"password\" placeholder=\"Password\" id=\"password\"/>
              </div>
              <div class=\"form-row\">
                <div class=\"checkbox margin-top-10 margin-bottom-10\">
        <input type=\"checkbox\" id=\"two-step\">
        <label for=\"two-step\"><span class=\"checkbox-icon\"></span> Remember Me</label>
        </div>
        <a class=\"#\" href=\"#\">Forgot Password?</a>\t
              </div>
      <input type=\"submit\" class=\"button full-width border margin-top-10\" name=\"login\" value=\"Login\" />
            </form>
        </div>
      </div>
    </div>
  </div>
</div>
<div>
  <div class=\"utf-welcome-text-item\">

  </div>
 
</div>

<!-- Container / End -->", "/home/weslquicksite/public_html/weslnew/themes/WESL/pages/agent_login.htm", "");
    }
}
