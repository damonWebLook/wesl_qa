<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /home/weslquicksite/public_html/weslnew/themes/WESL/pages/userregister.htm */
class __TwigTemplate_b24814bd655dabd942cb0e57fcf3c6e122aaa3a3f1a01a8d751c0ce21b0940e6 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = array();
        $filters = array("theme" => 4, "page" => 16);
        $functions = array("form_open" => 13, "form_close" => 47);

        try {
            $this->sandbox->checkSecurity(
                [],
                ['theme', 'page'],
                ['form_open', 'form_close']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"container-fluid imagewidth spaceweight\">
  <div class=\"row\">
    <div class=\"col-12 col-md-6\">
     <img  class=\"imagesize\" src=\"";
        // line 4
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/weslportal.png");
        echo "\">
    </div>
    <div class=\"col colform\">
        <div class=\"row\">
          <div class=\"col-12 col-md-10 panelcenter\">
            <div class=\"my-account\">
              <div class=\"tabs-container\"> 
                <!-- Login -->
            <div class=\"utf-welcome-text-item\">
              ";
        // line 13
        echo call_user_func_array($this->env->getFunction('form_open')->getCallable(), ["open", ["request" => "onRegister"]]);
        echo "
             
            <h3>Welcome Back Sign in to Continue</h3>
            <span> Have an Account? <a href=\"";
        // line 16
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("userlogin");
        echo "\">Sign In!</a></span> 
            </div>
                  <div class=\"form-row form-row-wide\">
                    <input type=\"email\" class=\"input-text\" name=\"email\" id=\"email\" placeholder=\"Email Address\" value=\"\" id =\"validationDefault01\" />
                  </div>
                  <div class=\"form-row form-row-wide\">
                    <input type=\"text\" class=\"input-text\" name=\"login\" id=\"email\" placeholder=\"Login\" value=\"\" />
                  </div>
                  <div class=\"form-row form-row-wide\">
                    <input type=\"number\" class=\"input-text\" name=\"birth\" id=\"birth\" placeholder=\"Date of Birth\" value=\"\" />
                  </div>
                  <div class=\"form-row form-row-wide\">
                    <input type=\"number\" class=\"input-text\" name=\"contactno\" id=\"contact\" placeholder=\"Contact No\" value=\"\" />
                  </div>
                  <div class=\"form-row form-row-wide\">
                    <input type=\"password\" class=\"input-text\" name=\"password\" id=\"password\" placeholder=\"password\" value=\"\" />
                  </div>
                  <div class=\"form-row form-row-wide\">
                    <input type=\"password\" class=\"input-text\" name=\"password\" id=\"password\" placeholder=\"Confirm Password\" value=\"\" />
                  </div>
                
               
                  <div class=\"form-row\">
                      <div class=\"checkbox margin-top-10 margin-bottom-10\">
              <!-- <input type=\"checkbox\" id=\"two-step\">
              <label for=\"two-step\"><span class=\"checkbox-icon\"></span> Remember Me</label> -->
              </div>
              <!-- <a class=\"lost_password\" href=\"forgot_password.html\">Forgot Password?</a>\t -->
                    </div>
            <input type=\"submit\" class=\"button full-width border margin-top-10\"  value=\"Create Account\" />

        ";
        // line 47
        echo call_user_func_array($this->env->getFunction('form_close')->getCallable(), ["close"]);
        echo "
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>";
    }

    public function getTemplateName()
    {
        return "/home/weslquicksite/public_html/weslnew/themes/WESL/pages/userregister.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  119 => 47,  85 => 16,  79 => 13,  67 => 4,  62 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"container-fluid imagewidth spaceweight\">
  <div class=\"row\">
    <div class=\"col-12 col-md-6\">
     <img  class=\"imagesize\" src=\"{{'assets/img/weslportal.png' |theme }}\">
    </div>
    <div class=\"col colform\">
        <div class=\"row\">
          <div class=\"col-12 col-md-10 panelcenter\">
            <div class=\"my-account\">
              <div class=\"tabs-container\"> 
                <!-- Login -->
            <div class=\"utf-welcome-text-item\">
              {{ form_open({  request: 'onRegister' }) }}
             
            <h3>Welcome Back Sign in to Continue</h3>
            <span> Have an Account? <a href=\"{{'userlogin' | page}}\">Sign In!</a></span> 
            </div>
                  <div class=\"form-row form-row-wide\">
                    <input type=\"email\" class=\"input-text\" name=\"email\" id=\"email\" placeholder=\"Email Address\" value=\"\" id =\"validationDefault01\" />
                  </div>
                  <div class=\"form-row form-row-wide\">
                    <input type=\"text\" class=\"input-text\" name=\"login\" id=\"email\" placeholder=\"Login\" value=\"\" />
                  </div>
                  <div class=\"form-row form-row-wide\">
                    <input type=\"number\" class=\"input-text\" name=\"birth\" id=\"birth\" placeholder=\"Date of Birth\" value=\"\" />
                  </div>
                  <div class=\"form-row form-row-wide\">
                    <input type=\"number\" class=\"input-text\" name=\"contactno\" id=\"contact\" placeholder=\"Contact No\" value=\"\" />
                  </div>
                  <div class=\"form-row form-row-wide\">
                    <input type=\"password\" class=\"input-text\" name=\"password\" id=\"password\" placeholder=\"password\" value=\"\" />
                  </div>
                  <div class=\"form-row form-row-wide\">
                    <input type=\"password\" class=\"input-text\" name=\"password\" id=\"password\" placeholder=\"Confirm Password\" value=\"\" />
                  </div>
                
               
                  <div class=\"form-row\">
                      <div class=\"checkbox margin-top-10 margin-bottom-10\">
              <!-- <input type=\"checkbox\" id=\"two-step\">
              <label for=\"two-step\"><span class=\"checkbox-icon\"></span> Remember Me</label> -->
              </div>
              <!-- <a class=\"lost_password\" href=\"forgot_password.html\">Forgot Password?</a>\t -->
                    </div>
            <input type=\"submit\" class=\"button full-width border margin-top-10\"  value=\"Create Account\" />

        {{ form_close()}}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>", "/home/weslquicksite/public_html/weslnew/themes/WESL/pages/userregister.htm", "");
    }
}
