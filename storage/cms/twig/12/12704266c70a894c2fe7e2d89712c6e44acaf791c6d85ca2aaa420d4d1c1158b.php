<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /home2/demowesl/public_html/weslnew2/themes/WESL/pages/myad.htm */
class __TwigTemplate_060f8bed4defb363cccddd82c2a5419fe22c7633a75eed9bb519bb214cbe4689 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = array("for" => 30, "partial" => 44);
        $filters = array("escape" => 31);
        $functions = array("form_ajax" => 23, "form_close" => 39);

        try {
            $this->sandbox->checkSecurity(
                ['for', 'partial'],
                ['escape'],
                ['form_ajax', 'form_close']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<style>
    table {
      font-family: arial, sans-serif;
      border-collapse: collapse;
      width: 100%;
    }
    
    td, th {
      border: 1px solid #dddddd;
      text-align: left;
      padding: 8px;
    }
    
    tr:nth-child(even) {
      background-color: #dddddd;
    }
    </style>
    <pre></pre>
<div class=\"container\">
    <div class=\"col-lg-12 col-md-12 col-xs-12\">
           <!-- sidebar -->
     <div class=\"col-lg-2 col-md-2 col-xs-12\">
        ";
        // line 23
        echo call_user_func_array($this->env->getFunction('form_ajax')->getCallable(), ["ajax", "onFilterSearch", ["class" => "category", "update" => ["video/adData" => "#adData"]]]);
        echo "
          <div class=\"row with-forms\"> 
            <div class=\"row with-forms\"> 
              <div class=\"col-md-12\">
                <label for=\"ad_id\">Advertisement Name</label>
                <select  name=\"ad_id\"  onchange=\"getFilterList()\" class=\"chosen-select\">
                  <option>Name</option>
                  ";
        // line 30
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["ad"] ?? null));
        foreach ($context['_seq'] as $context["key"] => $context["value"]) {
            // line 31
            echo "                  <option value=\"";
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed($context["key"], 31, $this->source), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed($context["value"], 31, $this->source), "html", null, true);
            echo "</option>
                  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['value'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 33
        echo "                                                  
                </select>
              </div>
            </div>
                  
          </div>
        ";
        // line 39
        echo call_user_func_array($this->env->getFunction('form_close')->getCallable(), ["close"]);
        echo "
       </div>
       <!-- Data  -->

       <div class=\"col-lg-10 col-md-10 col-xs-12\">
        ";
        // line 44
        $context['__cms_partial_params'] = [];
        $context['__cms_partial_params']['adData'] = ($context["adData"] ?? null)        ;
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("video/adData"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 45
        echo "       </div>
    </div>

</div>

<script>

    function getFilterList(){
      setTimeout(function(){
          \$('.category').request('onFilterSearch')
      }),1000;
    }
  </script>";
    }

    public function getTemplateName()
    {
        return "/home2/demowesl/public_html/weslnew2/themes/WESL/pages/myad.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  132 => 45,  127 => 44,  119 => 39,  111 => 33,  100 => 31,  96 => 30,  86 => 23,  62 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<style>
    table {
      font-family: arial, sans-serif;
      border-collapse: collapse;
      width: 100%;
    }
    
    td, th {
      border: 1px solid #dddddd;
      text-align: left;
      padding: 8px;
    }
    
    tr:nth-child(even) {
      background-color: #dddddd;
    }
    </style>
    <pre></pre>
<div class=\"container\">
    <div class=\"col-lg-12 col-md-12 col-xs-12\">
           <!-- sidebar -->
     <div class=\"col-lg-2 col-md-2 col-xs-12\">
        {{form_ajax('onFilterSearch',{ class: 'category', update: { 'video/adData': '#adData'}}) }}
          <div class=\"row with-forms\"> 
            <div class=\"row with-forms\"> 
              <div class=\"col-md-12\">
                <label for=\"ad_id\">Advertisement Name</label>
                <select  name=\"ad_id\"  onchange=\"getFilterList()\" class=\"chosen-select\">
                  <option>Name</option>
                  {% for key, value in ad %}
                  <option value=\"{{key}}\">{{value}}</option>
                  {% endfor %}
                                                  
                </select>
              </div>
            </div>
                  
          </div>
        {{form_close()}}
       </div>
       <!-- Data  -->

       <div class=\"col-lg-10 col-md-10 col-xs-12\">
        {% partial 'video/adData' adData=adData %}
       </div>
    </div>

</div>

<script>

    function getFilterList(){
      setTimeout(function(){
          \$('.category').request('onFilterSearch')
      }),1000;
    }
  </script>", "/home2/demowesl/public_html/weslnew2/themes/WESL/pages/myad.htm", "");
    }
}
