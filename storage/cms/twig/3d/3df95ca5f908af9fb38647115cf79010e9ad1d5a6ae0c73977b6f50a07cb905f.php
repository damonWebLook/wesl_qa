<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /home2/demowesl/public_html/weslnew2/themes/WESL/pages/serviceview.htm */
class __TwigTemplate_5379b63adeebb7839701ddb10bb9d9cfd782711b494766784a7f48005a6e8627 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = array();
        $filters = array("escape" => 7);
        $functions = array();

        try {
            $this->sandbox->checkSecurity(
                [],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!-- Titlebar -->
<div class=\"parallax titlebar\" data-background=\"images/listings-parallax.jpg\" data-color=\"rgba(48, 48, 48, 1)\" data-color-opacity=\"0.8\" data-img-width=\"800\" data-img-height=\"505\">
    <div id=\"titlebar\">
      <div class=\"container\">
        <div class=\"row\">
          <div class=\"col-md-12\">
            <h2>";
        // line 7
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["service"] ?? null), "service_name", [], "any", false, false, true, 7), 7, $this->source), "html", null, true);
        echo "</h2>
            <!-- Breadcrumbs -->
            <nav id=\"breadcrumbs\">
              <ul>
                <li><a href=\"../home\">Home</a></li>
                 <li><a href=\"../services\">Services</a></li>
                <li>";
        // line 13
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["service"] ?? null), "service_name", [], "any", false, false, true, 13), 13, $this->source), "html", null, true);
        echo "</li>
              </ul>
            </nav>
          </div>
        </div>
      </div>
    </div>
</div>


  <!-- Content -->
  <div class=\"container\" id=\"Content\">
    <div class=\"row\">
      <div class=\"col-md-12\"> 
        <!-- Service -->
        <div class=\"agent agents-profile agency margin-bottom-40\">
              <span class=\"utf-agent-avatar\">
                <img src=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["service"] ?? null), "service_image", [], "any", false, false, true, 30), "path", [], "any", false, false, true, 30), 30, $this->source), "html", null, true);
        echo "\" alt=\"\">
                </span>
          <div class=\"utf-agent-content\">
            <div class=\"utf-agent-name\">
              <h4><a href=\"\">";
        // line 34
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["service"] ?? null), "service_name", [], "any", false, false, true, 34), 34, $this->source), "html", null, true);
        echo " </a></h4>
\t\t\t  <span> ";
        // line 35
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["service"] ?? null), "address", [], "any", false, false, true, 35), 35, $this->source), "html", null, true);
        echo " </span>
            <ul class=\"utf-agent-contact-details\">
\t\t\t\t      <li><i class=\"icon-feather-smartphone\"></i>";
        // line 37
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["service"] ?? null), "contactno", [], "any", false, false, true, 37), 37, $this->source), "html", null, true);
        echo "</li>
\t\t\t\t      <li><i class=\"icon-material-outline-email\"></i><a href=\"#\">";
        // line 38
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["service"] ?? null), "email", [], "any", false, false, true, 38), 38, $this->source), "html", null, true);
        echo "</a></li>
\t\t\t      </ul> 

      </div>
      

\t\t\t<div class=\"clearfix\"></div>
                <span>
                    ";
        // line 46
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["service"] ?? null), "service_descripiton", [], "any", false, false, true, 46), 46, $this->source), "html", null, true);
        echo "
                </span>
                <br>
                <span>
                    ";
        // line 50
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["service"] ?? null), "openhours", [], "any", false, false, true, 50), 50, $this->source), "html", null, true);
        echo "
                </span>
          </div>
        </div>
\t  </div>
    </div>
  </div>";
    }

    public function getTemplateName()
    {
        return "/home2/demowesl/public_html/weslnew2/themes/WESL/pages/serviceview.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  137 => 50,  130 => 46,  119 => 38,  115 => 37,  110 => 35,  106 => 34,  99 => 30,  79 => 13,  70 => 7,  62 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!-- Titlebar -->
<div class=\"parallax titlebar\" data-background=\"images/listings-parallax.jpg\" data-color=\"rgba(48, 48, 48, 1)\" data-color-opacity=\"0.8\" data-img-width=\"800\" data-img-height=\"505\">
    <div id=\"titlebar\">
      <div class=\"container\">
        <div class=\"row\">
          <div class=\"col-md-12\">
            <h2>{{ service.service_name}}</h2>
            <!-- Breadcrumbs -->
            <nav id=\"breadcrumbs\">
              <ul>
                <li><a href=\"../home\">Home</a></li>
                 <li><a href=\"../services\">Services</a></li>
                <li>{{ service.service_name}}</li>
              </ul>
            </nav>
          </div>
        </div>
      </div>
    </div>
</div>


  <!-- Content -->
  <div class=\"container\" id=\"Content\">
    <div class=\"row\">
      <div class=\"col-md-12\"> 
        <!-- Service -->
        <div class=\"agent agents-profile agency margin-bottom-40\">
              <span class=\"utf-agent-avatar\">
                <img src=\"{{service.service_image.path}}\" alt=\"\">
                </span>
          <div class=\"utf-agent-content\">
            <div class=\"utf-agent-name\">
              <h4><a href=\"\">{{ service.service_name }} </a></h4>
\t\t\t  <span> {{ service.address}} </span>
            <ul class=\"utf-agent-contact-details\">
\t\t\t\t      <li><i class=\"icon-feather-smartphone\"></i>{{ service.contactno}}</li>
\t\t\t\t      <li><i class=\"icon-material-outline-email\"></i><a href=\"#\">{{ service.email}}</a></li>
\t\t\t      </ul> 

      </div>
      

\t\t\t<div class=\"clearfix\"></div>
                <span>
                    {{ service.service_descripiton }}
                </span>
                <br>
                <span>
                    {{ service.openhours }}
                </span>
          </div>
        </div>
\t  </div>
    </div>
  </div>", "/home2/demowesl/public_html/weslnew2/themes/WESL/pages/serviceview.htm", "");
    }
}
