<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /home2/demowesl/public_html/weslnew2/themes/WESL/pages/player.htm */
class __TwigTemplate_6ab571ade2b65b40987a014831eb27c8d3feb9c7c7ac07c42b25c5a97056b8c3 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = array();
        $filters = array("escape" => 12, "media" => 38, "page" => 70, "theme" => 76);
        $functions = array();

        try {
            $this->sandbox->checkSecurity(
                [],
                ['escape', 'media', 'page', 'theme'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<style>
  #overlay{
    height: 100% !important;
  }
</style>
 <!-- Titlebar -->
<div class=\"parallax titlebar\" data-background=\"images/listings-parallax.jpg\" data-color=\"rgba(48, 48, 48, 1)\" data-color-opacity=\"0.8\" data-img-width=\"800\" data-img-height=\"505\">
    <div id=\"titlebar\">
      <div class=\"container\">
        <div class=\"row\">
          <div class=\"col-md-12\">
            <h2>";
        // line 12
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["video"] ?? null), "name", [], "any", false, false, true, 12), 12, $this->source), "html", null, true);
        echo "</h2>
            <!-- Breadcrumbs -->
            <nav id=\"breadcrumbs\">
              <ul>
                <li><a href=\"../home\">Home</a></li>
                 <li><a href=\"../video\">Videos</a></li>
                <li>";
        // line 18
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["video"] ?? null), "name", [], "any", false, false, true, 18), 18, $this->source), "html", null, true);
        echo "</li>
              </ul>
            </nav>
          </div>
        </div>
      </div>
    </div>
</div>
<div class=\"container\">
  <div class=\"col-lg-10 col-md-10 col-xs-12\">


  <div class=\"row\">

    <div class=\"media-wrapper-solo\" id=\"video1\">
      <div id=\"overlay\">
        <div id=\"textSuccess\">You earned ";
        // line 34
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["video"] ?? null), "point", [], "any", false, false, true, 34), 34, $this->source), "html", null, true);
        echo " points for the video</div>
        <div id=\"textFail\">You already earned maximum points for the video</div>
      </div>
      <div class=\"media-player\">
         <video src=\"..";
        // line 38
        echo $this->extensions['System\Twig\Extension']->mediaFilter($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["video"] ?? null), "video_path", [], "any", false, false, true, 38), 38, $this->source));
        echo "\" id=\"media-video\"></video>
      </div>
      <div id='media-controls'>
          <!-- <button id='play-pause-button' class='button play' title='play'
          onclick='togglePlayPause();' >Play</button> -->
          <div class=\"row\">        
              <div class=\"hideButton\">
              <button class=\"ad\" id=\"playButton\" onclick='togglePlayPause();'>
                <i class=\"fas fa-play-circle\"></i>
              </button>
              <button id='mute-button' class='mute' title='mute'
              onclick='toggleMute();' value=\"false\"><i class=\"fas fa-volume-mute\"></i></button>
 
              <div style=\"padding-top: 5px;\">
                <button type=\"submit\" id=\"proceed\" style=\"display: none;\" onclick=\"addPoint()\">Skip</button>
              </div>

          </div>
          
          </div>
          
          
       </div>
  </div>
  <input type=\"hidden\" name=\"id\" id=\"getId\" value=\"";
        // line 62
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["video"] ?? null), "id", [], "any", false, false, true, 62), 62, $this->source), "html", null, true);
        echo "\" name=\"videoId\">
  <input type=\"hidden\" id=\"getpoint\"  value=\"";
        // line 63
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["video"] ?? null), "point", [], "any", false, false, true, 63), 63, $this->source), "html", null, true);
        echo "\">
  <input type=\"hidden\" id=\"gettime\" value=\"";
        // line 64
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["video"] ?? null), "time", [], "any", false, false, true, 64), 64, $this->source), "html", null, true);
        echo "\">
  <input type=\"hidden\" id=\"point\" name=\"finalPoints\">

    </div>
  </div>
  <div class=\"col-lg-2 col-md-2 col-xs-12\">
    <a  href=\"";
        // line 70
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("video");
        echo "\"  class=\"button\">Return to Videos</a>
  </div>
</div> 



    <script src=\"";
        // line 76
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/mediaPlayer.js");
        echo "\"></script>";
    }

    public function getTemplateName()
    {
        return "/home2/demowesl/public_html/weslnew2/themes/WESL/pages/player.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  163 => 76,  154 => 70,  145 => 64,  141 => 63,  137 => 62,  110 => 38,  103 => 34,  84 => 18,  75 => 12,  62 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "/home2/demowesl/public_html/weslnew2/themes/WESL/pages/player.htm", "");
    }
}
