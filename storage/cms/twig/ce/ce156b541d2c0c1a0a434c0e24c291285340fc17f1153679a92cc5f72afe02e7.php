<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /home/weslquicksite/public_html/weslnew/themes/WESL/partials/agents/agentlisting.htm */
class __TwigTemplate_86894b75f13702381cdd1c97bb1bbcfc9bc57eaca8c8b333465d0ab0b4ddccab extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = array("for" => 3, "if" => 9);
        $filters = array("escape" => 7, "theme" => 15);
        $functions = array();

        try {
            $this->sandbox->checkSecurity(
                ['for', 'if'],
                ['escape', 'theme'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div id=\"agent_list\">

";
        // line 3
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["agents"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["agent"]) {
            // line 4
            echo "
      <div class=\"col-md-6\"> 

        <div class=\"agent agents-profile agency\"> <a href=\"profileview/";
            // line 7
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["agent"], "agent", [], "any", false, false, true, 7), "id", [], "any", false, false, true, 7), 7, $this->source), "html", null, true);
            echo "\" class=\"utf-agent-avatar\"> 
      
          ";
            // line 9
            if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["agent"], "agent_user", [], "any", false, false, true, 9), "avatar", [], "any", false, false, true, 9), "path", [], "any", false, false, true, 9)) {
                // line 10
                echo "
            <img src=\"";
                // line 11
                echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["agent"], "agent_user", [], "any", false, false, true, 11), "avatar", [], "any", false, false, true, 11), "path", [], "any", false, false, true, 11), 11, $this->source), "html", null, true);
                echo "\" alt=\"\">
          
          ";
            } else {
                // line 13
                echo " 
          
            <img src=\"";
                // line 15
                echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/avaterplaceholder.jpg");
                echo "\" alt=\"\">
           
          ";
            }
            // line 17
            echo " 
         </a>
          <div class=\"utf-agent-content\">
            <div class=\"utf-agent-name\">
\t\t\t  <p class=\"text-alt\">Agent</p>
              <h4><a href=\"profileview/";
            // line 22
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["agent"], "agent", [], "any", false, false, true, 22), "id", [], "any", false, false, true, 22), 22, $this->source), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["agent"], "agent", [], "any", false, false, true, 22), "first_name", [], "any", false, false, true, 22), 22, $this->source), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["agent"], "agent", [], "any", false, false, true, 22), "last_name", [], "any", false, false, true, 22), 22, $this->source), "html", null, true);
            echo " </a></h4>
              <ul class=\"utf-agent-contact-details\">
\t\t\t      <li><i class=\"icon-material-outline-business\"></i>";
            // line 24
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["agent"], "country", [], "any", false, false, true, 24), "name", [], "any", false, false, true, 24), 24, $this->source), "html", null, true);
            echo "</li>
\t\t\t      <li><i class=\"icon-material-outline-business\"></i>";
            // line 25
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["agent"], "city", [], "any", false, false, true, 25), "name", [], "any", false, false, true, 25), 25, $this->source), "html", null, true);
            echo "</li>
          <li><i class=\"icon-feather-smartphone\"></i>";
            // line 26
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["agent"], "agent", [], "any", false, false, true, 26), "agent_contact", [], "any", false, false, true, 26), 26, $this->source), "html", null, true);
            echo "</li>
          
          <li><i class=\"icon-material-outline-email\"></i><a href=\"#\">";
            // line 28
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["agent"], "agent", [], "any", false, false, true, 28), "email", [], "any", false, false, true, 28), 28, $this->source), "html", null, true);
            echo "</a></li>

          
         
          
        </ul> 
        Services
        <br>
           ";
            // line 36
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["agent"], "currencies", [], "any", false, false, true, 36));
            foreach ($context['_seq'] as $context["_key"] => $context["currency"]) {
                // line 37
                echo "           
            ";
                // line 38
                echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["currency"], "currency_name", [], "any", false, false, true, 38), 38, $this->source), "html", null, true);
                echo "
           
           ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['currency'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 41
            echo "     
       
\t\t\t  <ul class=\"utf-social-icons\">
\t\t\t\t  <li><a class=\"facebook\" href=\"#\"><i class=\"icon-facebook\"></i></a></li>
\t\t\t\t  <li><a class=\"twitter\" href=\"#\"><i class=\"icon-twitter\"></i></a></li>
\t\t\t\t  <li><a class=\"linkedin\" href=\"#\"><i class=\"icon-linkedin\"></i></a></li>
\t\t\t\t  <li><a class=\"instagram\" href=\"#\"><i class=\"icon-instagram\"></i></a></li>
\t\t\t\t  <li><a class=\"gplus\" href=\"#\"><i class=\"icon-gplus\"></i></a></li>
\t\t\t  </ul>
\t\t\t</div>
\t\t\t<div class=\"clearfix\"></div>
            <p>";
            // line 52
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["agent"], "agent", [], "any", false, false, true, 52), "bio", [], "any", false, false, true, 52), 52, $this->source), "html", null, true);
            echo "</p>                      
          </div>
        </div>
     

       
    </div>\t
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['agent'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 60
        echo "</div>
     <!-- <div class=\"col-md-12\">
        <div class=\"clearfix\"></div>
         Pagination --> 
        <!-- <div class=\"utf-pagination-container margin-top-20\">
          <nav class=\"pagination\">
            <ul>
              <li><a href=\"#\"><i class=\"fa fa-angle-left\"></i></a></li>
\t\t\t  <li><a href=\"#\" class=\"current-page\">1</a></li>
              <li><a href=\"#\">2</a></li>
              <li><a href=\"#\">3</a></li>
              <li class=\"blank\">...</li>
              <li><a href=\"#\">10</a></li>
\t\t\t  <li><a href=\"#\"><i class=\"fa fa-angle-right\"></i></a></li>
            </ul>
          </nav>
        </div> -->
        <!-- Pagination / End --> 
      <!-- </div> -->";
    }

    public function getTemplateName()
    {
        return "/home/weslquicksite/public_html/weslnew/themes/WESL/partials/agents/agentlisting.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  184 => 60,  170 => 52,  157 => 41,  148 => 38,  145 => 37,  141 => 36,  130 => 28,  125 => 26,  121 => 25,  117 => 24,  108 => 22,  101 => 17,  95 => 15,  91 => 13,  85 => 11,  82 => 10,  80 => 9,  75 => 7,  70 => 4,  66 => 3,  62 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div id=\"agent_list\">

{% for agent in agents %}

      <div class=\"col-md-6\"> 

        <div class=\"agent agents-profile agency\"> <a href=\"profileview/{{agent.agent.id}}\" class=\"utf-agent-avatar\"> 
      
          {% if agent.agent_user.avatar.path %}

            <img src=\"{{agent.agent_user.avatar.path}}\" alt=\"\">
          
          {% else %} 
          
            <img src=\"{{'assets/img/avaterplaceholder.jpg' |theme}}\" alt=\"\">
           
          {% endif %} 
         </a>
          <div class=\"utf-agent-content\">
            <div class=\"utf-agent-name\">
\t\t\t  <p class=\"text-alt\">Agent</p>
              <h4><a href=\"profileview/{{agent.agent.id}}\">{{ agent.agent.first_name }} {{ agent.agent.last_name }} </a></h4>
              <ul class=\"utf-agent-contact-details\">
\t\t\t      <li><i class=\"icon-material-outline-business\"></i>{{ agent.country.name }}</li>
\t\t\t      <li><i class=\"icon-material-outline-business\"></i>{{ agent.city.name }}</li>
          <li><i class=\"icon-feather-smartphone\"></i>{{ agent.agent.agent_contact }}</li>
          
          <li><i class=\"icon-material-outline-email\"></i><a href=\"#\">{{ agent.agent.email }}</a></li>

          
         
          
        </ul> 
        Services
        <br>
           {% for currency in agent.currencies %}
           
            {{currency.currency_name}}
           
           {% endfor %}
     
       
\t\t\t  <ul class=\"utf-social-icons\">
\t\t\t\t  <li><a class=\"facebook\" href=\"#\"><i class=\"icon-facebook\"></i></a></li>
\t\t\t\t  <li><a class=\"twitter\" href=\"#\"><i class=\"icon-twitter\"></i></a></li>
\t\t\t\t  <li><a class=\"linkedin\" href=\"#\"><i class=\"icon-linkedin\"></i></a></li>
\t\t\t\t  <li><a class=\"instagram\" href=\"#\"><i class=\"icon-instagram\"></i></a></li>
\t\t\t\t  <li><a class=\"gplus\" href=\"#\"><i class=\"icon-gplus\"></i></a></li>
\t\t\t  </ul>
\t\t\t</div>
\t\t\t<div class=\"clearfix\"></div>
            <p>{{agent.agent.bio}}</p>                      
          </div>
        </div>
     

       
    </div>\t
    {% endfor %}
</div>
     <!-- <div class=\"col-md-12\">
        <div class=\"clearfix\"></div>
         Pagination --> 
        <!-- <div class=\"utf-pagination-container margin-top-20\">
          <nav class=\"pagination\">
            <ul>
              <li><a href=\"#\"><i class=\"fa fa-angle-left\"></i></a></li>
\t\t\t  <li><a href=\"#\" class=\"current-page\">1</a></li>
              <li><a href=\"#\">2</a></li>
              <li><a href=\"#\">3</a></li>
              <li class=\"blank\">...</li>
              <li><a href=\"#\">10</a></li>
\t\t\t  <li><a href=\"#\"><i class=\"fa fa-angle-right\"></i></a></li>
            </ul>
          </nav>
        </div> -->
        <!-- Pagination / End --> 
      <!-- </div> -->", "/home/weslquicksite/public_html/weslnew/themes/WESL/partials/agents/agentlisting.htm", "");
    }
}
