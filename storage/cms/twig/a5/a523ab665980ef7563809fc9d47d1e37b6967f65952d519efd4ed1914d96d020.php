<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /home2/demowesl/public_html/weslnew2/themes/WESL/pages/loyaltyservice.htm */
class __TwigTemplate_bccdeb49efbd405ed9347834d53958d89c0bc1dcc01999fca5e8bfca0479d644 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = array("for" => 11, "partial" => 24);
        $filters = array("escape" => 12);
        $functions = array("form_ajax" => 4, "form_close" => 20);

        try {
            $this->sandbox->checkSecurity(
                ['for', 'partial'],
                ['escape'],
                ['form_ajax', 'form_close']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"container\">
   
             <div class=\"col-lg-2 col-md-2 col-xs-12\">
              ";
        // line 4
        echo call_user_func_array($this->env->getFunction('form_ajax')->getCallable(), ["ajax", "onFilterSearch", ["class" => "category", "update" => ["service/service" => "#service"]]]);
        echo "
        <div class=\"row with-forms\"> 
          <div class=\"row with-forms\"> 
            <div class=\"col-md-12\">
              <label for=\"category_id\">Category</label>
              <select  name=\"category_id\"  onchange=\"getFilterList()\" class=\"chosen-select\">
                <option>Category</option>
                ";
        // line 11
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["categories"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["c"]) {
            // line 12
            echo "                <option value=\"";
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["c"], "category", [], "any", false, false, true, 12), 12, $this->source), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["c"], "category", [], "any", false, false, true, 12), 12, $this->source), "html", null, true);
            echo "</option>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['c'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 14
        echo "                                                
              </select>
            </div>
          </div>
                
        </div>
        ";
        // line 20
        echo call_user_func_array($this->env->getFunction('form_close')->getCallable(), ["close"]);
        echo "
     </div>
          <div class=\"col-md-10 col-lg-10 col-xs-12\"> 

        ";
        // line 24
        $context['__cms_partial_params'] = [];
        $context['__cms_partial_params']['service'] = ($context["service"] ?? null)        ;
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("service/service"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 25
        echo "         
            <!-- Agency -->
            

          

</div>
</div>



<script>

  function getFilterList(){
    setTimeout(function(){
        \$('.category').request('onFilterSearch')
    }),1000;
  }
</script>";
    }

    public function getTemplateName()
    {
        return "/home2/demowesl/public_html/weslnew2/themes/WESL/pages/loyaltyservice.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  112 => 25,  107 => 24,  100 => 20,  92 => 14,  81 => 12,  77 => 11,  67 => 4,  62 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "/home2/demowesl/public_html/weslnew2/themes/WESL/pages/loyaltyservice.htm", "");
    }
}
