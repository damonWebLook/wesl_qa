<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /home2/demowesl/public_html/weslnew2/themes/WESL/pages/agentlist.htm */
class __TwigTemplate_b70c0169809101b30d46f467bb00da78b4bec5c7e388570fa1780130faf664a6 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = array("for" => 36, "partial" => 106);
        $filters = array("escape" => 37);
        $functions = array("form_ajax" => 30, "form_close" => 100);

        try {
            $this->sandbox->checkSecurity(
                ['for', 'partial'],
                ['escape'],
                ['form_ajax', 'form_close']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"parallax titlebar\" data-background=\"images/listings-parallax.jpg\" data-color=\"rgba(48, 48, 48, 1)\" data-color-opacity=\"0.8\" data-img-width=\"800\" data-img-height=\"505\">
    <div id=\"titlebar\">
      <div class=\"container\">
        <div class=\"row\">
          <div class=\"col-md-12\">
            <h2>Agent List</h2>
            <!-- Breadcrumbs -->
            <nav id=\"breadcrumbs\">
              <ul>
                <li><a href=\"home\">Home</a></li>
                <li>Agent List</li>
              </ul>
            </nav>
          </div>
        </div>
      </div>
    </div>
</div>
  
  <!-- Content -->
<div class=\"container-fluid\">
  <div class=\"row\">
    <div class=\"col-md-4\">
      <div class=\"sidebar\"> 
    
    <div class=\"widget utf-sidebar-widget-item\">
          <div class=\"utf-boxed-list-headline-item\">
      <h3>Country</h3>
    </div>
    ";
        // line 30
        echo call_user_func_array($this->env->getFunction('form_ajax')->getCallable(), ["ajax", "onFilterSearch", ["class" => "form-items", "update" => ["agents/agentlisting" => "#agent_list"]]]);
        echo "
      <div class=\"row with-forms\"> 
        <div class=\"row with-forms\"> 
          <div class=\"col-md-12\">
            <select  name=\"country_id\"  class=\"chosen-select\">
              <option>Select Your Country</option>
              ";
        // line 36
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($context["country"]);
        foreach ($context['_seq'] as $context["_key"] => $context["country"]) {
            // line 37
            echo "              <option value=\"";
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["country"], "id", [], "any", false, false, true, 37), 37, $this->source), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["country"], "name", [], "any", false, false, true, 37), 37, $this->source), "html", null, true);
            echo "</option>
              ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['country'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 39
        echo "                                               
            </select>
          </div>
        </div>
              
      </div>

      <div class=\"utf-boxed-list-headline-item\">
       <h3>City</h3>
       </div>
      <div class=\"row with-forms\"> 
        <div class=\"row with-forms\"> 
          <div class=\"col-md-12\">
            <select onchange=\"getFilterList()\" name=\"city_id\"  class=\"chosen-select\">
              <option>Select Your City</option>
              ";
        // line 54
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($context["city"]);
        foreach ($context['_seq'] as $context["_key"] => $context["city"]) {
            // line 55
            echo "              <option value=\"";
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["city"], "id", [], "any", false, false, true, 55), 55, $this->source), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["city"], "name", [], "any", false, false, true, 55), 55, $this->source), "html", null, true);
            echo "</option>
              ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['city'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 57
        echo "                                              
            </select>
          </div>
        </div>
      </div>
      <!-- Currency Search -->
      <div class=\"utf-boxed-list-headline-item\">
        <h3>Currency</h3>
        </div>
       <div class=\"row with-forms\"> 
         <div class=\"row with-forms\"> 
           <div class=\"col-md-12\">
             <select onchange=\"getFilterList()\" name=\"currency_id\"  class=\"chosen-select\">
               <option>Select the Base Currency</option>
               ";
        // line 71
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["currencies"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["currency"]) {
            // line 72
            echo "               <option value=\"";
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["currency"], "id", [], "any", false, false, true, 72), 72, $this->source), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["currency"], "currency_name", [], "any", false, false, true, 72), 72, $this->source), "html", null, true);
            echo "</option>
               ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['currency'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 74
        echo "                                               
             </select>
           </div>
         </div>
       </div>
       <!-- Rating Search -->
       <!--<div class=\"utf-boxed-list-headline-item\">-->
       <!-- <h3>Rating</h3>-->
       <!-- </div>-->
        <!--<div class=\"row with-forms\"> -->
        <!--  <div class=\"row with-forms\"> -->
        <!--    <div class=\"col-md-12\">-->
        <!--      <select onchange=\"getFilterList()\" name=\"rating_id\"  class=\"chosen-select\">-->
        <!--        <option class=\"checked\" value=\"1\">&#9733;&#9734;&#9734;&#9734;&#9734;</option>-->
        <!--        <option class=\"checked\" value=\"2\">&#9733;&#9733;&#9734;&#9734;&#9734;</option>-->
        <!--        <option class=\"checked\" value=\"3\">&#9733;&#9733;&#9733;&#9734;&#9734;</option>-->
        <!--        <option class=\"checked\" value=\"4\">&#9733;&#9733;&#9733;&#9733;&#9734;</option>-->
        <!--        <option class=\"checked\" value=\"5\">&#9733;&#9733;&#9733;&#9733;&#9733;</option>-->

                                                
        <!--      </select>-->
        <!--    </div>-->
        <!--  </div>-->
        <!--</div>-->
      <!-- <button type=\"submit\" class=\"button fullwidth margin-top-10\">Search</button> -->
    </div>
    ";
        // line 100
        echo call_user_func_array($this->env->getFunction('form_close')->getCallable(), ["close"]);
        echo "
    
        
      </div>
    </div>
    <div class=\"col-xs-12 col-md-8\">
      ";
        // line 106
        $context['__cms_partial_params'] = [];
        $context['__cms_partial_params']['agents'] = ($context["agents"] ?? null)        ;
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("agents/agentlisting"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 107
        echo "    </div>
</div> 
</div>

<script>

  function getFilterList(){
    setTimeout(function(){
        \$('.form-items').request('onFilterSearch')
    }),1000;
  }
</script>";
    }

    public function getTemplateName()
    {
        return "/home2/demowesl/public_html/weslnew2/themes/WESL/pages/agentlist.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  222 => 107,  217 => 106,  208 => 100,  180 => 74,  169 => 72,  165 => 71,  149 => 57,  138 => 55,  134 => 54,  117 => 39,  106 => 37,  102 => 36,  93 => 30,  62 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"parallax titlebar\" data-background=\"images/listings-parallax.jpg\" data-color=\"rgba(48, 48, 48, 1)\" data-color-opacity=\"0.8\" data-img-width=\"800\" data-img-height=\"505\">
    <div id=\"titlebar\">
      <div class=\"container\">
        <div class=\"row\">
          <div class=\"col-md-12\">
            <h2>Agent List</h2>
            <!-- Breadcrumbs -->
            <nav id=\"breadcrumbs\">
              <ul>
                <li><a href=\"home\">Home</a></li>
                <li>Agent List</li>
              </ul>
            </nav>
          </div>
        </div>
      </div>
    </div>
</div>
  
  <!-- Content -->
<div class=\"container-fluid\">
  <div class=\"row\">
    <div class=\"col-md-4\">
      <div class=\"sidebar\"> 
    
    <div class=\"widget utf-sidebar-widget-item\">
          <div class=\"utf-boxed-list-headline-item\">
      <h3>Country</h3>
    </div>
    {{form_ajax('onFilterSearch',{ class: 'form-items', update: { 'agents/agentlisting': '#agent_list'}}) }}
      <div class=\"row with-forms\"> 
        <div class=\"row with-forms\"> 
          <div class=\"col-md-12\">
            <select  name=\"country_id\"  class=\"chosen-select\">
              <option>Select Your Country</option>
              {% for country in country %}
              <option value=\"{{country.id}}\">{{country.name}}</option>
              {% endfor %}
                                               
            </select>
          </div>
        </div>
              
      </div>

      <div class=\"utf-boxed-list-headline-item\">
       <h3>City</h3>
       </div>
      <div class=\"row with-forms\"> 
        <div class=\"row with-forms\"> 
          <div class=\"col-md-12\">
            <select onchange=\"getFilterList()\" name=\"city_id\"  class=\"chosen-select\">
              <option>Select Your City</option>
              {% for city in city %}
              <option value=\"{{city.id}}\">{{city.name}}</option>
              {% endfor %}
                                              
            </select>
          </div>
        </div>
      </div>
      <!-- Currency Search -->
      <div class=\"utf-boxed-list-headline-item\">
        <h3>Currency</h3>
        </div>
       <div class=\"row with-forms\"> 
         <div class=\"row with-forms\"> 
           <div class=\"col-md-12\">
             <select onchange=\"getFilterList()\" name=\"currency_id\"  class=\"chosen-select\">
               <option>Select the Base Currency</option>
               {% for currency in currencies %}
               <option value=\"{{ currency.id }}\">{{ currency.currency_name }}</option>
               {% endfor %}
                                               
             </select>
           </div>
         </div>
       </div>
       <!-- Rating Search -->
       <!--<div class=\"utf-boxed-list-headline-item\">-->
       <!-- <h3>Rating</h3>-->
       <!-- </div>-->
        <!--<div class=\"row with-forms\"> -->
        <!--  <div class=\"row with-forms\"> -->
        <!--    <div class=\"col-md-12\">-->
        <!--      <select onchange=\"getFilterList()\" name=\"rating_id\"  class=\"chosen-select\">-->
        <!--        <option class=\"checked\" value=\"1\">&#9733;&#9734;&#9734;&#9734;&#9734;</option>-->
        <!--        <option class=\"checked\" value=\"2\">&#9733;&#9733;&#9734;&#9734;&#9734;</option>-->
        <!--        <option class=\"checked\" value=\"3\">&#9733;&#9733;&#9733;&#9734;&#9734;</option>-->
        <!--        <option class=\"checked\" value=\"4\">&#9733;&#9733;&#9733;&#9733;&#9734;</option>-->
        <!--        <option class=\"checked\" value=\"5\">&#9733;&#9733;&#9733;&#9733;&#9733;</option>-->

                                                
        <!--      </select>-->
        <!--    </div>-->
        <!--  </div>-->
        <!--</div>-->
      <!-- <button type=\"submit\" class=\"button fullwidth margin-top-10\">Search</button> -->
    </div>
    {{ form_close()}}
    
        
      </div>
    </div>
    <div class=\"col-xs-12 col-md-8\">
      {% partial 'agents/agentlisting' agents=agents %}
    </div>
</div> 
</div>

<script>

  function getFilterList(){
    setTimeout(function(){
        \$('.form-items').request('onFilterSearch')
    }),1000;
  }
</script>", "/home2/demowesl/public_html/weslnew2/themes/WESL/pages/agentlist.htm", "");
    }
}
