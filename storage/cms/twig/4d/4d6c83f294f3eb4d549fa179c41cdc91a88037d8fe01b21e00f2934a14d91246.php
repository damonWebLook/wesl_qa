<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /home2/demowesl/public_html/weslnew2/themes/WESL/pages/agentlist2.htm */
class __TwigTemplate_5f671a0163e054000a4510173cc8fc5b001f3dac7d0404d1d9a35053816d3c71 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = array("for" => 36);
        $filters = array("escape" => 37, "theme" => 111);
        $functions = array("form_ajax" => 30, "form_close" => 100);

        try {
            $this->sandbox->checkSecurity(
                ['for'],
                ['escape', 'theme'],
                ['form_ajax', 'form_close']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"parallax titlebar\" data-background=\"images/listings-parallax.jpg\" data-color=\"rgba(48, 48, 48, 1)\" data-color-opacity=\"0.8\" data-img-width=\"800\" data-img-height=\"505\">
    <div id=\"titlebar\">
      <div class=\"container\">
        <div class=\"row\">
          <div class=\"col-md-12\">
            <h2>Agency List</h2>
            <!-- Breadcrumbs -->
            <nav id=\"breadcrumbs\">
              <ul>
                <li><a href=\"home\">Home</a></li>
                <li>Agency List</li>
              </ul>
            </nav>
          </div>
        </div>
      </div>
    </div>
</div>
  
  <!-- Content -->
<div class=\"container-fluid\">
  <div class=\"row\">
    <div class=\"col-md-4\">
      <div class=\"sidebar\"> 
    
    <div class=\"widget utf-sidebar-widget-item\">
          <div class=\"utf-boxed-list-headline-item\">
      <h3>Country</h3>
    </div>
    ";
        // line 30
        echo call_user_func_array($this->env->getFunction('form_ajax')->getCallable(), ["ajax", "onFilterSearch", ["class" => "form-items", "update" => ["agents/agentlisting" => "#agent_list"]]]);
        echo "
      <div class=\"row with-forms\"> 
        <div class=\"row with-forms\"> 
          <div class=\"col-md-12\">
            <select  name=\"country_id\"  class=\"chosen-select\">
              <option>Select Your Country</option>
              ";
        // line 36
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($context["country"]);
        foreach ($context['_seq'] as $context["_key"] => $context["country"]) {
            // line 37
            echo "              <option value=\"";
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["country"], "id", [], "any", false, false, true, 37), 37, $this->source), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["country"], "name", [], "any", false, false, true, 37), 37, $this->source), "html", null, true);
            echo "</option>
              ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['country'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 39
        echo "                                               
            </select>
          </div>
        </div>
              
      </div>

      <div class=\"utf-boxed-list-headline-item\">
       <h3>City</h3>
       </div>
      <div class=\"row with-forms\"> 
        <div class=\"row with-forms\"> 
          <div class=\"col-md-12\">
            <select onchange=\"getFilterList()\" name=\"city_id\"  class=\"chosen-select\">
              <option>Select Your Country</option>
              ";
        // line 54
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($context["city"]);
        foreach ($context['_seq'] as $context["_key"] => $context["city"]) {
            // line 55
            echo "              <option value=\"";
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["city"], "id", [], "any", false, false, true, 55), 55, $this->source), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["city"], "name", [], "any", false, false, true, 55), 55, $this->source), "html", null, true);
            echo "</option>
              ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['city'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 57
        echo "                                              
            </select>
          </div>
        </div>
      </div>
      <!-- Currency Search -->
      <div class=\"utf-boxed-list-headline-item\">
        <h3>Currency</h3>
        </div>
       <div class=\"row with-forms\"> 
         <div class=\"row with-forms\"> 
           <div class=\"col-md-12\">
             <select onchange=\"getFilterList()\" name=\"currency_id\"  class=\"chosen-select\">
               <option>Select the Base Currency</option>
               ";
        // line 71
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["currencies"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["currency"]) {
            // line 72
            echo "               <option value=\"";
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["currency"], "id", [], "any", false, false, true, 72), 72, $this->source), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["currency"], "currency_name", [], "any", false, false, true, 72), 72, $this->source), "html", null, true);
            echo "</option>
               ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['currency'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 74
        echo "                                               
             </select>
           </div>
         </div>
       </div>
       <!-- Rating Search -->
       <div class=\"utf-boxed-list-headline-item\">
        <h3>Rating</h3>
        </div>
        <div class=\"row with-forms\"> 
          <div class=\"row with-forms\"> 
            <div class=\"col-md-12\">
              <select onchange=\"getFilterList()\" name=\"rating_id\"  class=\"chosen-select\">
                <option class=\"checked\" value=\"1\">&#9733;&#9734;&#9734;&#9734;&#9734;</option>
                <option class=\"checked\" value=\"2\">&#9733;&#9733;&#9734;&#9734;&#9734;</option>
                <option class=\"checked\" value=\"3\">&#9733;&#9733;&#9733;&#9734;&#9734;</option>
                <option class=\"checked\" value=\"4\">&#9733;&#9733;&#9733;&#9733;&#9734;</option>
                <option class=\"checked\" value=\"5\">&#9733;&#9733;&#9733;&#9733;&#9733;</option>

                                                
              </select>
            </div>
          </div>
        </div>
      <!-- <button type=\"submit\" class=\"button fullwidth margin-top-10\">Search</button> -->
    </div>
    ";
        // line 100
        echo call_user_func_array($this->env->getFunction('form_close')->getCallable(), ["close"]);
        echo "
    
        
      </div>
    </div>
    <div class=\"col-xs-12 col-md-8\">
      <div id=\"agent_list\">

              <div class=\"col-md-6\"> 
        
                <div class=\"agent agents-profile agency\"> <a href=\"profileview/2\" class=\"utf-agent-avatar\">   
                    <img src=\"";
        // line 111
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/avaterplaceholder.jpg");
        echo "\" alt=\"\">
                 </a>
                  <div class=\"utf-agent-content\">
                    <div class=\"utf-agent-name\">
                <p class=\"text-alt\">Agent</p>
                      <h4><a href=\"profileview/";
        // line 116
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["dummyData"] ?? null), "id", [], "any", false, false, true, 116), 116, $this->source), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["dummyData"] ?? null), "name", [], "any", false, false, true, 116), 116, $this->source), "html", null, true);
        echo "</a></h4>
                      <ul class=\"utf-agent-contact-details\">
                    <li><i class=\"icon-material-outline-business\"></i>";
        // line 118
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["dummyData"] ?? null), "country", [], "any", false, false, true, 118), 118, $this->source), "html", null, true);
        echo "</li>
                    <li><i class=\"icon-material-outline-business\"></i>";
        // line 119
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["dummyData"] ?? null), "city", [], "any", false, false, true, 119), 119, $this->source), "html", null, true);
        echo "</li>
                  <li><i class=\"icon-feather-smartphone\"></i>";
        // line 120
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["dummyData"] ?? null), "phone", [], "any", false, false, true, 120), 120, $this->source), "html", null, true);
        echo "</li>
                  
                  <li><i class=\"icon-material-outline-email\"></i><a href=\"#\">";
        // line 122
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["dummyData"] ?? null), "email", [], "any", false, false, true, 122), 122, $this->source), "html", null, true);
        echo "</a></li>
        
                  
                 
                  
                </ul> 
             
               
                <ul class=\"utf-social-icons\">
                  <li><a class=\"facebook\" href=\"#\"><i class=\"icon-facebook\"></i></a></li>
                  <li><a class=\"twitter\" href=\"#\"><i class=\"icon-twitter\"></i></a></li>
                  <li><a class=\"linkedin\" href=\"#\"><i class=\"icon-linkedin\"></i></a></li>
                  <li><a class=\"instagram\" href=\"#\"><i class=\"icon-instagram\"></i></a></li>
                  <li><a class=\"gplus\" href=\"#\"><i class=\"icon-gplus\"></i></a></li>
                </ul>
              </div>
              <div class=\"clearfix\"></div>
                    <p>";
        // line 139
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["dummyData"] ?? null), "bio", [], "any", false, false, true, 139), 139, $this->source), "html", null, true);
        echo "</p>                      
                  </div>
                </div>
             
        
               
            </div>\t
        </div>
\t
        
     
    </div>
    


    
  </div> 
</div>

<script>

  function getFilterList(){
    setTimeout(function(){
        \$('.form-items').request('onFilterSearch')
    }),1000;
  }
</script>";
    }

    public function getTemplateName()
    {
        return "/home2/demowesl/public_html/weslnew2/themes/WESL/pages/agentlist2.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  270 => 139,  250 => 122,  245 => 120,  241 => 119,  237 => 118,  230 => 116,  222 => 111,  208 => 100,  180 => 74,  169 => 72,  165 => 71,  149 => 57,  138 => 55,  134 => 54,  117 => 39,  106 => 37,  102 => 36,  93 => 30,  62 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "/home2/demowesl/public_html/weslnew2/themes/WESL/pages/agentlist2.htm", "");
    }
}
