<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /home2/demowesl/public_html/weslnew2/themes/WESL/pages/video.htm */
class __TwigTemplate_344ececf1c65882a2aadbdf68f43e59c280ce99836a56e9bb3362fbd905273f1 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = array("for" => 30, "partial" => 43);
        $filters = array("escape" => 31);
        $functions = array("form_ajax" => 23, "form_close" => 39);

        try {
            $this->sandbox->checkSecurity(
                ['for', 'partial'],
                ['escape'],
                ['form_ajax', 'form_close']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"parallax titlebar\" data-background=\"images/listings-parallax.jpg\" data-color=\"rgba(48, 48, 48, 1)\" data-color-opacity=\"0.8\" data-img-width=\"800\" data-img-height=\"505\">
    <div id=\"titlebar\">
      <div class=\"container\">
        <div class=\"row\">
          <div class=\"col-md-12\">
            <h2>Videos</h2>
            <!-- Breadcrumbs -->
            <nav id=\"breadcrumbs\">
              <ul>
                <li><a href=\"./home\">Home</a></li>
                <li>Videos</li>
              </ul>
            </nav>
          </div>
        </div>
      </div>
    </div>
</div>
<div class=\"container\">
   <div class=\"col-lg-12 col-md-12 col-xs-12\">
     <!-- sidebar -->
     <div class=\"col-lg-2 col-md-2 col-xs-12\">
      ";
        // line 23
        echo call_user_func_array($this->env->getFunction('form_ajax')->getCallable(), ["ajax", "onFilterSearch", ["class" => "category", "update" => ["video/videos" => "#video"]]]);
        echo "
        <div class=\"row with-forms\"> 
          <div class=\"row with-forms\"> 
            <div class=\"col-md-12\">
              <label for=\"category_id\">Category</label>
              <select  name=\"category_id\"  onchange=\"getFilterList()\" class=\"chosen-select\">
                <option>Category</option>
                ";
        // line 30
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["categories"] ?? null));
        foreach ($context['_seq'] as $context["key"] => $context["value"]) {
            // line 31
            echo "                <option value=\"";
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed($context["key"], 31, $this->source), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed($context["value"], 31, $this->source), "html", null, true);
            echo "</option>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['value'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 33
        echo "                                                
              </select>
            </div>
          </div>
                
        </div>
      ";
        // line 39
        echo call_user_func_array($this->env->getFunction('form_close')->getCallable(), ["close"]);
        echo "
     </div>
     <!-- Video Tiles -->
     <div class=\"col-lg-10 col-md-10 col-xs-12\">
      ";
        // line 43
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("video/videos"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 44
        echo "     </div>
   </div>
</div>

<script>

  function getFilterList(){
    setTimeout(function(){
        \$('.category').request('onFilterSearch')
    }),1000;
  }
</script>";
    }

    public function getTemplateName()
    {
        return "/home2/demowesl/public_html/weslnew2/themes/WESL/pages/video.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  130 => 44,  126 => 43,  119 => 39,  111 => 33,  100 => 31,  96 => 30,  86 => 23,  62 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "/home2/demowesl/public_html/weslnew2/themes/WESL/pages/video.htm", "");
    }
}
