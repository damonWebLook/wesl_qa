<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /home/weslquicksite/public_html/weslnew/themes/WESL/partials/video/videos.htm */
class __TwigTemplate_ea2e7d9d628d3619622a28d39f5f81b813c9df1cc69d327dd57b455003915740 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = array("for" => 2);
        $filters = array("escape" => 9, "media" => 10);
        $functions = array();

        try {
            $this->sandbox->checkSecurity(
                ['for'],
                ['escape', 'media'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"\" id=\"video\">
    ";
        // line 2
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["videos"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["video"]) {
            // line 3
            echo "              <!-- Video -->

              <div class=\"col-md-3 col-lg-3 col-xs-12\" style=\"margin-left: 15px;\">

                <div class=\"row\">
                  <!-- <i class=\"fas fa-ad\" style=\"font-size: 45px;\"></i> -->
                    <a href=\"player/";
            // line 9
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["video"], "id", [], "any", false, false, true, 9), 9, $this->source), "html", null, true);
            echo "\" target=\"_blank\" style=\"padding: 05px 05px;\"> 
                      <video src=\".";
            // line 10
            echo $this->extensions['System\Twig\Extension']->mediaFilter($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["video"], "video_path", [], "any", false, false, true, 10), 10, $this->source));
            echo "\"id=\"media-video\"></video>
                    </a>

                    
              


                  <!-- <span>
                    <img src=\"https://via.placeholder.com/400x350?text=Video+Placeholder\" alt=\"\" width=\"100%\">
                  </span> -->
                </div>
              </div>

      
      

    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['video'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 27
        echo "</div>";
    }

    public function getTemplateName()
    {
        return "/home/weslquicksite/public_html/weslnew/themes/WESL/partials/video/videos.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  104 => 27,  81 => 10,  77 => 9,  69 => 3,  65 => 2,  62 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"\" id=\"video\">
    {% for video in videos%}
              <!-- Video -->

              <div class=\"col-md-3 col-lg-3 col-xs-12\" style=\"margin-left: 15px;\">

                <div class=\"row\">
                  <!-- <i class=\"fas fa-ad\" style=\"font-size: 45px;\"></i> -->
                    <a href=\"player/{{ video.id }}\" target=\"_blank\" style=\"padding: 05px 05px;\"> 
                      <video src=\".{{ video.video_path | media}}\"id=\"media-video\"></video>
                    </a>

                    
              


                  <!-- <span>
                    <img src=\"https://via.placeholder.com/400x350?text=Video+Placeholder\" alt=\"\" width=\"100%\">
                  </span> -->
                </div>
              </div>

      
      

    {% endfor %}
</div>", "/home/weslquicksite/public_html/weslnew/themes/WESL/partials/video/videos.htm", "");
    }
}
