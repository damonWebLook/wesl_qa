<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /home2/demowesl/public_html/weslnew2/themes/WESL/partials/slider/slide.htm */
class __TwigTemplate_7a3eb46cbe8c1afc1a559e2a37b51bda87d4c782b5cecd4bfb043564a42ad37a extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = array();
        $filters = array("theme" => 6);
        $functions = array();

        try {
            $this->sandbox->checkSecurity(
                [],
                ['theme'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"container\">
    <div class=\"row margin-bottom-50\">
      <div class=\"col-md-12\"> 
        <!-- Slider -->
        <div class=\"property-slider default\">
            <a href=\"#\" data-background-image=\"";
        // line 6
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/source (1).jpg");
        echo "\" class=\"item mfp-gallery\"></a> 
\t\t\t<a href=\"#\" data-background-image=\"";
        // line 7
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/source (1).jpg");
        echo "\" class=\"item mfp-gallery\"></a> 
\t\t\t<a href=\"#\" data-background-image=\"";
        // line 8
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/download.jpg");
        echo "\" class=\"item mfp-gallery\"></a> 
\t\t\t<a href=\"#\" data-background-image=\"";
        // line 9
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/cryptocurrency_trader_mobile_app_mockup_071703.jpg");
        echo "\" class=\"item mfp-gallery\"></a> 
\t\t\t<a href=\"#\" data-background-image=\"";
        // line 10
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/images.jpg");
        echo "\" class=\"item mfp-gallery\"></a> 
\t\t\t<a href=\"#\" data-background-image=\"";
        // line 11
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/source (2).jpg");
        echo "\" class=\"item mfp-gallery\"></a> 
\t\t\t<a href=\"#\" data-background-image=\"";
        // line 12
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/source.jpg");
        echo "\" class=\"item mfp-gallery\"></a> 
\t\t</div>
        <!-- Slider Thumbs -->
        <div class=\"property-slider-nav\">
          <!--<div class=\"item\"><img src=\"";
        // line 16
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/banner.jpg");
        echo "\" alt=\"\"></div>-->
          <!--<div class=\"item\"><img src=\"";
        // line 17
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/banner.jpg");
        echo "\" alt=\"\"></div>-->
          <!--<div class=\"item\"><img src=\"";
        // line 18
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/banner.jpg");
        echo "\" alt=\"\"></div>-->
          <!--<div class=\"item\"><img src=\"";
        // line 19
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/banner.jpg");
        echo "\" alt=\"\"></div>-->
          <!--<div class=\"item\"><img src=\"";
        // line 20
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/banner.jpg");
        echo "\" alt=\"\"></div>-->
          <!--<div class=\"item\"><img src=\"";
        // line 21
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/banner.jpg");
        echo "\" alt=\"\"></div>-->
        </div>
      </div>
    </div>
  </div>";
    }

    public function getTemplateName()
    {
        return "/home2/demowesl/public_html/weslnew2/themes/WESL/partials/slider/slide.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  120 => 21,  116 => 20,  112 => 19,  108 => 18,  104 => 17,  100 => 16,  93 => 12,  89 => 11,  85 => 10,  81 => 9,  77 => 8,  73 => 7,  69 => 6,  62 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"container\">
    <div class=\"row margin-bottom-50\">
      <div class=\"col-md-12\"> 
        <!-- Slider -->
        <div class=\"property-slider default\">
            <a href=\"#\" data-background-image=\"{{ 'assets/images/source (1).jpg' | theme}}\" class=\"item mfp-gallery\"></a> 
\t\t\t<a href=\"#\" data-background-image=\"{{ 'assets/images/source (1).jpg' | theme}}\" class=\"item mfp-gallery\"></a> 
\t\t\t<a href=\"#\" data-background-image=\"{{ 'assets/images/download.jpg' | theme}}\" class=\"item mfp-gallery\"></a> 
\t\t\t<a href=\"#\" data-background-image=\"{{ 'assets/images/cryptocurrency_trader_mobile_app_mockup_071703.jpg' | theme}}\" class=\"item mfp-gallery\"></a> 
\t\t\t<a href=\"#\" data-background-image=\"{{ 'assets/images/images.jpg' | theme}}\" class=\"item mfp-gallery\"></a> 
\t\t\t<a href=\"#\" data-background-image=\"{{ 'assets/images/source (2).jpg' | theme}}\" class=\"item mfp-gallery\"></a> 
\t\t\t<a href=\"#\" data-background-image=\"{{ 'assets/images/source.jpg' | theme}}\" class=\"item mfp-gallery\"></a> 
\t\t</div>
        <!-- Slider Thumbs -->
        <div class=\"property-slider-nav\">
          <!--<div class=\"item\"><img src=\"{{ 'assets/img/banner.jpg' | theme}}\" alt=\"\"></div>-->
          <!--<div class=\"item\"><img src=\"{{ 'assets/img/banner.jpg' | theme}}\" alt=\"\"></div>-->
          <!--<div class=\"item\"><img src=\"{{ 'assets/img/banner.jpg' | theme}}\" alt=\"\"></div>-->
          <!--<div class=\"item\"><img src=\"{{ 'assets/img/banner.jpg' | theme}}\" alt=\"\"></div>-->
          <!--<div class=\"item\"><img src=\"{{ 'assets/img/banner.jpg' | theme}}\" alt=\"\"></div>-->
          <!--<div class=\"item\"><img src=\"{{ 'assets/img/banner.jpg' | theme}}\" alt=\"\"></div>-->
        </div>
      </div>
    </div>
  </div>", "/home2/demowesl/public_html/weslnew2/themes/WESL/partials/slider/slide.htm", "");
    }
}
