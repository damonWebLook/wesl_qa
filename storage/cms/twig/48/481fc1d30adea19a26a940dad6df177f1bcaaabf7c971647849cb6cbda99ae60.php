<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /home2/demowesl/public_html/weslnew2/themes/WESL/pages/userlogin.htm */
class __TwigTemplate_d0171520d2443794cbbea1f692067cfd4f1f796a4a454b7f1f657e06c1551f05 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = array("partial" => 6);
        $filters = array("theme" => 4, "page" => 16);
        $functions = array();

        try {
            $this->sandbox->checkSecurity(
                ['partial'],
                ['theme', 'page'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"container-fluid imagewidth spaceweight\">
      <div class=\"row\">
        <div class=\"col-12 col-md-6\">
         <img  class=\"imagesize\" src=\"";
        // line 4
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/weslportal.png");
        echo "\">
        </div>
        ";
        // line 6
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("Flash/flashmessage"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 7
        echo "        <div class=\"col colform\">
            <div class=\"row\">
              <div class=\"col-12 col-md-10 panelcenter\">
                <div class=\"my-account\">
                  <div class=\"tabs-container\"> 
                    <!-- Login -->
                <div class=\"utf-welcome-text-item\">
              <form data-request=\"onLogin\" >    
                <h3>Welcome Back Sign in to Continue</h3>
                <span>Don't Have an Account? <a href=\"";
        // line 16
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("userregister");
        echo "\">Sign Up!</a></span> 
                </div>

                        <div class=\"form-row form-row-wide\">
                          <input type=\"text\" class=\"input-text\" name=\"email\" id=\"email\" placeholder=\"Email Address\" value=\"\" />
                        </div>
                        <div class=\"form-row form-row-wide\">
                          <input class=\"input-text\" type=\"password\" name=\"password\" placeholder=\"Password\" id=\"password\"/>
                        </div>
                        <div class=\"form-row\">
                          <div class=\"checkbox margin-top-10 margin-bottom-10\">
                  <input type=\"checkbox\" id=\"two-step\">
                  <label for=\"two-step\"><span class=\"checkbox-icon\"></span> Remember Me</label>
                  <a class=\"lost_password\" href=\"";
        // line 29
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("forgetpassword");
        echo "\">Forgot Password?</a>\t
                  </div>

               
                        </div>
                <input type=\"submit\" class=\"button full-width border margin-top-10\" name=\"login\" value=\"Login\" />

              </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
</div>";
    }

    public function getTemplateName()
    {
        return "/home2/demowesl/public_html/weslnew2/themes/WESL/pages/userlogin.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  103 => 29,  87 => 16,  76 => 7,  72 => 6,  67 => 4,  62 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"container-fluid imagewidth spaceweight\">
      <div class=\"row\">
        <div class=\"col-12 col-md-6\">
         <img  class=\"imagesize\" src=\"{{'assets/img/weslportal.png' |theme }}\">
        </div>
        {% partial 'Flash/flashmessage' %}
        <div class=\"col colform\">
            <div class=\"row\">
              <div class=\"col-12 col-md-10 panelcenter\">
                <div class=\"my-account\">
                  <div class=\"tabs-container\"> 
                    <!-- Login -->
                <div class=\"utf-welcome-text-item\">
              <form data-request=\"onLogin\" >    
                <h3>Welcome Back Sign in to Continue</h3>
                <span>Don't Have an Account? <a href=\"{{'userregister' | page}}\">Sign Up!</a></span> 
                </div>

                        <div class=\"form-row form-row-wide\">
                          <input type=\"text\" class=\"input-text\" name=\"email\" id=\"email\" placeholder=\"Email Address\" value=\"\" />
                        </div>
                        <div class=\"form-row form-row-wide\">
                          <input class=\"input-text\" type=\"password\" name=\"password\" placeholder=\"Password\" id=\"password\"/>
                        </div>
                        <div class=\"form-row\">
                          <div class=\"checkbox margin-top-10 margin-bottom-10\">
                  <input type=\"checkbox\" id=\"two-step\">
                  <label for=\"two-step\"><span class=\"checkbox-icon\"></span> Remember Me</label>
                  <a class=\"lost_password\" href=\"{{'forgetpassword' | page}}\">Forgot Password?</a>\t
                  </div>

               
                        </div>
                <input type=\"submit\" class=\"button full-width border margin-top-10\" name=\"login\" value=\"Login\" />

              </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
</div>", "/home2/demowesl/public_html/weslnew2/themes/WESL/pages/userlogin.htm", "");
    }
}
