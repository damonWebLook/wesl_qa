<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /home2/demowesl/public_html/weslnew2/themes/WESL/layouts/agentregister.htm */
class __TwigTemplate_d3845f86123f1c661c28d201047a780dd23c7965038da202aeed5f4dc446d39f extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = array("if" => 87, "page" => 114, "framework" => 119, "scripts" => 120);
        $filters = array("theme" => 9, "page" => 40, "escape" => 96);
        $functions = array("url" => 42);

        try {
            $this->sandbox->checkSecurity(
                ['if', 'page', 'framework', 'scripts'],
                ['theme', 'page', 'escape'],
                ['url']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<html>
    <head>
        <meta name=\"author\" content=\"\">
        <meta name=\"description\" content=\"\">
        <meta http-equiv=\"Content-Type\" content=\"text/html;charset=UTF-8\"/>
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
        <title>Welcome to WESL </title>
        <!--  Favicon -->
        <link rel=\"shortcut icon\" href=\"";
        // line 9
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/favicon.png");
        echo "\">
        <!-- CSS -->
        <link rel=\"stylesheet\" href=\"";
        // line 11
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/css/stylesheet.css");
        echo "\">
        
        <!-- Google Fonts -->
        <link href=\"https://fonts.googleapis.com/css?family=Nunito:300,400,600,700,800&display=swap\" rel=\"stylesheet\">
        <link href=\"https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800&display=swap\" rel=\"stylesheet\">  
        <!-- Font Awesome -->
        <script src=\"https://kit.fontawesome.com/f5acd884b0.js\" crossorigin=\"anonymous\"></script>
        <!-- AdPlayer -->
        <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css\">
        <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js\"></script>
        <style>

        </style>


        </head>
        <body>
            <header id=\"header-container\" class=\"fullwidth\"> 
                <!-- Header -->
                <div id=\"header\">
                  <div class=\"container\"> 
                    <div class=\"left-side\"> 
                      <div id=\"logo\"><a href=\"index.html\"><img src=\"";
        // line 33
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/WESL.png");
        echo "\" alt=\"\"></a></div>
                      <div class=\"mmenu-trigger\">
                        <button class=\"hamburger hamburger--collapse\" type=\"button\"> <span class=\"hamburger-box\"> <span class=\"hamburger-inner\"></span> </span> </button>
                      </div>
                      <!-- Main Navigation -->
                      <nav id=\"navigation\" class=\"style-1\">
                        <ul id=\"responsive\">
                         <li><a  href=\"";
        // line 40
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("home");
        echo "\">Home</a></li>
                          <li><a href=\"";
        // line 41
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("agentlist");
        echo "\">Agents</a></li>
                          <li><a href=\"";
        // line 42
        echo url("/services");
        echo "\">Listing</a></li>
                          <li><a href=\"";
        // line 43
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("video");
        echo "\">Videos</a></li>
                          <li><a href=\"";
        // line 44
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("contact");
        echo "\">Contact</a></li> 
                           
                         
                          <!-- <li><a href=\"#\">Pages</a>
                            <ul>
                              <li><a href=\"#\">Agents</a>
                                <ul>
                                  <li><a href=\"#\">Agents List</a></li>
                                  <li><a href=\"#\">Agents Profile</a></li>
                                </ul>
                              </li>
                              <li><a href=\"#\">Agency</a>
                                <ul>
                                  <li><a href=\"#\">Agency List</a></li>
                                  <li><a href=\"#\">Agency Profile</a></li>                      
                                </ul>
                              </li>
                              <li><a href=\"#\">About</a></li>
                              <li><a href=\"#\">Login</a></li>
                              <li><a href=\"#\">Register</a></li>
                              <li><a href=\"#\">Pricing Plan</a></li>
                              <li><a href=\"#\">Typography</a></li>
                              <li><a href=\"#\">Compare Properties</a></li>
                              <li><a href=\"#\">User Elements</a></li>
                              <li><a href=\"#\">Icons Cheatsheet</a></li>
                            </ul>
                          </li> -->


                         
                          <li><a href=\"";
        // line 74
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("contact");
        echo "\">Contact</a></li>
                        </ul>

                      </nav>


                      <div class=\"clearfix\"></div>
                    </div>

                      <!-- Right Side Content / End -->
                        <div class=\"right-side\"> 
                          <div class=\"header-widget\"> 
                            <div class=\"user-menu con-1\">
                              ";
        // line 87
        if ( !($context["user"] ?? null)) {
            // line 88
            echo "             

                              <div class=\"left-side con-2\" >
                                <a href=\"";
            // line 91
            echo $this->extensions['Cms\Twig\Extension']->pageFilter("userlogin");
            echo "\"><i class=\"sl sl-icon-user\"></i> User Login</a>
                              </div>
                               
                   
                           ";
        } else {
            // line 96
            echo "                           <div class=\"user-name\"><span><i class=\"sl sl-icon-user\"></i></span><div class=\"user-name-title\">";
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "login", [], "any", false, false, true, 96), 96, $this->source), "html", null, true);
            echo "</div></div>
                           <ul>
                             <li><a href=\"";
            // line 98
            echo $this->extensions['Cms\Twig\Extension']->pageFilter("userdashboard");
            echo "\"><i class=\"sl sl-icon-user\"></i> My Dashboard</a></li>
                             <li><a href=\"";
            // line 99
            echo $this->extensions['Cms\Twig\Extension']->pageFilter("userprofile");
            echo "\"><i class=\"sl sl-icon-user\"></i> My Profile</a></li>
                             <li><a href=\"";
            // line 100
            echo $this->extensions['Cms\Twig\Extension']->pageFilter("passwordchange");
            echo "\"><i class=\"sl sl-icon-user\"></i> Change Password</a></li>
                             <li><a data-request=\"onLogOut\"><i class=\"sl sl-icon-power\"></i> Log Out</a></li>
                            </ul> 
                         </div>
                           ";
        }
        // line 105
        echo "   
                            
                         
                      </div>
                        </div>
        <!-- Right Side Content / End -->  
                  
              </header>

            ";
        // line 114
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFunction();
        // line 115
        echo "        </body>

        <!-- Footer -->
        <script src=\"https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js\"></script>
        ";
        // line 119
        $_minify = System\Classes\CombineAssets::instance()->useMinify;
        if ($_minify) {
            echo '<script src="' . Request::getBasePath() . '/modules/system/assets/js/framework.combined-min.js"></script>'.PHP_EOL;
        }
        else {
            echo '<script src="' . Request::getBasePath() . '/modules/system/assets/js/framework.js"></script>'.PHP_EOL;
            echo '<script src="' . Request::getBasePath() . '/modules/system/assets/js/framework.extras.js"></script>'.PHP_EOL;
        }
        echo '<link rel="stylesheet" property="stylesheet" href="' . Request::getBasePath() .'/modules/system/assets/css/framework.extras'.($_minify ? '-min' : '').'.css">'.PHP_EOL;
        unset($_minify);
        // line 120
        echo "        ";
        echo $this->env->getExtension('Cms\Twig\Extension')->assetsFunction('js');
        echo $this->env->getExtension('Cms\Twig\Extension')->displayBlock('scripts');
        // line 121
        echo "  <div id=\"footer\"> 
    <div class=\"container\">
      <div class=\"row\">
\t    <div class=\"col-md-4 col-sm-12 col-xs-12\"> 
          <a href=\"index-1.html\"><img class=\"footer-logo\" src=\"";
        // line 125
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/WESL.png");
        echo "\" alt=\"\"></a>
          <p>Lorem Ipsum is simply dummy text of printing and type setting industry. Lorem Ipsum been industry standard dummy text ever since, when unknown printer took a galley type scrambled.</p>          
        </div>
        <div class=\"col-md-2 col-sm-3 col-xs-6\">
          <h4>Useful Links</h4>
          <ul class=\"utf-footer-links-item\">
            <li><a href=\"index-1.html\">Home</a></li>
            <li><a href=\"#\">About Us</a></li>
            <li><a href=\"#\">Services</a></li>
            <li><a href=\"#\">Properties</a></li>
            <li><a href=\"#\">Contact</a></li>
          </ul>
        </div>
        <div class=\"col-md-2 col-sm-3 col-xs-6\">
          <h4>My Account</h4>
          <ul class=\"utf-footer-links-item\">
            <li><a href=\"#\">Dashboard</a></li>
            <li><a href=\"#\">My Profile</a></li>
            <li><a href=\"#\">Add Property</a></li>
\t\t\t<li><a href=\"#\">My Listing</a></li>
            <li><a href=\"#\">Favorites</a></li>
          </ul>
        </div>
        <div class=\"col-md-2 col-sm-3 col-xs-6\">
          <h4>Resources</h4>
          <ul class=\"utf-footer-links-item\">
            <li><a href=\"#\">My Account</a></li>
            <li><a href=\"#\">Support</a></li>
            <li><a href=\"#\">How It Work</a></li>
            <li><a href=\"#\">Privacy Policy</a></li>
\t\t\t<li><a href=\"#\">Term & Condition</a></li>
          </ul>
        </div>
\t\t<div class=\"col-md-2 col-sm-3 col-xs-6\">
          <h4>Pages</h4>
          <ul class=\"utf-footer-links-item\">
            <li><a href=\"#\">Our Partners</a></li>
            <li><a href=\"#\">How It Work</a></li>
\t\t\t<li><a href=\"#\">FAQ Page</a></li>
            <li><a href=\"#\">Privacy Policy</a></li>
\t\t\t<li><a href=\"#\">Term & Condition</a></li>
          </ul>
        </div>\t
      </div>
      <div class=\"row\">
        <div class=\"col-md-12\">
          <div class=\"copyrights\">Copyright © 2020 All Rights Reserved.</div>
        </div>
      </div>
    </div>
  </div>
  
  
  <script src=\"";
        // line 178
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/chosen.min.js");
        echo "\"></script> 
  <script src=\"";
        // line 179
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/magnific-popup.min.js");
        echo "\"></script> 
  <script src=\"";
        // line 180
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/owl.carousel.min.js");
        echo "\"></script> 
  <script src=\"";
        // line 181
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/rangeSlider.js");
        echo "\"></script> 
  <script src=\"";
        // line 182
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/sticky-kit.min.js");
        echo "\"></script> 
  <script src=\"";
        // line 183
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/slick.min.js");
        echo "\"></script> 
  <script src=\"";
        // line 184
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/masonry.min.js");
        echo "\"></script> 
  <script src=\"";
        // line 185
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/mmenu.min.js");
        echo "\"></script> 
  <script src=\"";
        // line 186
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/tooltips.min.js");
        echo "\"></script> 
  <script src=\"";
        // line 187
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/typed.js");
        echo "\"></script>
  <script src=\"";
        // line 188
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/custom_jquery.js");
        echo "\"></script>  
       
  <!-- Footer / End -->

  <!-- Sign In Popup -->
<div id=\"utf-signin-dialog-block\" class=\"zoom-anim-dialog mfp-hide dialog-with-tabs\"> 
  <div class=\"utf-signin-form-part\">
    <ul class=\"utf-popup-tabs-nav-item\">
      <li><a href=\"#login\">Log In</a></li>
      <li><a href=\"#register\">Register</a></li>
    </ul>
    <div class=\"utf-popup-container-part-tabs\"> 
      <!-- Login -->
      <div class=\"utf-popup-tab-content-item\" id=\"login\"> 
        <div class=\"utf-welcome-text-item\">
          <h3>Welcome Back Sign in to Continue</h3>
          <span>Don't Have an Account? <a href=\"#\" class=\"register-tab\">Sign Up!</a></span> 
\t\t</div>
        <form method=\"post\" id=\"login-form\">
          <div class=\"utf-no-border\">
            <input type=\"text\" name=\"emailaddress\" id=\"emailaddress\" placeholder=\"Email Address\" required/>
          </div>
          <div class=\"utf-no-border\">
            <input type=\"password\" name=\"password\" id=\"password\" placeholder=\"Password\" required/>
          </div>
\t\t  <div class=\"checkbox margin-top-0\">
\t\t\t<input type=\"checkbox\" id=\"two-step\">
\t\t\t<label for=\"two-step\"><span class=\"checkbox-icon\"></span> Remember Me</label>
\t\t  </div>
          <a href=\"forgot_password.html\" class=\"forgot-password\">Forgot Password?</a>
        </form>
        <button class=\"button full-width utf-button-sliding-icon ripple-effect\" type=\"submit\" form=\"login-form\">Log In <i class=\"icon-feather-chevrons-right\"></i></button>
        <div class=\"utf-social-login-separator-item\"><span>or</span></div>
        <div class=\"utf-social-login-buttons-block\">
          <button class=\"facebook-login ripple-effect\"><i class=\"icon-brand-facebook-f\"></i> Facebook</button>
\t\t  <button class=\"google-login ripple-effect\"><i class=\"icon-brand-google-plus-g\"></i> Google+</button>
\t\t  <button class=\"twitter-login ripple-effect\"><i class=\"icon-brand-twitter\"></i> Twitter</button>
        </div>
      </div>
      
      <!-- Register -->
      <div class=\"utf-popup-tab-content-item\" id=\"register\"> 
        <div class=\"utf-welcome-text-item\">
          <h3>Create your Account!</h3>
\t\t  <span>Don't Have an Account? <a href=\"#\" class=\"register-tab\">Sign Up!</a></span> 
        </div>        
        <form method=\"post\" id=\"utf-register-account-form\">
          <div class=\"utf-no-border margin-bottom-20\">
\t\t\t<select class=\"utf-chosen-select-single-item utf-with-border\" title=\"Single User\">
\t\t\t\t<option>Single User</option>
\t\t\t\t<option>Agent</option>
\t\t\t\t<option>Multi User</option>\t\t\t\t
\t\t\t</select>
\t\t  </div>
\t\t  <div class=\"utf-no-border\">
            <input type=\"text\" name=\"name\" id=\"name\" placeholder=\"User Name\" required/>
          </div>
\t\t  <div class=\"utf-no-border\">
            <input type=\"text\" name=\"emailaddress-register\" id=\"emailaddress-register\" placeholder=\"Email Address\" required/>
          </div>
\t\t  <div class=\"utf-no-border\">
            <input type=\"password\" name=\"password-register\" id=\"password-register\" placeholder=\"Password\" required/>
          </div>
          <div class=\"utf-no-border\">
            <input type=\"password\" name=\"password-repeat-register\" id=\"password-repeat-register\" placeholder=\"Repeat Password\" required/>
          </div>
\t\t  <div class=\"checkbox margin-top-0\">
\t\t\t<input type=\"checkbox\" id=\"two-step0\">
\t\t\t<label for=\"two-step0\"><span class=\"checkbox-icon\"></span> By Registering You Confirm That You Accept <a href=\"#\">Terms & Conditions</a> and <a href=\"#\">Privacy Policy</a></label>
\t\t  </div>
        </form>
        <button class=\"margin-top-10 button full-width utf-button-sliding-icon ripple-effect\" type=\"submit\" form=\"utf-register-account-form\">Register <i class=\"icon-feather-chevrons-right\"></i></button>
        <div class=\"utf-social-login-separator-item\"><span>or</span></div>
        <div class=\"utf-social-login-buttons-block\">
          <button class=\"facebook-login ripple-effect\"><i class=\"icon-brand-facebook-f\"></i> Facebook</button>
\t\t  <button class=\"google-login ripple-effect\"><i class=\"icon-brand-google-plus-g\"></i> Google+</button>
\t\t  <button class=\"twitter-login ripple-effect\"><i class=\"icon-brand-twitter\"></i> Twitter</button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Sign In Popup / End -->


</html>";
    }

    public function getTemplateName()
    {
        return "/home2/demowesl/public_html/weslnew2/themes/WESL/layouts/agentregister.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  350 => 188,  346 => 187,  342 => 186,  338 => 185,  334 => 184,  330 => 183,  326 => 182,  322 => 181,  318 => 180,  314 => 179,  310 => 178,  254 => 125,  248 => 121,  244 => 120,  233 => 119,  227 => 115,  225 => 114,  214 => 105,  206 => 100,  202 => 99,  198 => 98,  192 => 96,  184 => 91,  179 => 88,  177 => 87,  161 => 74,  128 => 44,  124 => 43,  120 => 42,  116 => 41,  112 => 40,  102 => 33,  77 => 11,  72 => 9,  62 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "/home2/demowesl/public_html/weslnew2/themes/WESL/layouts/agentregister.htm", "");
    }
}
