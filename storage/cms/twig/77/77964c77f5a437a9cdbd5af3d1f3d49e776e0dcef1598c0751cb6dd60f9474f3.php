<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /home/weslquicksite/public_html/weslnew/themes/WESL/partials/agents/rating.htm */
class __TwigTemplate_a9fab3e10b73265adc9325b5de2ae42909b29677f83cb0b2d116238bece1f06d extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = array("if" => 1);
        $filters = array();
        $functions = array();

        try {
            $this->sandbox->checkSecurity(
                ['if'],
                [],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if ((($context["permission"] ?? null) == 1)) {
            // line 2
            echo "   <span style=\"color:green;\">
        <em>You have already Rated this agent</em>
    </span>

";
        } else {
            // line 7
            echo "<div class=\"\" id=\"rating\">
    <p>Please Rate Us!</p>
</div>   
";
        }
    }

    public function getTemplateName()
    {
        return "/home/weslquicksite/public_html/weslnew/themes/WESL/partials/agents/rating.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  71 => 7,  64 => 2,  62 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% if permission == 1 %}
   <span style=\"color:green;\">
        <em>You have already Rated this agent</em>
    </span>

{% else %}
<div class=\"\" id=\"rating\">
    <p>Please Rate Us!</p>
</div>   
{% endif %}", "/home/weslquicksite/public_html/weslnew/themes/WESL/partials/agents/rating.htm", "");
    }
}
