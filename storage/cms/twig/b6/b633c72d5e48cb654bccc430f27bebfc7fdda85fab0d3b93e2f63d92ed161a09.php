<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /home/weslquicksite/public_html/weslnew/themes/WESL/partials/home/currecysearch.htm */
class __TwigTemplate_a720d13c346670ba697fc31866f71ac15ba0af43b9c795a19ff6f090e3fab38d extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = array("for" => 14);
        $filters = array("escape" => 15);
        $functions = array("form_ajax" => 4, "form_close" => 32);

        try {
            $this->sandbox->checkSecurity(
                ['for'],
                ['escape'],
                ['form_ajax', 'form_close']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"utf-main-search-box-area\"> 


";
        // line 4
        echo call_user_func_array($this->env->getFunction('form_ajax')->getCallable(), ["ajax", "onSearch"]);
        echo "
   
           <div class=\"row with-forms\"> 
      <!--<div class=\"col-md-4 col-sm-12\">-->
      <!--    <input type=\"text\" class=\"ico-01\" placeholder=\"Enter Keywords...\" value=\"\"/>-->
      <!--</div>-->

      <div class=\"col-md-5 col-sm-12\">
        <select data-placeholder=\"Select From\" title=\"Select From Currency\" class=\"utf-chosen-select-single-item\" name=\"from\">
          <option>From</option>
          ";
        // line 14
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($context["currency"]);
        foreach ($context['_seq'] as $context["_key"] => $context["currency"]) {
            // line 15
            echo "            <option value=\"";
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["currency"], "id", [], "any", false, false, true, 15), 15, $this->source), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["currency"], "currency_name", [], "any", false, false, true, 15), 15, $this->source), "html", null, true);
            echo "</option>
          ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['currency'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 16
        echo "                       
        </select>
      </div>
      <div class=\"col-md-5 col-sm-12\">
        <select data-placeholder=\"Select To\" title=\"Select To Currency\" class=\"utf-chosen-select-single-item\" name=\"to\">
          <option >To</option>
          ";
        // line 22
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($context["currency"]);
        foreach ($context['_seq'] as $context["_key"] => $context["currency"]) {
            // line 23
            echo "            <option value=\"";
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["currency"], "id", [], "any", false, false, true, 23), 23, $this->source), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["currency"], "currency_name", [], "any", false, false, true, 23), 23, $this->source), "html", null, true);
            echo "</option>
          ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['currency'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 24
        echo "                      
        </select>
      </div>
      <div class=\"col-md-2 col-sm-6\">
          <button  type=\"submit\" class=\"button utf-search-btn-item\"><i class=\"fa fa-search\"></i> Search</button>\t
      </div>
   
    </div>
  ";
        // line 32
        echo call_user_func_array($this->env->getFunction('form_close')->getCallable(), ["close"]);
        echo "

                      
    
</div>";
    }

    public function getTemplateName()
    {
        return "/home/weslquicksite/public_html/weslnew/themes/WESL/partials/home/currecysearch.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  128 => 32,  118 => 24,  107 => 23,  103 => 22,  95 => 16,  84 => 15,  80 => 14,  67 => 4,  62 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"utf-main-search-box-area\"> 


{{ form_ajax('onSearch') }}
   
           <div class=\"row with-forms\"> 
      <!--<div class=\"col-md-4 col-sm-12\">-->
      <!--    <input type=\"text\" class=\"ico-01\" placeholder=\"Enter Keywords...\" value=\"\"/>-->
      <!--</div>-->

      <div class=\"col-md-5 col-sm-12\">
        <select data-placeholder=\"Select From\" title=\"Select From Currency\" class=\"utf-chosen-select-single-item\" name=\"from\">
          <option>From</option>
          {% for currency in currency %}
            <option value=\"{{currency.id}}\">{{currency.currency_name}}</option>
          {% endfor %}                       
        </select>
      </div>
      <div class=\"col-md-5 col-sm-12\">
        <select data-placeholder=\"Select To\" title=\"Select To Currency\" class=\"utf-chosen-select-single-item\" name=\"to\">
          <option >To</option>
          {% for currency in currency %}
            <option value=\"{{currency.id}}\">{{currency.currency_name}}</option>
          {% endfor %}                      
        </select>
      </div>
      <div class=\"col-md-2 col-sm-6\">
          <button  type=\"submit\" class=\"button utf-search-btn-item\"><i class=\"fa fa-search\"></i> Search</button>\t
      </div>
   
    </div>
  {{ form_close() }}

                      
    
</div>", "/home/weslquicksite/public_html/weslnew/themes/WESL/partials/home/currecysearch.htm", "");
    }
}
