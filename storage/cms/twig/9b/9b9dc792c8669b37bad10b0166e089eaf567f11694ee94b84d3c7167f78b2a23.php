<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /home/weslquicksite/public_html/weslnew/themes/WESL/pages/agentregister.htm */
class __TwigTemplate_e78bab9083e14555a72f2921555499d7d11ea04efc38a69a138e001c299f42fe extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = array("for" => 60);
        $filters = array("escape" => 61);
        $functions = array();

        try {
            $this->sandbox->checkSecurity(
                ['for'],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"clearfix\"></div>
<!-- Header Container / End --> 

<!-- Titlebar -->
<div class=\"parallax titlebar\" data-background=\"images/listings-parallax.jpg\" data-color=\"rgba(48, 48, 48, 1)\" data-color-opacity=\"0.8\" data-img-width=\"800\" data-img-height=\"505\">
  <div id=\"titlebar\">
    <div class=\"container\">
      <div class=\"row\">
        <div class=\"col-md-12\">
          <h2>Register</h2>
          <!-- Breadcrumbs -->
          <nav id=\"breadcrumbs\">
            <ul>
              <li><a href=\"#\">Home</a></li>
              <li>Agent Register</li>
            </ul>
          </nav>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Contact --> 
<div class=\"container\">
  <div class=\"row\">
    <div class=\"col-md-6 col-md-offset-3\">
      <div class=\"my-account\">
        <div class=\"tabs-container\"> 
          <!-- Register -->
            <div class=\"utf-welcome-text-item\">
              <h3>Create Your New Account!</h3>
              <span>Have a account <a href=\"agentlogin\">Log In</a></span> 
            </div>
            <form data-request=\"onInqurycontact\" class=\"login\">
              <div class=\"form-row form-row-wide margin-bottom-15\">
                  <!-- <select class=\"utf-chosen-select-single-item utf-with-border\" title=\"Single User\">
                      <option>Single User</option>
                      <option>Agent</option>
                      <option>Multi User</option>\t\t\t\t
                  </select> -->
              </div>
              <div class=\"form-row form-row-wide\">
                  <input type=\"text\" class=\"input-text\" name=\"first_name\" id=\"first_name\" placeholder=\"First Name\" value=\"\" />
              </div>
              <div class=\"form-row form-row-wide\">
                  <input type=\"text\" class=\"input-text\" name=\"last_name\" id=\"last_name\" placeholder=\"Last Name\" value=\"\" />
              </div>
              <div class=\"form-row form-row-wide\">
                <input type=\"text\" class=\"input-text\" name=\"login\" id=\"login\" placeholder=\"Username\" value=\"\" />
            </div>
            <div class=\"form-row form-row-wide\">
                <input type=\"email\" class=\"input-text\" name=\"email\" id=\"email\" placeholder=\"Email Address\" value=\"\" />
            </div>

            <div class=\"form-row form-row-wide\">
              <select name=\"country\" class=\"utf-chosen-select-single-item utf-with-border\" title=\"Choose The Country\">
                
                <!--<option>Choose the Country</option>-->
                ";
        // line 60
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($context["country"]);
        foreach ($context['_seq'] as $context["_key"] => $context["country"]) {
            // line 61
            echo "                <option value=\"";
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["country"], "id", [], "any", false, false, true, 61), 61, $this->source), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["country"], "name", [], "any", false, false, true, 61), 61, $this->source), "html", null, true);
            echo "</option>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['country'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 63
        echo "              </select>
            </div>
            
            <div class=\"form-row form-row-wide\">
                <input type=\"number\" class=\"input-text\" name=\"agent_contact\" id=\"email\" placeholder=\"Contact No\" value=\"\" />
            </div>


          

            <div class=\"form-row form-row-wide\">
              <select name=\"city\" class=\"utf-chosen-select-single-item utf-with-border\" title=\"Choose The City\">
                <!--<option>Choose The City</option>-->
                ";
        // line 76
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($context["city"]);
        foreach ($context['_seq'] as $context["_key"] => $context["city"]) {
            // line 77
            echo "                <option value=\"";
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["city"], "id", [], "any", false, false, true, 77), 77, $this->source), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["city"], "name", [], "any", false, false, true, 77), 77, $this->source), "html", null, true);
            echo "</option>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['city'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 79
        echo "               
             </select>
            
            </div>
            



            

              
            <div class=\"form-row checkbox margin-top-10 margin-bottom-10\">
                <input type=\"checkbox\" id=\"two-step0\">
                  <!-- <label for=\"two-step0\"><span class=\"checkbox-icon\"></span> By Registering You Confirm That You Accept <a href=\"#\">Terms & Conditions</a> and <a href=\"#\">Privacy Policy</a></label> -->
              </div>
            <input type=\"submit\" class=\"button full-width border margin-top-10\" name=\"Create An Account\" value=\"Create your Account\" />
            \t
            </form>
        </div>
      </div>
    </div>
  </div>
</div>";
    }

    public function getTemplateName()
    {
        return "/home/weslquicksite/public_html/weslnew/themes/WESL/pages/agentregister.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  168 => 79,  157 => 77,  153 => 76,  138 => 63,  127 => 61,  123 => 60,  62 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"clearfix\"></div>
<!-- Header Container / End --> 

<!-- Titlebar -->
<div class=\"parallax titlebar\" data-background=\"images/listings-parallax.jpg\" data-color=\"rgba(48, 48, 48, 1)\" data-color-opacity=\"0.8\" data-img-width=\"800\" data-img-height=\"505\">
  <div id=\"titlebar\">
    <div class=\"container\">
      <div class=\"row\">
        <div class=\"col-md-12\">
          <h2>Register</h2>
          <!-- Breadcrumbs -->
          <nav id=\"breadcrumbs\">
            <ul>
              <li><a href=\"#\">Home</a></li>
              <li>Agent Register</li>
            </ul>
          </nav>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Contact --> 
<div class=\"container\">
  <div class=\"row\">
    <div class=\"col-md-6 col-md-offset-3\">
      <div class=\"my-account\">
        <div class=\"tabs-container\"> 
          <!-- Register -->
            <div class=\"utf-welcome-text-item\">
              <h3>Create Your New Account!</h3>
              <span>Have a account <a href=\"agentlogin\">Log In</a></span> 
            </div>
            <form data-request=\"onInqurycontact\" class=\"login\">
              <div class=\"form-row form-row-wide margin-bottom-15\">
                  <!-- <select class=\"utf-chosen-select-single-item utf-with-border\" title=\"Single User\">
                      <option>Single User</option>
                      <option>Agent</option>
                      <option>Multi User</option>\t\t\t\t
                  </select> -->
              </div>
              <div class=\"form-row form-row-wide\">
                  <input type=\"text\" class=\"input-text\" name=\"first_name\" id=\"first_name\" placeholder=\"First Name\" value=\"\" />
              </div>
              <div class=\"form-row form-row-wide\">
                  <input type=\"text\" class=\"input-text\" name=\"last_name\" id=\"last_name\" placeholder=\"Last Name\" value=\"\" />
              </div>
              <div class=\"form-row form-row-wide\">
                <input type=\"text\" class=\"input-text\" name=\"login\" id=\"login\" placeholder=\"Username\" value=\"\" />
            </div>
            <div class=\"form-row form-row-wide\">
                <input type=\"email\" class=\"input-text\" name=\"email\" id=\"email\" placeholder=\"Email Address\" value=\"\" />
            </div>

            <div class=\"form-row form-row-wide\">
              <select name=\"country\" class=\"utf-chosen-select-single-item utf-with-border\" title=\"Choose The Country\">
                
                <!--<option>Choose the Country</option>-->
                {% for country in country %}
                <option value=\"{{country.id}}\">{{country.name}}</option>
                {% endfor %}
              </select>
            </div>
            
            <div class=\"form-row form-row-wide\">
                <input type=\"number\" class=\"input-text\" name=\"agent_contact\" id=\"email\" placeholder=\"Contact No\" value=\"\" />
            </div>


          

            <div class=\"form-row form-row-wide\">
              <select name=\"city\" class=\"utf-chosen-select-single-item utf-with-border\" title=\"Choose The City\">
                <!--<option>Choose The City</option>-->
                {% for city in city %}
                <option value=\"{{city.id}}\">{{city.name}}</option>
                {% endfor %}
               
             </select>
            
            </div>
            



            

              
            <div class=\"form-row checkbox margin-top-10 margin-bottom-10\">
                <input type=\"checkbox\" id=\"two-step0\">
                  <!-- <label for=\"two-step0\"><span class=\"checkbox-icon\"></span> By Registering You Confirm That You Accept <a href=\"#\">Terms & Conditions</a> and <a href=\"#\">Privacy Policy</a></label> -->
              </div>
            <input type=\"submit\" class=\"button full-width border margin-top-10\" name=\"Create An Account\" value=\"Create your Account\" />
            \t
            </form>
        </div>
      </div>
    </div>
  </div>
</div>", "/home/weslquicksite/public_html/weslnew/themes/WESL/pages/agentregister.htm", "");
    }
}
