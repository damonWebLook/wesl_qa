<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /home2/demowesl/public_html/weslnew2/themes/WESL/partials/agents/star.htm */
class __TwigTemplate_c58bf87fa167018d8ae04cde98f81ad7e12b0b53a814b017b0aed3a032703c4d extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = array("if" => 1);
        $filters = array();
        $functions = array();

        try {
            $this->sandbox->checkSecurity(
                ['if'],
                [],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if ((($context["star"] ?? null) == 1)) {
            // line 2
            echo "    <span class=\"fa fa-star checked\"></span>
    <span class=\"fa fa-star\"></span>
    <span class=\"fa fa-star\"></span>
    <span class=\"fa fa-star\"></span>
    <span class=\"fa fa-star\"></span>  
";
        } elseif ((        // line 7
($context["star"] ?? null) == 2)) {
            // line 8
            echo "    <span class=\"fa fa-star checked\"></span>
    <span class=\"fa fa-star checked\"></span>
    <span class=\"fa fa-star\"></span>
    <span class=\"fa fa-star\"></span>
    <span class=\"fa fa-star\"></span>  
";
        } elseif ((        // line 13
($context["star"] ?? null) == 3)) {
            // line 14
            echo "    <span class=\"fa fa-star checked\"></span>
    <span class=\"fa fa-star checked\"></span>
    <span class=\"fa fa-star checked\"></span>
    <span class=\"fa fa-star\"></span>
    <span class=\"fa fa-star\"></span>  
";
        } elseif ((        // line 19
($context["star"] ?? null) == 4)) {
            // line 20
            echo "    <span class=\"fa fa-star checked\"></span>
    <span class=\"fa fa-star checked\"></span>
    <span class=\"fa fa-star checked\"></span>
    <span class=\"fa fa-star checked\"></span>
    <span class=\"fa fa-star\"></span>  
";
        } elseif ((        // line 25
($context["star"] ?? null) == 5)) {
            // line 26
            echo "    <span class=\"fa fa-star checked\"></span>
    <span class=\"fa fa-star checked\"></span>
    <span class=\"fa fa-star checked\"></span>
    <span class=\"fa fa-star checked\"></span>
    <span class=\"fa fa-star checked\"></span>  
";
        } else {
            // line 32
            echo "    <span class=\"fa fa-star\"></span>
    <span class=\"fa fa-star\"></span>
    <span class=\"fa fa-star\"></span>
    <span class=\"fa fa-star\"></span>
    <span class=\"fa fa-star\"></span>  
";
        }
    }

    public function getTemplateName()
    {
        return "/home2/demowesl/public_html/weslnew2/themes/WESL/partials/agents/star.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  108 => 32,  100 => 26,  98 => 25,  91 => 20,  89 => 19,  82 => 14,  80 => 13,  73 => 8,  71 => 7,  64 => 2,  62 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% if star == 1 %}
    <span class=\"fa fa-star checked\"></span>
    <span class=\"fa fa-star\"></span>
    <span class=\"fa fa-star\"></span>
    <span class=\"fa fa-star\"></span>
    <span class=\"fa fa-star\"></span>  
{% elseif star == 2 %}
    <span class=\"fa fa-star checked\"></span>
    <span class=\"fa fa-star checked\"></span>
    <span class=\"fa fa-star\"></span>
    <span class=\"fa fa-star\"></span>
    <span class=\"fa fa-star\"></span>  
{% elseif star == 3 %}
    <span class=\"fa fa-star checked\"></span>
    <span class=\"fa fa-star checked\"></span>
    <span class=\"fa fa-star checked\"></span>
    <span class=\"fa fa-star\"></span>
    <span class=\"fa fa-star\"></span>  
{% elseif star == 4 %}
    <span class=\"fa fa-star checked\"></span>
    <span class=\"fa fa-star checked\"></span>
    <span class=\"fa fa-star checked\"></span>
    <span class=\"fa fa-star checked\"></span>
    <span class=\"fa fa-star\"></span>  
{% elseif star == 5 %}
    <span class=\"fa fa-star checked\"></span>
    <span class=\"fa fa-star checked\"></span>
    <span class=\"fa fa-star checked\"></span>
    <span class=\"fa fa-star checked\"></span>
    <span class=\"fa fa-star checked\"></span>  
{% else %}
    <span class=\"fa fa-star\"></span>
    <span class=\"fa fa-star\"></span>
    <span class=\"fa fa-star\"></span>
    <span class=\"fa fa-star\"></span>
    <span class=\"fa fa-star\"></span>  
{% endif %}", "/home2/demowesl/public_html/weslnew2/themes/WESL/partials/agents/star.htm", "");
    }
}
