<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /home/weslquicksite/public_html/weslnew/themes/WESL/pages/home.htm */
class __TwigTemplate_e73976d522de73ff6f4fc33545a46154ce5e8e17fafba72505269a448fa2edac extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = array("partial" => 25, "for" => 39, "if" => 48);
        $filters = array("theme" => 4, "escape" => 49);
        $functions = array();

        try {
            $this->sandbox->checkSecurity(
                ['partial', 'for', 'if'],
                ['theme', 'escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"clearfix\"></div>
  

  <!--<div class=\"parallax\" data-background=\"";
        // line 4
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/banner.jpg");
        echo "\" data-color=\"#36383e\" data-color-opacity=\"0.72\" data-img-width=\"2500\" data-img-height=\"1600\">-->
  <!--  <div class=\"utf-parallax-content-area\">-->
  <!--    <div class=\"container\">-->
  <!--      <div class=\"row\">-->
  <!--        <div class=\"col-md-12\"> -->
  <!--          <div class=\"utf-main-search-container-area\">-->
  <!--            <div class=\"utf-banner-headline-text-part\">-->
\t\t<!--\t\t<h2>Welcome to WESL  <span class=\"typed-words\"></span></h2>-->
\t\t<!--\t\t<span>From as low as \$10 per day with limited time offer discounts.</span> -->
\t\t<!--\t  </div>-->
         
         
             
  <!--          </div>-->
  <!--        </div>-->
  <!--      </div>-->
  <!--    </div>-->
  <!--  </div>-->
  <!--</div>-->
  
  
  <!--";
        // line 25
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("slider/slide"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        echo "-->
  
  <!-- Content -->
  <section class=\"fullwidth\" data-background-color=\"#ffffff\">
\t  <div class=\"container\">
\t\t<div class=\"row\">
\t\t  <div class=\"col-md-12\">\t
\t\t\t<div class=\"utf-section-headline-item centered margin-bottom-30 margin-top-0\">
\t\t\t  <h3 class=\"headline\"> Top Agents</h3>
\t\t\t  <div class=\"utf-headline-display-inner-item\">Most Featured Agents</div>
\t\t\t</div>  
\t\t  </div>
\t\t  <div class=\"col-md-12\">
\t\t\t<div class=\"carousel\"> 
\t\t\t";
        // line 39
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($context["agent"]);
        foreach ($context['_seq'] as $context["_key"] => $context["agent"]) {
            // line 40
            echo "\t\t\t  <div class=\"utf-carousel-item-area\">
\t\t\t\t<div class=\"utf-listing-item\"> 
\t\t\t\t  <a href=\"single-property-page-1.html\" class=\"utf-smt-listing-img-container\">
\t\t\t\t  <div class=\"utf-listing-badges-item\"> 
\t\t\t\t\t<!--<span class=\"featured\">Featured</span> -->
\t\t\t\t\t<!--<span class=\"for-sale\">For Sale</span> -->
\t\t\t\t  </div>\t\t\t\t  
\t\t\t\t  <div class=\"utf-listing-img-content-item\"> \t\t\t\t\t
\t\t\t\t\t";
            // line 48
            if (twig_get_attribute($this->env, $this->source, $context["agent"], "avater", [], "any", false, false, true, 48)) {
                // line 49
                echo "\t\t\t\t\t<img class=\"utf-user-picture\" src=\"";
                echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["agent_user"] ?? null), "avatar", [], "any", false, false, true, 49), "path", [], "any", false, false, true, 49), 49, $this->source), "html", null, true);
                echo "\" alt=\"user_1\" />
\t\t\t\t\t   
\t\t\t\t   ";
            } else {
                // line 52
                echo "\t\t\t\t   
\t\t\t\t\t<img class=\"utf-user-picture\" src=\"";
                // line 53
                echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/avaterplaceholder.jpg");
                echo "\" alt=\"user_1\" />
\t\t\t\t   
\t\t\t\t   ";
            }
            // line 55
            echo " 
\t\t\t\t\t<span class=\"like-icon with-tip\" data-tip-content=\"Bookmark\"></span> 
\t\t\t\t\t<span class=\"compare-button with-tip\" data-tip-content=\"Add to Compare\"></span> 
\t\t\t\t\t<span class=\"video-button with-tip\" data-tip-content=\"Video\"></span>
\t\t\t\t  </div>
\t\t\t\t  <div class=\"utf-listing-carousel-item\">
\t\t\t\t\t<div>
\t\t\t\t\t\t";
            // line 62
            if (twig_get_attribute($this->env, $this->source, $context["agent"], "avater", [], "any", false, false, true, 62)) {
                // line 63
                echo "\t\t\t\t\t\t<img src=\"";
                echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["agent"], "agent_user", [], "any", false, false, true, 63), "avatar", [], "any", false, false, true, 63), "path", [], "any", false, false, true, 63), 63, $this->source), "html", null, true);
                echo "\" alt=\"\">
\t\t\t\t\t\t";
            } else {
                // line 65
                echo "\t\t\t\t\t\t<img class=\"utf-user-picture\" src=\"";
                echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/avaterplaceholder.jpg");
                echo "\" alt=\"user_1\" />
\t\t\t\t\t\t";
            }
            // line 66
            echo " 
\t\t\t\t\t</div>
\t\t\t\t
\t\t\t\t  </div>
\t\t\t\t  </a>
\t\t\t\t  <div class=\"utf-listing-content\">\t\t\t\t
\t\t\t\t\t<div class=\"utf-listing-title\">
\t\t\t\t\t  <!-- <span class=\"utf-listing-price\">\$18,000/mo</span> \t\t\t\t\t\t  \t -->
\t\t\t\t\t  <h4><a href=\"single-property-page-1.html\">";
            // line 74
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["agent"], "userName", [], "any", false, false, true, 74), "first_name", [], "any", false, false, true, 74), 74, $this->source), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["agent"], "userName", [], "any", false, false, true, 74), "last_name", [], "any", false, false, true, 74), 74, $this->source), "html", null, true);
            echo "</a></h4>
\t\t\t\t\t  <!--<span class=\"utf-listing-address\"><i class=\"icon-material-outline-location-on\"></i> ";
            // line 75
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["agent"], "country", [], "any", false, false, true, 75), "name", [], "any", false, false, true, 75), 75, $this->source), "html", null, true);
            echo ", ";
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["agent"], "city", [], "any", false, false, true, 75), "name", [], "any", false, false, true, 75), 75, $this->source), "html", null, true);
            echo "</span> \t\t\t\t\t\t\t\t\t\t\t  -->
\t\t\t\t\t</div>\t\t\t\t
\t\t\t\t
\t\t\t\t\t<!--<div class=\"utf-listing-user-info\"><a href=\"agents-profile.html\"><i class=\"icon-line-awesome-user\"></i> ";
            // line 78
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["agent"], "userName", [], "any", false, false, true, 78), "first_name", [], "any", false, false, true, 78), 78, $this->source), "html", null, true);
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["agent"], "userName", [], "any", false, false, true, 78), "last_name", [], "any", false, false, true, 78), 78, $this->source), "html", null, true);
            echo "</a> <span>1 Days Ago</span></div>\t-->
\t\t\t\t  </div>
\t\t\t\t</div>
\t\t\t  </div>
\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['agent'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 82
        echo "  
\t\t\t  
\t\t\t
\t\t\t
\t\t\t  
\t\t
\t\t\t</div>
\t\t  </div>      
\t\t</div>
\t  </div>
  </section>
  

 
  

  
  <!-- Fullwidth Section -->

  <!-- Start Section Callout -->

  <!-- End Section Callout -->
  
  <section class=\"fullwidth\" data-background-color=\"#ffffff\">
\t<div class=\"container\">
\t\t<div class=\"row\">
\t\t  <div class=\"col-md-12\">\t
\t\t\t<div class=\"utf-section-headline-item centered margin-bottom-30 margin-top-0\">
\t\t\t  <h3 class=\"headline\"> Top Cities Around You </h3>
\t\t\t  <div class=\"utf-headline-display-inner-item\">Most Popular Places</div>
\t\t\t  <p class=\"utf-slogan-text\">Find the agents from cities closer to you</p>
\t\t\t</div>  
\t\t  </div>
\t\t  <div class=\"col-md-4 col-sm-6\"> 
\t\t\t
\t\t\t   <a href=\"agentlistt?id=47&name=Lalith+Dale&country=Italy&city=Venice&email=vamabazeza@mailinator.com&phone=+39-351-2555-8141\" class=\"img-box\"> 
\t\t\t\t<img src=\"storage/app/media/venice.jpg\" alt=\"\" />
\t\t\t\t<div class=\"utf-cat-img-box-content visible\">
\t\t\t\t  <h4>Venice</h4>
\t\t\t\t  <!-- <span>14 Properties</span>  -->
\t\t\t\t</div>
\t\t\t</a> 
\t\t
\t\t  </div>
\t\t  <div class=\"col-md-4 col-sm-6\"> 
\t\t\t
\t\t\t   <a href=\"agentlistt?id=48&name=Disney+Jude&country=Italy&city=Rome&email=zunavowad@mailinator.com&phone=+39-351-2555-8141\" class=\"img-box\"> 
\t\t\t\t<img src=\"storage/app/media/rome.jpg\" alt=\"\" />
\t\t\t\t<div class=\"utf-cat-img-box-content visible\">
\t\t\t\t  <h4>Rome</h4>
\t\t\t\t  <!-- <span>14 Properties</span>  -->
\t\t\t\t</div>
\t\t\t</a> 
\t\t
\t\t  </div>
\t\t  <div class=\"col-md-4 col-sm-6\"> 
\t\t\t
\t\t\t   <a href=\"agentlistt?id=34&name=Blair+Pruitt&country=Italy&city=Milan&email=xecexypak@mailinator.com&phone=+39-351-2555-8141\" class=\"img-box\"> 
\t\t\t\t<img src=\"storage/app/media/milan.jpg\" alt=\"\" />
\t\t\t\t<div class=\"utf-cat-img-box-content visible\">
\t\t\t\t  <h4>Milan</h4>
\t\t\t\t  <!-- <span>14 Properties</span>  -->
\t\t\t\t</div>
\t\t\t</a> 
\t\t
\t\t  </div>
\t\t  <div class=\"col-md-4 col-sm-6\"> 
\t\t\t
\t\t\t   <a href=\"agentlistt?id=49&name=Devid+Billa&country=Italy&city=Bologna&email=gahyw@mailinator.com&phone=+39-351-2555-8141\" class=\"img-box\"> 
\t\t\t\t<img src=\"storage/app/media/bologna.jpg\" alt=\"\" />
\t\t\t\t<div class=\"utf-cat-img-box-content visible\">
\t\t\t\t  <h4>Bologna</h4>
\t\t\t\t  <!-- <span>14 Properties</span>  -->
\t\t\t\t</div>
\t\t\t</a> 
\t\t
\t\t  </div>
\t\t  <div class=\"col-md-4 col-sm-6\"> 
\t\t\t
\t\t\t   <a href=\"agentlistt?id=50&name=Anjalow+Kartramar&country=Italy&city=Tuscany&email=vamabazeza@mailinator.com&phone=+39-351-2555-8141\" class=\"img-box\"> 
\t\t\t\t<img src=\"storage/app/media/tuscany.jpg\" alt=\"\" />
\t\t\t\t<div class=\"utf-cat-img-box-content visible\">
\t\t\t\t  <h4>Tuscany</h4>
\t\t\t\t  <!-- <span>14 Properties</span>  -->
\t\t\t\t</div>
\t\t\t</a> 
\t\t
\t\t  </div>
\t\t  <div class=\"col-md-4 col-sm-6\"> 
\t\t\t
\t\t\t   <a href=\"agentlistt?id=47&name=Lalith+Dale&country=Italy&city=Palermo&email=zunavowad@mailinator.com&phone=+39-351-2555-8141\" class=\"img-box\"> 
\t\t\t\t<img src=\"storage/app/media/palermo.jpg\" alt=\"\" />
\t\t\t\t<div class=\"utf-cat-img-box-content visible\">
\t\t\t\t  <h4>Palermo</h4>
\t\t\t\t  <!-- <span>14 Properties</span>  -->
\t\t\t\t</div>
\t\t\t</a> 
\t\t
\t\t  </div>\t\t  
\t\t  
\t\t<!--<div class=\"utf-centered-button margin-top-10\">-->
\t\t<!--\t<a href=\"all-categorie.html\" class=\"button\">View All Categories</a> -->
\t\t<!--</div>-->
\t </div>
  </section>
  
  <!-- Fullwidth Section -->";
    }

    public function getTemplateName()
    {
        return "/home/weslquicksite/public_html/weslnew/themes/WESL/pages/home.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  201 => 82,  189 => 78,  181 => 75,  175 => 74,  165 => 66,  159 => 65,  153 => 63,  151 => 62,  142 => 55,  136 => 53,  133 => 52,  126 => 49,  124 => 48,  114 => 40,  110 => 39,  91 => 25,  67 => 4,  62 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"clearfix\"></div>
  

  <!--<div class=\"parallax\" data-background=\"{{ 'assets/img/banner.jpg' | theme}}\" data-color=\"#36383e\" data-color-opacity=\"0.72\" data-img-width=\"2500\" data-img-height=\"1600\">-->
  <!--  <div class=\"utf-parallax-content-area\">-->
  <!--    <div class=\"container\">-->
  <!--      <div class=\"row\">-->
  <!--        <div class=\"col-md-12\"> -->
  <!--          <div class=\"utf-main-search-container-area\">-->
  <!--            <div class=\"utf-banner-headline-text-part\">-->
\t\t<!--\t\t<h2>Welcome to WESL  <span class=\"typed-words\"></span></h2>-->
\t\t<!--\t\t<span>From as low as \$10 per day with limited time offer discounts.</span> -->
\t\t<!--\t  </div>-->
         
         
             
  <!--          </div>-->
  <!--        </div>-->
  <!--      </div>-->
  <!--    </div>-->
  <!--  </div>-->
  <!--</div>-->
  
  
  <!--{% partial 'slider/slide' %}-->
  
  <!-- Content -->
  <section class=\"fullwidth\" data-background-color=\"#ffffff\">
\t  <div class=\"container\">
\t\t<div class=\"row\">
\t\t  <div class=\"col-md-12\">\t
\t\t\t<div class=\"utf-section-headline-item centered margin-bottom-30 margin-top-0\">
\t\t\t  <h3 class=\"headline\"> Top Agents</h3>
\t\t\t  <div class=\"utf-headline-display-inner-item\">Most Featured Agents</div>
\t\t\t</div>  
\t\t  </div>
\t\t  <div class=\"col-md-12\">
\t\t\t<div class=\"carousel\"> 
\t\t\t{% for agent in agent %}
\t\t\t  <div class=\"utf-carousel-item-area\">
\t\t\t\t<div class=\"utf-listing-item\"> 
\t\t\t\t  <a href=\"single-property-page-1.html\" class=\"utf-smt-listing-img-container\">
\t\t\t\t  <div class=\"utf-listing-badges-item\"> 
\t\t\t\t\t<!--<span class=\"featured\">Featured</span> -->
\t\t\t\t\t<!--<span class=\"for-sale\">For Sale</span> -->
\t\t\t\t  </div>\t\t\t\t  
\t\t\t\t  <div class=\"utf-listing-img-content-item\"> \t\t\t\t\t
\t\t\t\t\t{% if agent.avater %}
\t\t\t\t\t<img class=\"utf-user-picture\" src=\"{{agent_user.avatar.path}}\" alt=\"user_1\" />
\t\t\t\t\t   
\t\t\t\t   {% else %}
\t\t\t\t   
\t\t\t\t\t<img class=\"utf-user-picture\" src=\"{{'assets/img/avaterplaceholder.jpg' |theme}}\" alt=\"user_1\" />
\t\t\t\t   
\t\t\t\t   {% endif %} 
\t\t\t\t\t<span class=\"like-icon with-tip\" data-tip-content=\"Bookmark\"></span> 
\t\t\t\t\t<span class=\"compare-button with-tip\" data-tip-content=\"Add to Compare\"></span> 
\t\t\t\t\t<span class=\"video-button with-tip\" data-tip-content=\"Video\"></span>
\t\t\t\t  </div>
\t\t\t\t  <div class=\"utf-listing-carousel-item\">
\t\t\t\t\t<div>
\t\t\t\t\t\t{% if agent.avater %}
\t\t\t\t\t\t<img src=\"{{agent.agent_user.avatar.path}}\" alt=\"\">
\t\t\t\t\t\t{% else %}
\t\t\t\t\t\t<img class=\"utf-user-picture\" src=\"{{'assets/img/avaterplaceholder.jpg' |theme}}\" alt=\"user_1\" />
\t\t\t\t\t\t{% endif %} 
\t\t\t\t\t</div>
\t\t\t\t
\t\t\t\t  </div>
\t\t\t\t  </a>
\t\t\t\t  <div class=\"utf-listing-content\">\t\t\t\t
\t\t\t\t\t<div class=\"utf-listing-title\">
\t\t\t\t\t  <!-- <span class=\"utf-listing-price\">\$18,000/mo</span> \t\t\t\t\t\t  \t -->
\t\t\t\t\t  <h4><a href=\"single-property-page-1.html\">{{ agent.userName.first_name }} {{ agent.userName.last_name }}</a></h4>
\t\t\t\t\t  <!--<span class=\"utf-listing-address\"><i class=\"icon-material-outline-location-on\"></i> {{ agent.country.name }}, {{ agent.city.name }}</span> \t\t\t\t\t\t\t\t\t\t\t  -->
\t\t\t\t\t</div>\t\t\t\t
\t\t\t\t
\t\t\t\t\t<!--<div class=\"utf-listing-user-info\"><a href=\"agents-profile.html\"><i class=\"icon-line-awesome-user\"></i> {{ agent.userName.first_name }}{{ agent.userName.last_name }}</a> <span>1 Days Ago</span></div>\t-->
\t\t\t\t  </div>
\t\t\t\t</div>
\t\t\t  </div>
\t\t\t{% endfor %}  
\t\t\t  
\t\t\t
\t\t\t
\t\t\t  
\t\t
\t\t\t</div>
\t\t  </div>      
\t\t</div>
\t  </div>
  </section>
  

 
  

  
  <!-- Fullwidth Section -->

  <!-- Start Section Callout -->

  <!-- End Section Callout -->
  
  <section class=\"fullwidth\" data-background-color=\"#ffffff\">
\t<div class=\"container\">
\t\t<div class=\"row\">
\t\t  <div class=\"col-md-12\">\t
\t\t\t<div class=\"utf-section-headline-item centered margin-bottom-30 margin-top-0\">
\t\t\t  <h3 class=\"headline\"> Top Cities Around You </h3>
\t\t\t  <div class=\"utf-headline-display-inner-item\">Most Popular Places</div>
\t\t\t  <p class=\"utf-slogan-text\">Find the agents from cities closer to you</p>
\t\t\t</div>  
\t\t  </div>
\t\t  <div class=\"col-md-4 col-sm-6\"> 
\t\t\t
\t\t\t   <a href=\"agentlistt?id=47&name=Lalith+Dale&country=Italy&city=Venice&email=vamabazeza@mailinator.com&phone=+39-351-2555-8141\" class=\"img-box\"> 
\t\t\t\t<img src=\"storage/app/media/venice.jpg\" alt=\"\" />
\t\t\t\t<div class=\"utf-cat-img-box-content visible\">
\t\t\t\t  <h4>Venice</h4>
\t\t\t\t  <!-- <span>14 Properties</span>  -->
\t\t\t\t</div>
\t\t\t</a> 
\t\t
\t\t  </div>
\t\t  <div class=\"col-md-4 col-sm-6\"> 
\t\t\t
\t\t\t   <a href=\"agentlistt?id=48&name=Disney+Jude&country=Italy&city=Rome&email=zunavowad@mailinator.com&phone=+39-351-2555-8141\" class=\"img-box\"> 
\t\t\t\t<img src=\"storage/app/media/rome.jpg\" alt=\"\" />
\t\t\t\t<div class=\"utf-cat-img-box-content visible\">
\t\t\t\t  <h4>Rome</h4>
\t\t\t\t  <!-- <span>14 Properties</span>  -->
\t\t\t\t</div>
\t\t\t</a> 
\t\t
\t\t  </div>
\t\t  <div class=\"col-md-4 col-sm-6\"> 
\t\t\t
\t\t\t   <a href=\"agentlistt?id=34&name=Blair+Pruitt&country=Italy&city=Milan&email=xecexypak@mailinator.com&phone=+39-351-2555-8141\" class=\"img-box\"> 
\t\t\t\t<img src=\"storage/app/media/milan.jpg\" alt=\"\" />
\t\t\t\t<div class=\"utf-cat-img-box-content visible\">
\t\t\t\t  <h4>Milan</h4>
\t\t\t\t  <!-- <span>14 Properties</span>  -->
\t\t\t\t</div>
\t\t\t</a> 
\t\t
\t\t  </div>
\t\t  <div class=\"col-md-4 col-sm-6\"> 
\t\t\t
\t\t\t   <a href=\"agentlistt?id=49&name=Devid+Billa&country=Italy&city=Bologna&email=gahyw@mailinator.com&phone=+39-351-2555-8141\" class=\"img-box\"> 
\t\t\t\t<img src=\"storage/app/media/bologna.jpg\" alt=\"\" />
\t\t\t\t<div class=\"utf-cat-img-box-content visible\">
\t\t\t\t  <h4>Bologna</h4>
\t\t\t\t  <!-- <span>14 Properties</span>  -->
\t\t\t\t</div>
\t\t\t</a> 
\t\t
\t\t  </div>
\t\t  <div class=\"col-md-4 col-sm-6\"> 
\t\t\t
\t\t\t   <a href=\"agentlistt?id=50&name=Anjalow+Kartramar&country=Italy&city=Tuscany&email=vamabazeza@mailinator.com&phone=+39-351-2555-8141\" class=\"img-box\"> 
\t\t\t\t<img src=\"storage/app/media/tuscany.jpg\" alt=\"\" />
\t\t\t\t<div class=\"utf-cat-img-box-content visible\">
\t\t\t\t  <h4>Tuscany</h4>
\t\t\t\t  <!-- <span>14 Properties</span>  -->
\t\t\t\t</div>
\t\t\t</a> 
\t\t
\t\t  </div>
\t\t  <div class=\"col-md-4 col-sm-6\"> 
\t\t\t
\t\t\t   <a href=\"agentlistt?id=47&name=Lalith+Dale&country=Italy&city=Palermo&email=zunavowad@mailinator.com&phone=+39-351-2555-8141\" class=\"img-box\"> 
\t\t\t\t<img src=\"storage/app/media/palermo.jpg\" alt=\"\" />
\t\t\t\t<div class=\"utf-cat-img-box-content visible\">
\t\t\t\t  <h4>Palermo</h4>
\t\t\t\t  <!-- <span>14 Properties</span>  -->
\t\t\t\t</div>
\t\t\t</a> 
\t\t
\t\t  </div>\t\t  
\t\t  
\t\t<!--<div class=\"utf-centered-button margin-top-10\">-->
\t\t<!--\t<a href=\"all-categorie.html\" class=\"button\">View All Categories</a> -->
\t\t<!--</div>-->
\t </div>
  </section>
  
  <!-- Fullwidth Section -->", "/home/weslquicksite/public_html/weslnew/themes/WESL/pages/home.htm", "");
    }
}
