<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /home/weslquicksite/public_html/weslnew/themes/WESL/partials/slider/slide.htm */
class __TwigTemplate_952e1dc2d7a7d34104118d6ffe2eda9cfd20141505899b8f6fde2f641de33412 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = array();
        $filters = array("theme" => 6);
        $functions = array();

        try {
            $this->sandbox->checkSecurity(
                [],
                ['theme'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"container\">
    <div class=\"row margin-bottom-50\">
      <div class=\"col-md-12\"> 
        <!-- Slider -->
        <div class=\"property-slider default\"> 
\t\t\t<a href=\"images/single-property-01.jpg\" data-background-image=\"";
        // line 6
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/banner.jpg");
        echo "\" class=\"item mfp-gallery\"></a> 
\t\t\t<a href=\"images/single-property-02.jpg\" data-background-image=\"";
        // line 7
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/download.jpg");
        echo "\" class=\"item mfp-gallery\"></a> 
\t\t\t<a href=\"images/single-property-03.jpg\" data-background-image=\"";
        // line 8
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/banner.jpg");
        echo "\" class=\"item mfp-gallery\"></a> 
\t\t\t<a href=\"images/single-property-04.jpg\" data-background-image=\"";
        // line 9
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/banner.jpg");
        echo "\" class=\"item mfp-gallery\"></a> 
\t\t\t<a href=\"images/single-property-05.jpg\" data-background-image=\"";
        // line 10
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/banner.jpg");
        echo "\" class=\"item mfp-gallery\"></a> 
\t\t\t<a href=\"images/single-property-06.jpg\" data-background-image=\"";
        // line 11
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/banner.jpg");
        echo "\" class=\"item mfp-gallery\"></a> 
\t\t</div>
        <!-- Slider Thumbs -->
        <div class=\"property-slider-nav\">
          <!--<div class=\"item\"><img src=\"";
        // line 15
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/banner.jpg");
        echo "\" alt=\"\"></div>-->
          <!--<div class=\"item\"><img src=\"";
        // line 16
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/banner.jpg");
        echo "\" alt=\"\"></div>-->
          <!--<div class=\"item\"><img src=\"";
        // line 17
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/banner.jpg");
        echo "\" alt=\"\"></div>-->
          <!--<div class=\"item\"><img src=\"";
        // line 18
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/banner.jpg");
        echo "\" alt=\"\"></div>-->
          <!--<div class=\"item\"><img src=\"";
        // line 19
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/banner.jpg");
        echo "\" alt=\"\"></div>-->
          <!--<div class=\"item\"><img src=\"";
        // line 20
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/banner.jpg");
        echo "\" alt=\"\"></div>-->
        </div>
      </div>
    </div>
  </div>";
    }

    public function getTemplateName()
    {
        return "/home/weslquicksite/public_html/weslnew/themes/WESL/partials/slider/slide.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  116 => 20,  112 => 19,  108 => 18,  104 => 17,  100 => 16,  96 => 15,  89 => 11,  85 => 10,  81 => 9,  77 => 8,  73 => 7,  69 => 6,  62 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"container\">
    <div class=\"row margin-bottom-50\">
      <div class=\"col-md-12\"> 
        <!-- Slider -->
        <div class=\"property-slider default\"> 
\t\t\t<a href=\"images/single-property-01.jpg\" data-background-image=\"{{ 'assets/img/banner.jpg' | theme}}\" class=\"item mfp-gallery\"></a> 
\t\t\t<a href=\"images/single-property-02.jpg\" data-background-image=\"{{ 'assets/images/download.jpg' | theme}}\" class=\"item mfp-gallery\"></a> 
\t\t\t<a href=\"images/single-property-03.jpg\" data-background-image=\"{{ 'assets/img/banner.jpg' | theme}}\" class=\"item mfp-gallery\"></a> 
\t\t\t<a href=\"images/single-property-04.jpg\" data-background-image=\"{{ 'assets/img/banner.jpg' | theme}}\" class=\"item mfp-gallery\"></a> 
\t\t\t<a href=\"images/single-property-05.jpg\" data-background-image=\"{{ 'assets/img/banner.jpg' | theme}}\" class=\"item mfp-gallery\"></a> 
\t\t\t<a href=\"images/single-property-06.jpg\" data-background-image=\"{{ 'assets/img/banner.jpg' | theme}}\" class=\"item mfp-gallery\"></a> 
\t\t</div>
        <!-- Slider Thumbs -->
        <div class=\"property-slider-nav\">
          <!--<div class=\"item\"><img src=\"{{ 'assets/img/banner.jpg' | theme}}\" alt=\"\"></div>-->
          <!--<div class=\"item\"><img src=\"{{ 'assets/img/banner.jpg' | theme}}\" alt=\"\"></div>-->
          <!--<div class=\"item\"><img src=\"{{ 'assets/img/banner.jpg' | theme}}\" alt=\"\"></div>-->
          <!--<div class=\"item\"><img src=\"{{ 'assets/img/banner.jpg' | theme}}\" alt=\"\"></div>-->
          <!--<div class=\"item\"><img src=\"{{ 'assets/img/banner.jpg' | theme}}\" alt=\"\"></div>-->
          <!--<div class=\"item\"><img src=\"{{ 'assets/img/banner.jpg' | theme}}\" alt=\"\"></div>-->
        </div>
      </div>
    </div>
  </div>", "/home/weslquicksite/public_html/weslnew/themes/WESL/partials/slider/slide.htm", "");
    }
}
