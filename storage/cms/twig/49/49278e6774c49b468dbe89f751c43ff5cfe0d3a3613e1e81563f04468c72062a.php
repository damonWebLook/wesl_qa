<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /home2/demowesl/public_html/weslnew2/themes/WESL/partials/video/adData.htm */
class __TwigTemplate_9caac0223257895eebcd73143e815723b741b957908e79bee6a8efcb07947404 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = array("if" => 14, "for" => 19);
        $filters = array("escape" => 21);
        $functions = array();

        try {
            $this->sandbox->checkSecurity(
                ['if', 'for'],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"\" id=\"adData\">
    <!--TODO: ADD total view below-->
     <!--<h5>Total Views: </h5>-->
    <table>
        <thead>

        </thead>
        <tr>
            <th>#</th>
            <th>User's Name</th>
            <th>IP</th>
            <th>Seen At</th>
        </tr>
        ";
        // line 14
        if ((($context["adData"] ?? null) == null)) {
            // line 15
            echo "            <tr>
               <td colspan=\"4\"> Please Select an Advertisement to Continue</td> 
            </tr>
        ";
        } else {
            // line 19
            echo "            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["adData"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["data"]) {
                // line 20
                echo "            <tr>
                <td>";
                // line 21
                echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["data"], "no", [], "any", false, false, true, 21), 21, $this->source), "html", null, true);
                echo "</td>
                <td>";
                // line 22
                echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["data"], "userName", [], "any", false, false, true, 22), 22, $this->source), "html", null, true);
                echo "</td>
                <td>";
                // line 23
                echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["data"], "ip", [], "any", false, false, true, 23), 23, $this->source), "html", null, true);
                echo "</td>
                <td>";
                // line 24
                echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["data"], "seenAt", [], "any", false, false, true, 24), 24, $this->source), "html", null, true);
                echo "</td>
            </tr>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['data'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 27
            echo "        ";
        }
        // line 28
        echo "    </table>
</div>";
    }

    public function getTemplateName()
    {
        return "/home2/demowesl/public_html/weslnew2/themes/WESL/partials/video/adData.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  117 => 28,  114 => 27,  105 => 24,  101 => 23,  97 => 22,  93 => 21,  90 => 20,  85 => 19,  79 => 15,  77 => 14,  62 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"\" id=\"adData\">
    <!--TODO: ADD total view below-->
     <!--<h5>Total Views: </h5>-->
    <table>
        <thead>

        </thead>
        <tr>
            <th>#</th>
            <th>User's Name</th>
            <th>IP</th>
            <th>Seen At</th>
        </tr>
        {% if adData==null%}
            <tr>
               <td colspan=\"4\"> Please Select an Advertisement to Continue</td> 
            </tr>
        {% else%}
            {% for data in adData%}
            <tr>
                <td>{{ data.no }}</td>
                <td>{{ data.userName}}</td>
                <td>{{ data.ip}}</td>
                <td>{{ data.seenAt}}</td>
            </tr>
            {% endfor %}
        {% endif %}
    </table>
</div>", "/home2/demowesl/public_html/weslnew2/themes/WESL/partials/video/adData.htm", "");
    }
}
