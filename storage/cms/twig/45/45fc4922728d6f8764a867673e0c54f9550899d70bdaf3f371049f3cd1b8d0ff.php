<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /home2/demowesl/public_html/weslnew2/themes/WESL/pages/profileview.htm */
class __TwigTemplate_3ce8b8efa5fbba48b19f71473f44499cabf3126adfa35fde1d0637f5a5f3f96b extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = array("if" => 28, "partial" => 56, "for" => 117);
        $filters = array("escape" => 7, "theme" => 35, "media" => 198);
        $functions = array("form_ajax" => 66, "form_close" => 87);

        try {
            $this->sandbox->checkSecurity(
                ['if', 'partial', 'for'],
                ['escape', 'theme', 'media'],
                ['form_ajax', 'form_close']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!-- Titlebar -->
<div class=\"parallax titlebar\" data-background=\"images/listings-parallax.jpg\" data-color=\"rgba(48, 48, 48, 1)\" data-color-opacity=\"0.8\" data-img-width=\"800\" data-img-height=\"505\">
    <div id=\"titlebar\">
      <div class=\"container\">
        <div class=\"row\">
          <div class=\"col-md-12\">
            <h2>";
        // line 7
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["agent"] ?? null), "agent", [], "any", false, false, true, 7), "first_name", [], "any", false, false, true, 7), 7, $this->source), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["agent"] ?? null), "agent", [], "any", false, false, true, 7), "last_name", [], "any", false, false, true, 7), 7, $this->source), "html", null, true);
        echo "</h2>
            <!-- Breadcrumbs -->
            <nav id=\"breadcrumbs\">
              <ul>
                <li><a href=\"../home\">Home</a></li>
                 <li><a href=\"../agentlist\">Agents</a></li>
                <li>";
        // line 13
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["agent"] ?? null), "agent", [], "any", false, false, true, 13), "first_name", [], "any", false, false, true, 13), 13, $this->source), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["agent"] ?? null), "agent", [], "any", false, false, true, 13), "last_name", [], "any", false, false, true, 13), 13, $this->source), "html", null, true);
        echo "</li>
              </ul>
            </nav>
          </div>
        </div>
      </div>
    </div>
</div>
  <!-- Content -->
  <div class=\"container\" id=\"Content\">
    <div class=\"row\">
      <div class=\"col-md-12\"> 
        <!-- Agency -->
        <div class=\"agent agents-profile agency margin-bottom-40\"> 
        <a href=\"#\" class=\"utf-agent-avatar\">
          ";
        // line 28
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["agent"] ?? null), "agent_user", [], "any", false, false, true, 28), "avatar", [], "any", false, false, true, 28), "path", [], "any", false, false, true, 28)) {
            // line 29
            echo "  
          
          <img src=\"";
            // line 31
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["agent"] ?? null), "agent_user", [], "any", false, false, true, 31), "avatar", [], "any", false, false, true, 31), "path", [], "any", false, false, true, 31), 31, $this->source), "html", null, true);
            echo "\" alt=\"\">
        
        ";
        } else {
            // line 34
            echo "        
          <img src=\"";
            // line 35
            echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/avaterplaceholder.jpg");
            echo "\" alt=\"\">
         
        ";
        }
        // line 37
        echo " 
        </a>
          <div class=\"utf-agent-content\">
            <div class=\"utf-agent-name\">
              <h4><a href=\"agency-profile.html\">";
        // line 41
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["agent"] ?? null), "agent", [], "any", false, false, true, 41), "first_name", [], "any", false, false, true, 41), 41, $this->source), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["agent"] ?? null), "agent", [], "any", false, false, true, 41), "last_name", [], "any", false, false, true, 41), 41, $this->source), "html", null, true);
        echo " </a></h4>
\t\t\t  <span>Agent In ";
        // line 42
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["agent"] ?? null), "country", [], "any", false, false, true, 42), "name", [], "any", false, false, true, 42), 42, $this->source), "html", null, true);
        echo "  Town ";
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["agent"] ?? null), "city", [], "any", false, false, true, 42), "name", [], "any", false, false, true, 42), 42, $this->source), "html", null, true);
        echo "  </span>
            <ul class=\"utf-agent-contact-details\">
\t\t\t\t      <li><i class=\"icon-feather-smartphone\"></i>";
        // line 44
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["agent"] ?? null), "agent", [], "any", false, false, true, 44), "agent_contact", [], "any", false, false, true, 44), 44, $this->source), "html", null, true);
        echo "</li>
\t\t\t\t      <li><i class=\"icon-material-outline-email\"></i><a href=\"#\">";
        // line 45
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["agent"] ?? null), "agent", [], "any", false, false, true, 45), "email", [], "any", false, false, true, 45), 45, $this->source), "html", null, true);
        echo "</a></li>
\t\t\t      </ul> 
\t\t\t       <ul class=\"utf-social-icons\">
\t\t\t\t        <li><a class=\"facebook\" href=\"#\"><i class=\"icon-facebook\"></i></a></li>
\t\t\t\t        <li><a class=\"twitter\" href=\"#\"><i class=\"icon-twitter\"></i></a></li>
\t\t\t\t        <li><a class=\"linkedin\" href=\"#\"><i class=\"icon-linkedin\"></i></a></li>
\t\t\t\t        <li><a class=\"instagram\" href=\"#\"><i class=\"icon-instagram\"></i></a></li>
\t\t\t\t        <li><a class=\"gplus\" href=\"#\"><i class=\"icon-gplus\"></i></a></li>
\t\t\t       </ul>
      </div>
      
      ";
        // line 56
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("service/servicelist"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 57
        echo "\t\t\t<div class=\"clearfix\"></div>
            <p>";
        // line 58
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["agent"] ?? null), "agent", [], "any", false, false, true, 58), "description", [], "any", false, false, true, 58), 58, $this->source), "html", null, true);
        echo "</p>
            <div>
              Avg. Rating ";
        // line 60
        $context['__cms_partial_params'] = [];
        $context['__cms_partial_params']['star'] = ($context["avgRating"] ?? null)        ;
        $context['__cms_partial_params']['hide'] = ($context["hideRating"] ?? null)        ;
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("agents/star"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 61
        echo "            </div>
            ";
        // line 62
        $context['__cms_partial_params'] = [];
        $context['__cms_partial_params']['permission'] = ($context["ratingPermission"] ?? null)        ;
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("agents/rating"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 63
        echo "            <!-- Rating Form -->
            
            ";
        // line 65
        if ((($context["ratingPermission"] ?? null) == 0)) {
            // line 66
            echo "                ";
            echo call_user_func_array($this->env->getFunction('form_ajax')->getCallable(), ["ajax", "onChange", ["class" => "rating", "update" => ["agents/ratingResponse" => "#rating"]]]);
            echo "
                <fieldset class=\"rating\">
                    <input type=\"radio\" id=\"star5\" name=\"rating\" value=\"5\" /><label class = \"full\" for=\"star5\" title=\"Awesome - 5 stars\"></label>
                    
                    <input type=\"radio\" id=\"star4\" name=\"rating\" value=\"4\" /><label class = \"full\" for=\"star4\" title=\"Pretty good - 4 stars\"></label>
                    
                    <input type=\"radio\" id=\"star3\" name=\"rating\" value=\"3\" /><label class = \"full\" for=\"star3\" title=\"Meh - 3 stars\"></label>
                    
                    <input type=\"radio\" id=\"star2\" name=\"rating\" value=\"2\" /><label class = \"full\" for=\"star2\" title=\"Kinda bad - 2 stars\"></label>
                    
                    <input type=\"radio\" id=\"star1\" name=\"rating\" value=\"1\" /><label class = \"full\" for=\"star1\" title=\"Sucks big time - 1 star\"></label>
                    
                </fieldset>
                <br>
                <p style=\"margin-right: 75%;\">Comments</p>
                <!-- <input type=\"text\" name=\"comment\"> -->
                <textarea name=\"comment\" id=\"comment\" cols=\"50\" rows=\"1\"></textarea>
                <button data-request=\"onSubmit\"
                        data-request-update=\"'agents/ratingResponse' : '#rating'\"
                        onclick=\"getRating()\"
                >Submit</button>
                ";
            // line 87
            echo call_user_func_array($this->env->getFunction('form_close')->getCallable(), ["close"]);
            echo "
            ";
        }
        // line 89
        echo "          </div>
        </div>
\t  </div>
    </div>
  </div>
  
  <!-- Content -->
  <div class=\"container\">
    <!-- Currency Calculator -->
       <div class=\"row sticky-wrapper col-xs-12 col-md-12\">
      <div class=\"col-lg-12 col-md-12 col-xs-12\">
        <div class=\"\">
                  
        <div class=\"utf-desc-headline-item margin-top-0\">
          <h3><i class=\"icon-material-outline-description\"></i> Currency Calculator</h3>
      </div>
        <!-- Form goes here -->
        <div class=\"col-lg-12 col-md-12 col-xs-12\">
        ";
        // line 107
        echo call_user_func_array($this->env->getFunction('form_ajax')->getCallable(), ["ajax", "onSearch", ["class" => "currency", "update" => ["currency/calc" => "#calc"]]]);
        echo "
       
          <div class=\"col-lg-2 col-md-2 col-xs-12\">
            <label style=\"text-align: center;\" >Amount</label>
            <input type=\"number\" name=\"amount\" placeholder=\"Amount\">
          </div>
          <div class=\"col-lg-2 col-md-2 col-xs-12\">
            <label  style=\"text-align: center;\">From</label>
            <select name=\"from\">

            ";
        // line 117
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["calcurrency"] ?? null));
        foreach ($context['_seq'] as $context["key"] => $context["value"]) {
            // line 118
            echo "            <option value=\"";
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed($context["key"], 118, $this->source), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed($context["value"], 118, $this->source), "html", null, true);
            echo "</option>
              ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['value'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 120
        echo "          </select></div>
          <div class=\"col-lg-2 col-md-2 col-xs-12\">
            <label style=\"text-align: center;\" >To</label>
            <select name=\"to\">
              ";
        // line 124
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["calcurrency"] ?? null));
        foreach ($context['_seq'] as $context["key"] => $context["value"]) {
            // line 125
            echo "              <option value=\"";
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed($context["key"], 125, $this->source), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed($context["value"], 125, $this->source), "html", null, true);
            echo "</option>
              ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['value'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 127
        echo "            </select>
          </div>
          <div class=\"col-lg-2 col-md-2 col-xs-12\">
            <button class=\"button utf-search-btn-item\"
            id=\"calcButton\"
            type=\"submit\" 
            data-request=\"onSearch\"
            data-request-update=\"'currency/calc' : '#calc'\"
            style=\"width: 100%; margin-left: 0% !important;\"
            >Calculate</button>
          </div>
      
        ";
        // line 139
        echo call_user_func_array($this->env->getFunction('form_close')->getCallable(), ["close"]);
        echo "
          <div class=\"col-lg-4 col-md-4 col-xs-12\">
            ";
        // line 141
        $context['__cms_partial_params'] = [];
        $context['__cms_partial_params']['calcresult'] = ($context["calcresult"] ?? null)        ;
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("currency/calc"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 142
        echo "          </div>

      </div>
        <!-- Partial here -->




        </div>
        </div>

    </div>
    <!-- Currency Rates -->
    <div class=\"row sticky-wrapper col-xs-12 col-md-12\">
      <div class=\"col-lg-9 col-md-9 col-xs-12\">
        <div class=\"\">
          <div class=\"utf-desc-headline-item margin-top-0\">
              <h3><i class=\"icon-material-outline-description\"></i> My Currency Rates</h3>
          </div>
          <!-- Sorting -->
          ";
        // line 162
        echo call_user_func_array($this->env->getFunction('form_ajax')->getCallable(), ["ajax", "onFilterSearch", ["class" => "form-items", "update" => ["currency/currencytable" => "#currency_list"]]]);
        echo "
            <div class=\"utf-sort-box-aera\">
              <div class=\"sort-by\">
                <label>From:</label>
                <div class=\"sort-by-select\">
                  <select data-placeholder=\"Default Properties\" onchange=\"getFilterList()\" class=\"utf-chosen-select-single-item\" name=\"currency_id\">
                    <option>Select Base Currency</option>
                    ";
        // line 169
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["baseCurrency"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["currency"]) {
            // line 170
            echo "                    <option value=\"";
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["currency"], "id", [], "any", false, false, true, 170), 170, $this->source), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["currency"], "name", [], "any", false, false, true, 170), 170, $this->source), "html", null, true);
            echo "</option>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['currency'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 172
        echo "                  </select>
                </div>
              </div>
              <!-- <div class=\"utf-layout-switcher\"> 
                  <a href=\"#\" class=\"list\"><i class=\"sl sl-icon-list\"></i></a> 
                  <a href=\"#\" class=\"grid\"><i class=\"sl sl-icon-grid\"></i></a> 
              </div> -->
            </div>
          
          <!-- Listings -->
        </div>
        ";
        // line 183
        echo call_user_func_array($this->env->getFunction('form_close')->getCallable(), ["close"]);
        echo "
        ";
        // line 184
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("currency/currencytable"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 185
        echo "      </div>
      <!-- Video -->

        <div class=\"col-md-3 col-lg-3 col-xs-12\">

          <div class=\"row\">
            <i class=\"fas fa-ad\" style=\"font-size: 45px;\"></i>
            <div class=\"media-wrapper\" id=\"video1\">
              <div id=\"overlay\">
                <div id=\"textSuccess\">You earned ";
        // line 194
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["video"] ?? null), "point", [], "any", false, false, true, 194), 194, $this->source), "html", null, true);
        echo " points for the video</div>
                <div id=\"textFail\">You already earned maximum points for the video</div>
              </div>
              <div class=\"media-player\">
                 <video src=\"..";
        // line 198
        echo $this->extensions['System\Twig\Extension']->mediaFilter($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["video"] ?? null), "path", [], "any", false, false, true, 198), 198, $this->source));
        echo "\" id=\"media-video\"></video>
              </div>
              <div id='media-controls'>
                  <!-- <button id='play-pause-button' class='button play' title='play'
                  onclick='togglePlayPause();' >Play</button> -->
                  <div class=\"row\">        
                      <div class=\"hideButton\">
                      <button class=\"ad\" id=\"playButton\" onclick='togglePlayPause();'>
                        <i class=\"fas fa-play-circle\"></i>
                      </button>
                      <button id='mute-button' class='mute' title='mute'
                      onclick='toggleMute();' value=\"false\"><i class=\"fas fa-volume-mute\"></i></button>
         
                      <div style=\"padding-top: 5px;\">
                        <button type=\"submit\" id=\"proceed\" style=\"display: none;\" onclick=\"addPoint()\">Skip</button>
                      </div>

                  </div>
                  
                  </div>
                  
                  
               </div>
          </div>
          <input type=\"hidden\" name=\"id\" id=\"getId\" value=\"";
        // line 222
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["video"] ?? null), "id", [], "any", false, false, true, 222), 222, $this->source), "html", null, true);
        echo "\" name=\"videoId\">
          <input type=\"hidden\" id=\"getpoint\"  value=\"";
        // line 223
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["video"] ?? null), "point", [], "any", false, false, true, 223), 223, $this->source), "html", null, true);
        echo "\">
          <input type=\"hidden\" id=\"gettime\" value=\"";
        // line 224
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["video"] ?? null), "time", [], "any", false, false, true, 224), 224, $this->source), "html", null, true);
        echo "\">
          <input type=\"hidden\" id=\"point\" name=\"finalPoints\">
            <!-- <span>
              <img src=\"https://via.placeholder.com/400x350?text=Video+Placeholder\" alt=\"\" width=\"100%\">
            </span> -->
          </div>
        </div>
        <script src=\"";
        // line 231
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/mediaPlayer.js");
        echo "\"></script>









            
          
          
    </div>\t<!-- Listing Item -->

    <!-- Reviews -->
    <div class=\"row sticky-wrapper col-xs-12 col-md-12\">
      <div class=\"col-lg-12 col-md-12 col-xs-12\">
        <div class=\"\">
          <div class=\"utf-desc-headline-item margin-top-0\">
            <h3><i class=\"icon-material-outline-description\"></i> Reviews</h3>
        </div>
                    <div class=\"col-lg-11 col-md-11 col-xs-12\">

      
          <div class=\"containerCard\" style=\"line-height: 20px !important;\">
                <div class=\"row\">
                  <ul  id=\"menu\" style=\"list-style-type: none;margin-bottom: 0px;\"> 
                    <li class=\"\" id=\"1\"><i class=\"fa fa-star ";
        // line 259
        if (((twig_get_attribute($this->env, $this->source, ($context["showRating"] ?? null), "avg", [], "any", false, false, true, 259) == 1) || (twig_get_attribute($this->env, $this->source, ($context["showRating"] ?? null), "avg", [], "any", false, false, true, 259) > 1))) {
            echo "checked";
        }
        echo "\"></i></li>
                    <li class=\"\" id=\"2\"><i class=\"fa fa-star ";
        // line 260
        if (((twig_get_attribute($this->env, $this->source, ($context["showRating"] ?? null), "avg", [], "any", false, false, true, 260) == 2) || (twig_get_attribute($this->env, $this->source, ($context["showRating"] ?? null), "avg", [], "any", false, false, true, 260) > 2))) {
            echo "checked";
        }
        echo "\"></i></li>
                    <li class=\"\" id=\"3\"><i class=\"fa fa-star ";
        // line 261
        if (((twig_get_attribute($this->env, $this->source, ($context["showRating"] ?? null), "avg", [], "any", false, false, true, 261) == 3) || (twig_get_attribute($this->env, $this->source, ($context["showRating"] ?? null), "avg", [], "any", false, false, true, 261) > 3))) {
            echo "checked";
        }
        echo "\"></i></li>
                    <li class=\"\" id=\"4\"><i class=\"fa fa-star ";
        // line 262
        if (((twig_get_attribute($this->env, $this->source, ($context["showRating"] ?? null), "avg", [], "any", false, false, true, 262) == 4) || (twig_get_attribute($this->env, $this->source, ($context["showRating"] ?? null), "avg", [], "any", false, false, true, 262) > 4))) {
            echo "checked";
        }
        echo "\"></i></li>
                    <li class=\"\" id=\"5\"><i class=\"fa fa-star ";
        // line 263
        if (((twig_get_attribute($this->env, $this->source, ($context["showRating"] ?? null), "avg", [], "any", false, false, true, 263) == 5) || (twig_get_attribute($this->env, $this->source, ($context["showRating"] ?? null), "avg", [], "any", false, false, true, 263) > 5))) {
            echo "checked";
        }
        echo "\"></i></li>
              
                    <span style=\"display: inline;font-size: 18px\">
                      ";
        // line 266
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["showRating"] ?? null), "avg", [], "any", false, false, true, 266), 266, $this->source), "html", null, true);
        echo " out of 5
                    </span>
                  </ul>
                  <span style=\"font-size: 12px;margin-left:10px ;margin-top: 0px;\">
                    ";
        // line 270
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["showRating"] ?? null), "count", [], "any", false, false, true, 270), 270, $this->source), "html", null, true);
        echo " customer ratings
                  </span>
                </div>
                <div class=\"col-lg-10 col-md-10 col-xs-12\">
                  <!-- 5 -->
                  <div class=\"row\">
                    <div class=\"col-lg-1 col-md-1 col-xs-3\" style=\"padding: 0px 0px;\">
                      <p>5 star</p>
                    </div>
                    <div class=\"col-lg-3 col-md-3 col-xs-6\" >
                 
                      <div class=\"progress\"  style=\"margin-top: 5px;\">
                        <div class=\"progress-bar\" role=\"progressbar\" style=\"width:  ";
        // line 282
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["showRating"] ?? null), "five", [], "any", false, false, true, 282), 282, $this->source), "html", null, true);
        echo "%\" aria-valuenow=\"25\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>
                      </div>
                    </div>
                    <div class=\"col-lg-3 col-md-3 col-xs-3\">
                      <p>";
        // line 286
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["showRating"] ?? null), "five", [], "any", false, false, true, 286), 286, $this->source), "html", null, true);
        echo "%</p>
                      </div>
                      
                  </div>
                  <!-- 4 -->
                  <div class=\"row\">
                    <div class=\"col-lg-1 col-md-1 col-xs-3\" style=\"padding: 0px 0px;\">
                      <p>4 star</p>
                    </div>
                    <div class=\"col-lg-3 col-md-3 col-xs-6\" >
                 
                      <div class=\"progress\"  style=\"margin-top: 5px;\">
                        <div class=\"progress-bar\" role=\"progressbar\" style=\"width:  ";
        // line 298
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["showRating"] ?? null), "four", [], "any", false, false, true, 298), 298, $this->source), "html", null, true);
        echo "%\" aria-valuenow=\"25\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>
                      </div>
                    </div>
                    <div class=\"col-lg-3 col-md-3 col-xs-3\">
                      <p>";
        // line 302
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["showRating"] ?? null), "four", [], "any", false, false, true, 302), 302, $this->source), "html", null, true);
        echo "%</p>
                      </div>

                  </div>
                  <!-- 3 -->
                  <div class=\"row\">
                    <div class=\"col-lg-1 col-md-1 col-xs-3\" style=\"padding: 0px 0px;\">
                      <p>3 star</p>
                    </div>
                    <div class=\"col-lg-3 col-md-3 col-xs-6\" >
                 
                      <div class=\"progress\"  style=\"margin-top: 5px;\">
                        <div class=\"progress-bar\" role=\"progressbar\" style=\"width:  ";
        // line 314
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["showRating"] ?? null), "three", [], "any", false, false, true, 314), 314, $this->source), "html", null, true);
        echo "%\" aria-valuenow=\"25\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>
                      </div>
                    </div>
                    <div class=\"col-lg-3 col-md-3 col-xs-3\">
                      <p>";
        // line 318
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["showRating"] ?? null), "three", [], "any", false, false, true, 318), 318, $this->source), "html", null, true);
        echo "%</p>
                      </div>

                  </div>
                  <!-- 2 -->
                  <div class=\"row\">
                    <div class=\"col-lg-1 col-md-1 col-xs-3\" style=\"padding: 0px 0px;\">
                      <p>2 star</p>
                    </div>
                    <div class=\"col-lg-3 col-md-3 col-xs-6\" >
                 
                      <div class=\"progress\"  style=\"margin-top: 5px;\">
                        <div class=\"progress-bar\" role=\"progressbar\" style=\"width:  ";
        // line 330
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["showRating"] ?? null), "two", [], "any", false, false, true, 330), 330, $this->source), "html", null, true);
        echo "%\" aria-valuenow=\"25\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>
                      </div>
                    </div>
                    <div class=\"col-lg-3 col-md-3 col-xs-3\">
                      <p>";
        // line 334
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["showRating"] ?? null), "two", [], "any", false, false, true, 334), 334, $this->source), "html", null, true);
        echo "%</p>
                      </div>

                  </div>
                  <!-- 1 -->
                  <div class=\"row\">
                    <div class=\"col-lg-1 col-md-1 col-xs-3\" style=\"padding: 0px 0px;\">
                      <p>1 star</p>
                    </div>
                    <div class=\"col-lg-3 col-md-3 col-xs-6\" >
                 
                      <div class=\"progress\"  style=\"margin-top: 5px;\">
                        <div class=\"progress-bar\" role=\"progressbar\" style=\"width:  ";
        // line 346
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["showRating"] ?? null), "one", [], "any", false, false, true, 346), 346, $this->source), "html", null, true);
        echo "%\" aria-valuenow=\"25\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>
                      </div>
                    </div>
                    <div class=\"col-lg-3 col-md-3 col-xs-3\">
                      <p>";
        // line 350
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["showRating"] ?? null), "one", [], "any", false, false, true, 350), 350, $this->source), "html", null, true);
        echo "%</p>
                      </div>

                  </div>
                </div>

              </div>

  
      </div>
        </div>
      </div>


    </div>
    <!-- Reviews End -->
  </div>


    
\t\t
</div>
<script>
  function getFilterList(){
    setTimeout(function(){
      \$('.form-items').request('onFilterSearch')
    }),1000;
  }
  
  function getRating(){

    setTimeout(function(){
                    location = ''
                  },300)
    


  }
</script>";
    }

    public function getTemplateName()
    {
        return "/home2/demowesl/public_html/weslnew2/themes/WESL/pages/profileview.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  625 => 350,  618 => 346,  603 => 334,  596 => 330,  581 => 318,  574 => 314,  559 => 302,  552 => 298,  537 => 286,  530 => 282,  515 => 270,  508 => 266,  500 => 263,  494 => 262,  488 => 261,  482 => 260,  476 => 259,  445 => 231,  435 => 224,  431 => 223,  427 => 222,  400 => 198,  393 => 194,  382 => 185,  378 => 184,  374 => 183,  361 => 172,  350 => 170,  346 => 169,  336 => 162,  314 => 142,  309 => 141,  304 => 139,  290 => 127,  279 => 125,  275 => 124,  269 => 120,  258 => 118,  254 => 117,  241 => 107,  221 => 89,  216 => 87,  191 => 66,  189 => 65,  185 => 63,  180 => 62,  177 => 61,  171 => 60,  166 => 58,  163 => 57,  159 => 56,  145 => 45,  141 => 44,  134 => 42,  128 => 41,  122 => 37,  116 => 35,  113 => 34,  107 => 31,  103 => 29,  101 => 28,  81 => 13,  70 => 7,  62 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "/home2/demowesl/public_html/weslnew2/themes/WESL/pages/profileview.htm", "");
    }
}
