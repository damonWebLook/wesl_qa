<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /home/weslquicksite/public_html/weslnew/themes/WESL/pages/profileview.htm */
class __TwigTemplate_8de82d88b24e9cdafb0a02c36bdc14c59f1c087c577f900ac8070e6c20cc7988 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = array("if" => 27, "partial" => 55, "for" => 116);
        $filters = array("escape" => 7, "theme" => 34, "media" => 197);
        $functions = array("form_ajax" => 65, "form_close" => 86);

        try {
            $this->sandbox->checkSecurity(
                ['if', 'partial', 'for'],
                ['escape', 'theme', 'media'],
                ['form_ajax', 'form_close']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!-- Titlebar -->
<div class=\"parallax titlebar\" data-background=\"images/listings-parallax.jpg\" data-color=\"rgba(48, 48, 48, 1)\" data-color-opacity=\"0.8\" data-img-width=\"800\" data-img-height=\"505\">
    <div id=\"titlebar\">
      <div class=\"container\">
        <div class=\"row\">
          <div class=\"col-md-12\">
            <h2>";
        // line 7
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["agent"] ?? null), "agent", [], "any", false, false, true, 7), "first_name", [], "any", false, false, true, 7), 7, $this->source), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["agent"] ?? null), "agent", [], "any", false, false, true, 7), "last_name", [], "any", false, false, true, 7), 7, $this->source), "html", null, true);
        echo "</h2>
            <!-- Breadcrumbs -->
            <nav id=\"breadcrumbs\">
              <ul>
                <li><a href=\"../home\">Home</a></li>
                 <li><a href=\"../agentlist\">Agents</a></li>
                <li>";
        // line 13
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["agent"] ?? null), "agent", [], "any", false, false, true, 13), "first_name", [], "any", false, false, true, 13), 13, $this->source), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["agent"] ?? null), "agent", [], "any", false, false, true, 13), "last_name", [], "any", false, false, true, 13), 13, $this->source), "html", null, true);
        echo "</li>
              </ul>
            </nav>
          </div>
        </div>
      </div>
    </div>
</div>
  <!-- Content -->
  <div class=\"container\" id=\"Content\">
    <div class=\"row\">
      <div class=\"col-md-12\"> 
        <!-- Agency -->
        <div class=\"agent agents-profile agency margin-bottom-40\"> <a href=\"agency-profile.html\" class=\"utf-agent-avatar\">
          ";
        // line 27
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["agent"] ?? null), "agent_user", [], "any", false, false, true, 27), "avatar", [], "any", false, false, true, 27), "path", [], "any", false, false, true, 27)) {
            // line 28
            echo "  
          
          <img src=\"";
            // line 30
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["agent"] ?? null), "agent_user", [], "any", false, false, true, 30), "avatar", [], "any", false, false, true, 30), "path", [], "any", false, false, true, 30), 30, $this->source), "html", null, true);
            echo "\" alt=\"\">
        
        ";
        } else {
            // line 33
            echo "        
          <img src=\"";
            // line 34
            echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/img/avaterplaceholder.jpg");
            echo "\" alt=\"\">
         
        ";
        }
        // line 36
        echo " 
        </a>
          <div class=\"utf-agent-content\">
            <div class=\"utf-agent-name\">
              <h4><a href=\"agency-profile.html\">";
        // line 40
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["agent"] ?? null), "agent", [], "any", false, false, true, 40), "first_name", [], "any", false, false, true, 40), 40, $this->source), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["agent"] ?? null), "agent", [], "any", false, false, true, 40), "last_name", [], "any", false, false, true, 40), 40, $this->source), "html", null, true);
        echo " </a></h4>
\t\t\t  <span>Agent In ";
        // line 41
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["agent"] ?? null), "country", [], "any", false, false, true, 41), "name", [], "any", false, false, true, 41), 41, $this->source), "html", null, true);
        echo "  Town ";
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["agent"] ?? null), "city", [], "any", false, false, true, 41), "name", [], "any", false, false, true, 41), 41, $this->source), "html", null, true);
        echo "  </span>
            <ul class=\"utf-agent-contact-details\">
\t\t\t\t      <li><i class=\"icon-feather-smartphone\"></i>";
        // line 43
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["agent"] ?? null), "agent", [], "any", false, false, true, 43), "agent_contact", [], "any", false, false, true, 43), 43, $this->source), "html", null, true);
        echo "</li>
\t\t\t\t      <li><i class=\"icon-material-outline-email\"></i><a href=\"#\">";
        // line 44
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["agent"] ?? null), "agent", [], "any", false, false, true, 44), "email", [], "any", false, false, true, 44), 44, $this->source), "html", null, true);
        echo "</a></li>
\t\t\t      </ul> 
\t\t\t       <ul class=\"utf-social-icons\">
\t\t\t\t        <li><a class=\"facebook\" href=\"#\"><i class=\"icon-facebook\"></i></a></li>
\t\t\t\t        <li><a class=\"twitter\" href=\"#\"><i class=\"icon-twitter\"></i></a></li>
\t\t\t\t        <li><a class=\"linkedin\" href=\"#\"><i class=\"icon-linkedin\"></i></a></li>
\t\t\t\t        <li><a class=\"instagram\" href=\"#\"><i class=\"icon-instagram\"></i></a></li>
\t\t\t\t        <li><a class=\"gplus\" href=\"#\"><i class=\"icon-gplus\"></i></a></li>
\t\t\t       </ul>
      </div>
      
      ";
        // line 55
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("service/servicelist"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 56
        echo "\t\t\t<div class=\"clearfix\"></div>
            <p>";
        // line 57
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["agent"] ?? null), "agent", [], "any", false, false, true, 57), "description", [], "any", false, false, true, 57), 57, $this->source), "html", null, true);
        echo "</p>
            <div>
              Avg. Rating ";
        // line 59
        $context['__cms_partial_params'] = [];
        $context['__cms_partial_params']['star'] = ($context["avgRating"] ?? null)        ;
        $context['__cms_partial_params']['hide'] = ($context["hideRating"] ?? null)        ;
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("agents/star"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 60
        echo "            </div>
            ";
        // line 61
        $context['__cms_partial_params'] = [];
        $context['__cms_partial_params']['permission'] = ($context["ratingPermission"] ?? null)        ;
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("agents/rating"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 62
        echo "            <!-- Rating Form -->
            
            ";
        // line 64
        if ((($context["ratingPermission"] ?? null) == 0)) {
            // line 65
            echo "                ";
            echo call_user_func_array($this->env->getFunction('form_ajax')->getCallable(), ["ajax", "onChange", ["class" => "rating", "update" => ["agents/ratingResponse" => "#rating"]]]);
            echo "
                <fieldset class=\"rating\">
                    <input type=\"radio\" id=\"star5\" name=\"rating\" value=\"5\" /><label class = \"full\" for=\"star5\" title=\"Awesome - 5 stars\"></label>
                    
                    <input type=\"radio\" id=\"star4\" name=\"rating\" value=\"4\" /><label class = \"full\" for=\"star4\" title=\"Pretty good - 4 stars\"></label>
                    
                    <input type=\"radio\" id=\"star3\" name=\"rating\" value=\"3\" /><label class = \"full\" for=\"star3\" title=\"Meh - 3 stars\"></label>
                    
                    <input type=\"radio\" id=\"star2\" name=\"rating\" value=\"2\" /><label class = \"full\" for=\"star2\" title=\"Kinda bad - 2 stars\"></label>
                    
                    <input type=\"radio\" id=\"star1\" name=\"rating\" value=\"1\" /><label class = \"full\" for=\"star1\" title=\"Sucks big time - 1 star\"></label>
                    
                </fieldset>
                <br>
                <p style=\"margin-right: 75%;\">Comments</p>
                <!-- <input type=\"text\" name=\"comment\"> -->
                <textarea name=\"comment\" id=\"comment\" cols=\"50\" rows=\"1\"></textarea>
                <button data-request=\"onSubmit\"
                        data-request-update=\"'agents/ratingResponse' : '#rating'\"
                        onclick=\"getRating()\"
                >Submit</button>
                ";
            // line 86
            echo call_user_func_array($this->env->getFunction('form_close')->getCallable(), ["close"]);
            echo "
            ";
        }
        // line 88
        echo "          </div>
        </div>
\t  </div>
    </div>
  </div>
  
  <!-- Content -->
  <div class=\"container\">
    <!-- Currency Calculator -->
       <div class=\"row sticky-wrapper col-xs-12 col-md-12\">
      <div class=\"col-lg-12 col-md-12 col-xs-12\">
        <div class=\"\">
                  
        <div class=\"utf-desc-headline-item margin-top-0\">
          <h3><i class=\"icon-material-outline-description\"></i> Currency Calculator</h3>
      </div>
        <!-- Form goes here -->
        <div class=\"col-lg-12 col-md-12 col-xs-12\">
        ";
        // line 106
        echo call_user_func_array($this->env->getFunction('form_ajax')->getCallable(), ["ajax", "onSearch", ["class" => "currency", "update" => ["currency/calc" => "#calc"]]]);
        echo "
       
          <div class=\"col-lg-2 col-md-2 col-xs-12\">
            <label style=\"text-align: center;\" >Amount</label>
            <input type=\"number\" name=\"amount\" placeholder=\"Enter Amount\">
          </div>
          <div class=\"col-lg-2 col-md-2 col-xs-12\">
            <label  style=\"text-align: center;\">From</label>
            <select name=\"from\">

            ";
        // line 116
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["calcurrency"] ?? null));
        foreach ($context['_seq'] as $context["key"] => $context["value"]) {
            // line 117
            echo "            <option value=\"";
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed($context["key"], 117, $this->source), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed($context["value"], 117, $this->source), "html", null, true);
            echo "</option>
              ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['value'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 119
        echo "          </select></div>
          <div class=\"col-lg-2 col-md-2 col-xs-12\">
            <label style=\"text-align: center;\" >To</label>
            <select name=\"to\">
              ";
        // line 123
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["calcurrency"] ?? null));
        foreach ($context['_seq'] as $context["key"] => $context["value"]) {
            // line 124
            echo "              <option value=\"";
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed($context["key"], 124, $this->source), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed($context["value"], 124, $this->source), "html", null, true);
            echo "</option>
              ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['value'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 126
        echo "            </select>
          </div>
          <div class=\"col-lg-2 col-md-2 col-xs-12\">
            <button class=\"button utf-search-btn-item\"
            id=\"calcButton\"
            type=\"submit\" 
            data-request=\"onSearch\"
            data-request-update=\"'currency/calc' : '#calc'\"
            style=\"width: 100%; margin-left: 0% !important;\"
            >Calculate</button>
          </div>
      
        ";
        // line 138
        echo call_user_func_array($this->env->getFunction('form_close')->getCallable(), ["close"]);
        echo "
          <div class=\"col-lg-4 col-md-4 col-xs-12\">
            ";
        // line 140
        $context['__cms_partial_params'] = [];
        $context['__cms_partial_params']['calcresult'] = ($context["calcresult"] ?? null)        ;
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("currency/calc"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 141
        echo "          </div>

      </div>
        <!-- Partial here -->




        </div>
        </div>

    </div>
    <!-- Currency Rates -->
    <div class=\"row sticky-wrapper col-xs-12 col-md-12\">
      <div class=\"col-lg-9 col-md-9 col-xs-12\">
        <div class=\"\">
          <div class=\"utf-desc-headline-item margin-top-0\">
              <h3><i class=\"icon-material-outline-description\"></i> My Currency Rates</h3>
          </div>
          <!-- Sorting -->
          ";
        // line 161
        echo call_user_func_array($this->env->getFunction('form_ajax')->getCallable(), ["ajax", "onFilterSearch", ["class" => "form-items", "update" => ["currency/currencytable" => "#currency_list"]]]);
        echo "
            <div class=\"utf-sort-box-aera\">
              <div class=\"sort-by\">
                <label>From:</label>
                <div class=\"sort-by-select\">
                  <select data-placeholder=\"Default Properties\" onchange=\"getFilterList()\" class=\"utf-chosen-select-single-item\" name=\"currency_id\">
                    <option>Select Base Currency</option>
                    ";
        // line 168
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["baseCurrency"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["currency"]) {
            // line 169
            echo "                    <option value=\"";
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["currency"], "id", [], "any", false, false, true, 169), 169, $this->source), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["currency"], "name", [], "any", false, false, true, 169), 169, $this->source), "html", null, true);
            echo "</option>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['currency'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 171
        echo "                  </select>
                </div>
              </div>
              <!-- <div class=\"utf-layout-switcher\"> 
                  <a href=\"#\" class=\"list\"><i class=\"sl sl-icon-list\"></i></a> 
                  <a href=\"#\" class=\"grid\"><i class=\"sl sl-icon-grid\"></i></a> 
              </div> -->
            </div>
          
          <!-- Listings -->
        </div>
        ";
        // line 182
        echo call_user_func_array($this->env->getFunction('form_close')->getCallable(), ["close"]);
        echo "
        ";
        // line 183
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("currency/currencytable"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 184
        echo "      </div>
      <!-- Video -->

        <div class=\"col-md-3 col-lg-3 col-xs-12\">

          <div class=\"row\">
            <i class=\"fas fa-ad\" style=\"font-size: 45px;\"></i>
            <div class=\"media-wrapper\" id=\"video1\">
              <div id=\"overlay\">
                <div id=\"textSuccess\">You earned ";
        // line 193
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["video"] ?? null), "point", [], "any", false, false, true, 193), 193, $this->source), "html", null, true);
        echo " points for the video</div>
                <div id=\"textFail\">You already earned maximum points for the video</div>
              </div>
              <div class=\"media-player\">
                 <video src=\"..";
        // line 197
        echo $this->extensions['System\Twig\Extension']->mediaFilter($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["video"] ?? null), "path", [], "any", false, false, true, 197), 197, $this->source));
        echo "\" id=\"media-video\"></video>
              </div>
              <div id='media-controls'>
                  <!-- <button id='play-pause-button' class='button play' title='play'
                  onclick='togglePlayPause();' >Play</button> -->
                  <div class=\"row\">        
                      <div class=\"hideButton\">
                      <button class=\"ad\" id=\"playButton\" onclick='togglePlayPause();'>
                        <i class=\"fas fa-play-circle\"></i>
                      </button>
                      <button id='mute-button' class='mute' title='mute'
                      onclick='toggleMute();' value=\"false\"><i class=\"fas fa-volume-mute\"></i></button>
         
                      <div style=\"padding-top: 5px;\">
                        <button type=\"submit\" id=\"proceed\" style=\"display: none;\" onclick=\"addPoint()\">Skip</button>
                      </div>

                  </div>
                  
                  </div>
                  
                  
               </div>
          </div>
          <input type=\"hidden\" name=\"id\" id=\"getId\" value=\"";
        // line 221
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["video"] ?? null), "id", [], "any", false, false, true, 221), 221, $this->source), "html", null, true);
        echo "\" name=\"videoId\">
          <input type=\"hidden\" id=\"getpoint\"  value=\"";
        // line 222
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["video"] ?? null), "point", [], "any", false, false, true, 222), 222, $this->source), "html", null, true);
        echo "\">
          <input type=\"hidden\" id=\"gettime\" value=\"";
        // line 223
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["video"] ?? null), "time", [], "any", false, false, true, 223), 223, $this->source), "html", null, true);
        echo "\">
          <input type=\"hidden\" id=\"point\" name=\"finalPoints\">
            <!-- <span>
              <img src=\"https://via.placeholder.com/400x350?text=Video+Placeholder\" alt=\"\" width=\"100%\">
            </span> -->
          </div>
        </div>
        <script src=\"";
        // line 230
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/mediaPlayer.js");
        echo "\"></script>









            
          
          
    </div>\t<!-- Listing Item -->

    <!-- Reviews -->
    <div class=\"row sticky-wrapper col-xs-12 col-md-12\">
      <div class=\"col-lg-12 col-md-12 col-xs-12\">
        <div class=\"\">
          <div class=\"utf-desc-headline-item margin-top-0\">
            <h3><i class=\"icon-material-outline-description\"></i> Reviews</h3>
        </div>
                    <div class=\"col-lg-11 col-md-11 col-xs-12\">

      
          <div class=\"containerCard\" style=\"line-height: 20px !important;\">
                <div class=\"row\">
                  <ul  id=\"menu\" style=\"list-style-type: none;margin-bottom: 0px;\"> 
                    <li class=\"\" id=\"1\"><i class=\"fa fa-star ";
        // line 258
        if (((twig_get_attribute($this->env, $this->source, ($context["showRating"] ?? null), "avg", [], "any", false, false, true, 258) == 1) || (twig_get_attribute($this->env, $this->source, ($context["showRating"] ?? null), "avg", [], "any", false, false, true, 258) > 1))) {
            echo "checked";
        }
        echo "\"></i></li>
                    <li class=\"\" id=\"2\"><i class=\"fa fa-star ";
        // line 259
        if (((twig_get_attribute($this->env, $this->source, ($context["showRating"] ?? null), "avg", [], "any", false, false, true, 259) == 2) || (twig_get_attribute($this->env, $this->source, ($context["showRating"] ?? null), "avg", [], "any", false, false, true, 259) > 2))) {
            echo "checked";
        }
        echo "\"></i></li>
                    <li class=\"\" id=\"3\"><i class=\"fa fa-star ";
        // line 260
        if (((twig_get_attribute($this->env, $this->source, ($context["showRating"] ?? null), "avg", [], "any", false, false, true, 260) == 3) || (twig_get_attribute($this->env, $this->source, ($context["showRating"] ?? null), "avg", [], "any", false, false, true, 260) > 3))) {
            echo "checked";
        }
        echo "\"></i></li>
                    <li class=\"\" id=\"4\"><i class=\"fa fa-star ";
        // line 261
        if (((twig_get_attribute($this->env, $this->source, ($context["showRating"] ?? null), "avg", [], "any", false, false, true, 261) == 4) || (twig_get_attribute($this->env, $this->source, ($context["showRating"] ?? null), "avg", [], "any", false, false, true, 261) > 4))) {
            echo "checked";
        }
        echo "\"></i></li>
                    <li class=\"\" id=\"5\"><i class=\"fa fa-star ";
        // line 262
        if (((twig_get_attribute($this->env, $this->source, ($context["showRating"] ?? null), "avg", [], "any", false, false, true, 262) == 5) || (twig_get_attribute($this->env, $this->source, ($context["showRating"] ?? null), "avg", [], "any", false, false, true, 262) > 5))) {
            echo "checked";
        }
        echo "\"></i></li>
              
                    <span style=\"display: inline;font-size: 18px\">
                      ";
        // line 265
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["showRating"] ?? null), "avg", [], "any", false, false, true, 265), 265, $this->source), "html", null, true);
        echo " out of 5
                    </span>
                  </ul>
                  <span style=\"font-size: 12px;margin-left:10px ;margin-top: 0px;\">
                    ";
        // line 269
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["showRating"] ?? null), "count", [], "any", false, false, true, 269), 269, $this->source), "html", null, true);
        echo " customer ratings
                  </span>
                </div>
                <div class=\"col-lg-10 col-md-10 col-xs-12\">
                  <!-- 5 -->
                  <div class=\"row\">
                    <div class=\"col-lg-1 col-md-1 col-xs-3\" style=\"padding: 0px 0px;\">
                      <p>5 star</p>
                    </div>
                    <div class=\"col-lg-3 col-md-3 col-xs-6\" >
                 
                      <div class=\"progress\"  style=\"margin-top: 5px;\">
                        <div class=\"progress-bar\" role=\"progressbar\" style=\"width:  ";
        // line 281
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["showRating"] ?? null), "five", [], "any", false, false, true, 281), 281, $this->source), "html", null, true);
        echo "%\" aria-valuenow=\"25\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>
                      </div>
                    </div>
                    <div class=\"col-lg-3 col-md-3 col-xs-3\">
                      <p>";
        // line 285
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["showRating"] ?? null), "five", [], "any", false, false, true, 285), 285, $this->source), "html", null, true);
        echo "%</p>
                      </div>
                      
                  </div>
                  <!-- 4 -->
                  <div class=\"row\">
                    <div class=\"col-lg-1 col-md-1 col-xs-3\" style=\"padding: 0px 0px;\">
                      <p>4 star</p>
                    </div>
                    <div class=\"col-lg-3 col-md-3 col-xs-6\" >
                 
                      <div class=\"progress\"  style=\"margin-top: 5px;\">
                        <div class=\"progress-bar\" role=\"progressbar\" style=\"width:  ";
        // line 297
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["showRating"] ?? null), "four", [], "any", false, false, true, 297), 297, $this->source), "html", null, true);
        echo "%\" aria-valuenow=\"25\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>
                      </div>
                    </div>
                    <div class=\"col-lg-3 col-md-3 col-xs-3\">
                      <p>";
        // line 301
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["showRating"] ?? null), "four", [], "any", false, false, true, 301), 301, $this->source), "html", null, true);
        echo "%</p>
                      </div>

                  </div>
                  <!-- 3 -->
                  <div class=\"row\">
                    <div class=\"col-lg-1 col-md-1 col-xs-3\" style=\"padding: 0px 0px;\">
                      <p>3 star</p>
                    </div>
                    <div class=\"col-lg-3 col-md-3 col-xs-6\" >
                 
                      <div class=\"progress\"  style=\"margin-top: 5px;\">
                        <div class=\"progress-bar\" role=\"progressbar\" style=\"width:  ";
        // line 313
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["showRating"] ?? null), "three", [], "any", false, false, true, 313), 313, $this->source), "html", null, true);
        echo "%\" aria-valuenow=\"25\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>
                      </div>
                    </div>
                    <div class=\"col-lg-3 col-md-3 col-xs-3\">
                      <p>";
        // line 317
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["showRating"] ?? null), "three", [], "any", false, false, true, 317), 317, $this->source), "html", null, true);
        echo "%</p>
                      </div>

                  </div>
                  <!-- 2 -->
                  <div class=\"row\">
                    <div class=\"col-lg-1 col-md-1 col-xs-3\" style=\"padding: 0px 0px;\">
                      <p>2 star</p>
                    </div>
                    <div class=\"col-lg-3 col-md-3 col-xs-6\" >
                 
                      <div class=\"progress\"  style=\"margin-top: 5px;\">
                        <div class=\"progress-bar\" role=\"progressbar\" style=\"width:  ";
        // line 329
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["showRating"] ?? null), "two", [], "any", false, false, true, 329), 329, $this->source), "html", null, true);
        echo "%\" aria-valuenow=\"25\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>
                      </div>
                    </div>
                    <div class=\"col-lg-3 col-md-3 col-xs-3\">
                      <p>";
        // line 333
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["showRating"] ?? null), "two", [], "any", false, false, true, 333), 333, $this->source), "html", null, true);
        echo "%</p>
                      </div>

                  </div>
                  <!-- 1 -->
                  <div class=\"row\">
                    <div class=\"col-lg-1 col-md-1 col-xs-3\" style=\"padding: 0px 0px;\">
                      <p>1 star</p>
                    </div>
                    <div class=\"col-lg-3 col-md-3 col-xs-6\" >
                 
                      <div class=\"progress\"  style=\"margin-top: 5px;\">
                        <div class=\"progress-bar\" role=\"progressbar\" style=\"width:  ";
        // line 345
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["showRating"] ?? null), "one", [], "any", false, false, true, 345), 345, $this->source), "html", null, true);
        echo "%\" aria-valuenow=\"25\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>
                      </div>
                    </div>
                    <div class=\"col-lg-3 col-md-3 col-xs-3\">
                      <p>";
        // line 349
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["showRating"] ?? null), "one", [], "any", false, false, true, 349), 349, $this->source), "html", null, true);
        echo "%</p>
                      </div>

                  </div>
                </div>

              </div>

  
      </div>
        </div>
      </div>


    </div>
    <!-- Reviews End -->
  </div>


    
\t\t
</div>
<script>
  function getFilterList(){
    setTimeout(function(){
      \$('.form-items').request('onFilterSearch')
    }),1000;
  }
  
  function getRating(){

    setTimeout(function(){
                    location = ''
                  },300)
    


  }
</script>";
    }

    public function getTemplateName()
    {
        return "/home/weslquicksite/public_html/weslnew/themes/WESL/pages/profileview.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  624 => 349,  617 => 345,  602 => 333,  595 => 329,  580 => 317,  573 => 313,  558 => 301,  551 => 297,  536 => 285,  529 => 281,  514 => 269,  507 => 265,  499 => 262,  493 => 261,  487 => 260,  481 => 259,  475 => 258,  444 => 230,  434 => 223,  430 => 222,  426 => 221,  399 => 197,  392 => 193,  381 => 184,  377 => 183,  373 => 182,  360 => 171,  349 => 169,  345 => 168,  335 => 161,  313 => 141,  308 => 140,  303 => 138,  289 => 126,  278 => 124,  274 => 123,  268 => 119,  257 => 117,  253 => 116,  240 => 106,  220 => 88,  215 => 86,  190 => 65,  188 => 64,  184 => 62,  179 => 61,  176 => 60,  170 => 59,  165 => 57,  162 => 56,  158 => 55,  144 => 44,  140 => 43,  133 => 41,  127 => 40,  121 => 36,  115 => 34,  112 => 33,  106 => 30,  102 => 28,  100 => 27,  81 => 13,  70 => 7,  62 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!-- Titlebar -->
<div class=\"parallax titlebar\" data-background=\"images/listings-parallax.jpg\" data-color=\"rgba(48, 48, 48, 1)\" data-color-opacity=\"0.8\" data-img-width=\"800\" data-img-height=\"505\">
    <div id=\"titlebar\">
      <div class=\"container\">
        <div class=\"row\">
          <div class=\"col-md-12\">
            <h2>{{ agent.agent.first_name }} {{ agent.agent.last_name }}</h2>
            <!-- Breadcrumbs -->
            <nav id=\"breadcrumbs\">
              <ul>
                <li><a href=\"../home\">Home</a></li>
                 <li><a href=\"../agentlist\">Agents</a></li>
                <li>{{ agent.agent.first_name }} {{ agent.agent.last_name }}</li>
              </ul>
            </nav>
          </div>
        </div>
      </div>
    </div>
</div>
  <!-- Content -->
  <div class=\"container\" id=\"Content\">
    <div class=\"row\">
      <div class=\"col-md-12\"> 
        <!-- Agency -->
        <div class=\"agent agents-profile agency margin-bottom-40\"> <a href=\"agency-profile.html\" class=\"utf-agent-avatar\">
          {% if agent.agent_user.avatar.path %}
  
          
          <img src=\"{{agent.agent_user.avatar.path}}\" alt=\"\">
        
        {% else %}
        
          <img src=\"{{'assets/img/avaterplaceholder.jpg' |theme}}\" alt=\"\">
         
        {% endif %} 
        </a>
          <div class=\"utf-agent-content\">
            <div class=\"utf-agent-name\">
              <h4><a href=\"agency-profile.html\">{{ agent.agent.first_name }} {{ agent.agent.last_name }} </a></h4>
\t\t\t  <span>Agent In {{agent.country.name}}  Town {{agent.city.name}}  </span>
            <ul class=\"utf-agent-contact-details\">
\t\t\t\t      <li><i class=\"icon-feather-smartphone\"></i>{{agent.agent.agent_contact}}</li>
\t\t\t\t      <li><i class=\"icon-material-outline-email\"></i><a href=\"#\">{{agent.agent.email}}</a></li>
\t\t\t      </ul> 
\t\t\t       <ul class=\"utf-social-icons\">
\t\t\t\t        <li><a class=\"facebook\" href=\"#\"><i class=\"icon-facebook\"></i></a></li>
\t\t\t\t        <li><a class=\"twitter\" href=\"#\"><i class=\"icon-twitter\"></i></a></li>
\t\t\t\t        <li><a class=\"linkedin\" href=\"#\"><i class=\"icon-linkedin\"></i></a></li>
\t\t\t\t        <li><a class=\"instagram\" href=\"#\"><i class=\"icon-instagram\"></i></a></li>
\t\t\t\t        <li><a class=\"gplus\" href=\"#\"><i class=\"icon-gplus\"></i></a></li>
\t\t\t       </ul>
      </div>
      
      {% partial 'service/servicelist' %}
\t\t\t<div class=\"clearfix\"></div>
            <p>{{agent.agent.description}}</p>
            <div>
              Avg. Rating {% partial 'agents/star' star=avgRating hide=hideRating %}
            </div>
            {% partial 'agents/rating' permission=ratingPermission%}
            <!-- Rating Form -->
            
            {% if  ratingPermission==0 %}
                {{ form_ajax('onChange', { class: 'rating', update: { 'agents/ratingResponse': '#rating'}})}}
                <fieldset class=\"rating\">
                    <input type=\"radio\" id=\"star5\" name=\"rating\" value=\"5\" /><label class = \"full\" for=\"star5\" title=\"Awesome - 5 stars\"></label>
                    
                    <input type=\"radio\" id=\"star4\" name=\"rating\" value=\"4\" /><label class = \"full\" for=\"star4\" title=\"Pretty good - 4 stars\"></label>
                    
                    <input type=\"radio\" id=\"star3\" name=\"rating\" value=\"3\" /><label class = \"full\" for=\"star3\" title=\"Meh - 3 stars\"></label>
                    
                    <input type=\"radio\" id=\"star2\" name=\"rating\" value=\"2\" /><label class = \"full\" for=\"star2\" title=\"Kinda bad - 2 stars\"></label>
                    
                    <input type=\"radio\" id=\"star1\" name=\"rating\" value=\"1\" /><label class = \"full\" for=\"star1\" title=\"Sucks big time - 1 star\"></label>
                    
                </fieldset>
                <br>
                <p style=\"margin-right: 75%;\">Comments</p>
                <!-- <input type=\"text\" name=\"comment\"> -->
                <textarea name=\"comment\" id=\"comment\" cols=\"50\" rows=\"1\"></textarea>
                <button data-request=\"onSubmit\"
                        data-request-update=\"'agents/ratingResponse' : '#rating'\"
                        onclick=\"getRating()\"
                >Submit</button>
                {{ form_close()}}
            {% endif %}
          </div>
        </div>
\t  </div>
    </div>
  </div>
  
  <!-- Content -->
  <div class=\"container\">
    <!-- Currency Calculator -->
       <div class=\"row sticky-wrapper col-xs-12 col-md-12\">
      <div class=\"col-lg-12 col-md-12 col-xs-12\">
        <div class=\"\">
                  
        <div class=\"utf-desc-headline-item margin-top-0\">
          <h3><i class=\"icon-material-outline-description\"></i> Currency Calculator</h3>
      </div>
        <!-- Form goes here -->
        <div class=\"col-lg-12 col-md-12 col-xs-12\">
        {{form_ajax('onSearch',{ class: 'currency', update: { 'currency/calc': '#calc'}}) }}
       
          <div class=\"col-lg-2 col-md-2 col-xs-12\">
            <label style=\"text-align: center;\" >Amount</label>
            <input type=\"number\" name=\"amount\" placeholder=\"Enter Amount\">
          </div>
          <div class=\"col-lg-2 col-md-2 col-xs-12\">
            <label  style=\"text-align: center;\">From</label>
            <select name=\"from\">

            {% for key, value in calcurrency %}
            <option value=\"{{ key }}\">{{ value }}</option>
              {% endfor %}
          </select></div>
          <div class=\"col-lg-2 col-md-2 col-xs-12\">
            <label style=\"text-align: center;\" >To</label>
            <select name=\"to\">
              {% for  key, value in calcurrency %}
              <option value=\"{{ key }}\">{{ value }}</option>
              {% endfor %}
            </select>
          </div>
          <div class=\"col-lg-2 col-md-2 col-xs-12\">
            <button class=\"button utf-search-btn-item\"
            id=\"calcButton\"
            type=\"submit\" 
            data-request=\"onSearch\"
            data-request-update=\"'currency/calc' : '#calc'\"
            style=\"width: 100%; margin-left: 0% !important;\"
            >Calculate</button>
          </div>
      
        {{ form_close()}}
          <div class=\"col-lg-4 col-md-4 col-xs-12\">
            {% partial 'currency/calc'  calcresult=calcresult %}
          </div>

      </div>
        <!-- Partial here -->




        </div>
        </div>

    </div>
    <!-- Currency Rates -->
    <div class=\"row sticky-wrapper col-xs-12 col-md-12\">
      <div class=\"col-lg-9 col-md-9 col-xs-12\">
        <div class=\"\">
          <div class=\"utf-desc-headline-item margin-top-0\">
              <h3><i class=\"icon-material-outline-description\"></i> My Currency Rates</h3>
          </div>
          <!-- Sorting -->
          {{form_ajax('onFilterSearch',{ class: 'form-items', update: { 'currency/currencytable': '#currency_list'}}) }}
            <div class=\"utf-sort-box-aera\">
              <div class=\"sort-by\">
                <label>From:</label>
                <div class=\"sort-by-select\">
                  <select data-placeholder=\"Default Properties\" onchange=\"getFilterList()\" class=\"utf-chosen-select-single-item\" name=\"currency_id\">
                    <option>Select Base Currency</option>
                    {% for currency in baseCurrency %}
                    <option value=\"{{ currency.id}}\">{{ currency.name}}</option>
                    {% endfor %}
                  </select>
                </div>
              </div>
              <!-- <div class=\"utf-layout-switcher\"> 
                  <a href=\"#\" class=\"list\"><i class=\"sl sl-icon-list\"></i></a> 
                  <a href=\"#\" class=\"grid\"><i class=\"sl sl-icon-grid\"></i></a> 
              </div> -->
            </div>
          
          <!-- Listings -->
        </div>
        {{ form_close()}}
        {% partial 'currency/currencytable' %}
      </div>
      <!-- Video -->

        <div class=\"col-md-3 col-lg-3 col-xs-12\">

          <div class=\"row\">
            <i class=\"fas fa-ad\" style=\"font-size: 45px;\"></i>
            <div class=\"media-wrapper\" id=\"video1\">
              <div id=\"overlay\">
                <div id=\"textSuccess\">You earned {{ video.point }} points for the video</div>
                <div id=\"textFail\">You already earned maximum points for the video</div>
              </div>
              <div class=\"media-player\">
                 <video src=\"..{{ video.path | media}}\" id=\"media-video\"></video>
              </div>
              <div id='media-controls'>
                  <!-- <button id='play-pause-button' class='button play' title='play'
                  onclick='togglePlayPause();' >Play</button> -->
                  <div class=\"row\">        
                      <div class=\"hideButton\">
                      <button class=\"ad\" id=\"playButton\" onclick='togglePlayPause();'>
                        <i class=\"fas fa-play-circle\"></i>
                      </button>
                      <button id='mute-button' class='mute' title='mute'
                      onclick='toggleMute();' value=\"false\"><i class=\"fas fa-volume-mute\"></i></button>
         
                      <div style=\"padding-top: 5px;\">
                        <button type=\"submit\" id=\"proceed\" style=\"display: none;\" onclick=\"addPoint()\">Skip</button>
                      </div>

                  </div>
                  
                  </div>
                  
                  
               </div>
          </div>
          <input type=\"hidden\" name=\"id\" id=\"getId\" value=\"{{ video.id}}\" name=\"videoId\">
          <input type=\"hidden\" id=\"getpoint\"  value=\"{{ video.point }}\">
          <input type=\"hidden\" id=\"gettime\" value=\"{{ video.time }}\">
          <input type=\"hidden\" id=\"point\" name=\"finalPoints\">
            <!-- <span>
              <img src=\"https://via.placeholder.com/400x350?text=Video+Placeholder\" alt=\"\" width=\"100%\">
            </span> -->
          </div>
        </div>
        <script src=\"{{'assets/js/mediaPlayer.js'|theme}}\"></script>









            
          
          
    </div>\t<!-- Listing Item -->

    <!-- Reviews -->
    <div class=\"row sticky-wrapper col-xs-12 col-md-12\">
      <div class=\"col-lg-12 col-md-12 col-xs-12\">
        <div class=\"\">
          <div class=\"utf-desc-headline-item margin-top-0\">
            <h3><i class=\"icon-material-outline-description\"></i> Reviews</h3>
        </div>
                    <div class=\"col-lg-11 col-md-11 col-xs-12\">

      
          <div class=\"containerCard\" style=\"line-height: 20px !important;\">
                <div class=\"row\">
                  <ul  id=\"menu\" style=\"list-style-type: none;margin-bottom: 0px;\"> 
                    <li class=\"\" id=\"1\"><i class=\"fa fa-star {% if showRating.avg ==1 or showRating.avg > 1  %}checked{% endif %}\"></i></li>
                    <li class=\"\" id=\"2\"><i class=\"fa fa-star {% if showRating.avg ==2 or showRating.avg > 2  %}checked{% endif %}\"></i></li>
                    <li class=\"\" id=\"3\"><i class=\"fa fa-star {% if showRating.avg ==3 or showRating.avg > 3  %}checked{% endif %}\"></i></li>
                    <li class=\"\" id=\"4\"><i class=\"fa fa-star {% if showRating.avg ==4 or showRating.avg > 4  %}checked{% endif %}\"></i></li>
                    <li class=\"\" id=\"5\"><i class=\"fa fa-star {% if showRating.avg ==5 or showRating.avg > 5  %}checked{% endif %}\"></i></li>
              
                    <span style=\"display: inline;font-size: 18px\">
                      {{ showRating.avg }} out of 5
                    </span>
                  </ul>
                  <span style=\"font-size: 12px;margin-left:10px ;margin-top: 0px;\">
                    {{ showRating.count }} customer ratings
                  </span>
                </div>
                <div class=\"col-lg-10 col-md-10 col-xs-12\">
                  <!-- 5 -->
                  <div class=\"row\">
                    <div class=\"col-lg-1 col-md-1 col-xs-3\" style=\"padding: 0px 0px;\">
                      <p>5 star</p>
                    </div>
                    <div class=\"col-lg-3 col-md-3 col-xs-6\" >
                 
                      <div class=\"progress\"  style=\"margin-top: 5px;\">
                        <div class=\"progress-bar\" role=\"progressbar\" style=\"width:  {{ showRating.five }}%\" aria-valuenow=\"25\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>
                      </div>
                    </div>
                    <div class=\"col-lg-3 col-md-3 col-xs-3\">
                      <p>{{ showRating.five }}%</p>
                      </div>
                      
                  </div>
                  <!-- 4 -->
                  <div class=\"row\">
                    <div class=\"col-lg-1 col-md-1 col-xs-3\" style=\"padding: 0px 0px;\">
                      <p>4 star</p>
                    </div>
                    <div class=\"col-lg-3 col-md-3 col-xs-6\" >
                 
                      <div class=\"progress\"  style=\"margin-top: 5px;\">
                        <div class=\"progress-bar\" role=\"progressbar\" style=\"width:  {{ showRating.four }}%\" aria-valuenow=\"25\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>
                      </div>
                    </div>
                    <div class=\"col-lg-3 col-md-3 col-xs-3\">
                      <p>{{ showRating.four }}%</p>
                      </div>

                  </div>
                  <!-- 3 -->
                  <div class=\"row\">
                    <div class=\"col-lg-1 col-md-1 col-xs-3\" style=\"padding: 0px 0px;\">
                      <p>3 star</p>
                    </div>
                    <div class=\"col-lg-3 col-md-3 col-xs-6\" >
                 
                      <div class=\"progress\"  style=\"margin-top: 5px;\">
                        <div class=\"progress-bar\" role=\"progressbar\" style=\"width:  {{ showRating.three }}%\" aria-valuenow=\"25\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>
                      </div>
                    </div>
                    <div class=\"col-lg-3 col-md-3 col-xs-3\">
                      <p>{{ showRating.three }}%</p>
                      </div>

                  </div>
                  <!-- 2 -->
                  <div class=\"row\">
                    <div class=\"col-lg-1 col-md-1 col-xs-3\" style=\"padding: 0px 0px;\">
                      <p>2 star</p>
                    </div>
                    <div class=\"col-lg-3 col-md-3 col-xs-6\" >
                 
                      <div class=\"progress\"  style=\"margin-top: 5px;\">
                        <div class=\"progress-bar\" role=\"progressbar\" style=\"width:  {{ showRating.two }}%\" aria-valuenow=\"25\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>
                      </div>
                    </div>
                    <div class=\"col-lg-3 col-md-3 col-xs-3\">
                      <p>{{ showRating.two }}%</p>
                      </div>

                  </div>
                  <!-- 1 -->
                  <div class=\"row\">
                    <div class=\"col-lg-1 col-md-1 col-xs-3\" style=\"padding: 0px 0px;\">
                      <p>1 star</p>
                    </div>
                    <div class=\"col-lg-3 col-md-3 col-xs-6\" >
                 
                      <div class=\"progress\"  style=\"margin-top: 5px;\">
                        <div class=\"progress-bar\" role=\"progressbar\" style=\"width:  {{ showRating.one }}%\" aria-valuenow=\"25\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>
                      </div>
                    </div>
                    <div class=\"col-lg-3 col-md-3 col-xs-3\">
                      <p>{{ showRating.one }}%</p>
                      </div>

                  </div>
                </div>

              </div>

  
      </div>
        </div>
      </div>


    </div>
    <!-- Reviews End -->
  </div>


    
\t\t
</div>
<script>
  function getFilterList(){
    setTimeout(function(){
      \$('.form-items').request('onFilterSearch')
    }),1000;
  }
  
  function getRating(){

    setTimeout(function(){
                    location = ''
                  },300)
    


  }
</script>", "/home/weslquicksite/public_html/weslnew/themes/WESL/pages/profileview.htm", "");
    }
}
