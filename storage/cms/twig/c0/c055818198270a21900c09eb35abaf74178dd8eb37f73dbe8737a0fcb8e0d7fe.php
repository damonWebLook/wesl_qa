<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /home2/demowesl/public_html/weslnew2/themes/WESL/pages/userprofile.htm */
class __TwigTemplate_c25824add202d479ead88eedf79b629bc514727ec9e7c517c5de745403d88593 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = array("for" => 74);
        $filters = array("page" => 31, "escape" => 54);
        $functions = array();

        try {
            $this->sandbox->checkSecurity(
                ['for'],
                ['page', 'escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<style>
  input:disabled {
    background: #dddddd;
  }
</style>

<div class=\"clearfix\"></div>
<!-- Header Container / End --> 

<!-- Titlebar -->


<!-- Content -->
<div class=\"container\">
  <div class=\"row\"> 
    <!-- Widget -->
    <div class=\"col-md-3\">
      <div class=\"margin-bottom-20 \"> 
          <!-- <div class=\"utf-edit-profile-photo-area\"> <img src=\"#\" alt=\"\"> -->
            <!-- <div class=\"utf-change-photo-btn-item\">
              <div class=\"utf-user-photo-upload\"> <span><i class=\"fa fa-upload\"></i> Upload Photo</span>
                <input type=\"file\" class=\"upload tooltip top\" title=\"Upload Photo\" />
              </div>
            </div> -->
          <!-- </div> -->
      </div>
      <div class=\"clearfix\"></div>
      <div class=\"sidebar margin-top-20\">
        <div class=\"user-smt-account-menu-container\">
          <ul class=\"user-account-nav-menu\">
            <li><a href=\"";
        // line 31
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("userdashboard");
        echo "\"><i class=\"sl sl-icon-user\"></i> My Dashboard</a></li>
            <li><a href=\"";
        // line 32
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("userprofile");
        echo "\"><i class=\"sl sl-icon-user\"></i> My Profile</a></li>
            <li><a href=\"";
        // line 33
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("passwordchange");
        echo "\"><i class=\"sl sl-icon-user\"></i> Change Password</a></li>
           
              <li><a data-request=\"onLogOut\"><i class=\"sl sl-icon-power\"></i>Log Out</a></li>
        
            
          </ul>            
        </div>
      </div>
      <div class=\"widget utf-sidebar-widget-item\">

      </div>
    </div>
    <div class=\"col-md-9\">
        <div class=\"utf-user-profile-item userpanel\">
          <div class=\"utf-submit-page-inner-box\">
              <h3>My Profile</h3>
              <form data-request=\"onSaveProfile\">
              
               <div class=\"content with-padding\">
                  <div class=\"col-md-6\" >
                      <label>Your Name</label>
                      <input name=\"login\" value=\"";
        // line 54
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "login", [], "any", false, false, true, 54), 54, $this->source), "html", null, true);
        echo "\" type=\"text\" disabled>
                  </div> 
                  <div class=\"col-md-6\">\t
                      <label>Email Address</label>
                     <input name=\"email\" value=\"";
        // line 58
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "email", [], "any", false, false, true, 58), 58, $this->source), "html", null, true);
        echo "\" type=\"Email\">
                  </div>
                  <div class=\"col-md-6\">\t
                     <label>DOB</label>
                     <input name=\"birth\" value=\"";
        // line 62
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "birth", [], "any", false, false, true, 62), 62, $this->source), "html", null, true);
        echo "\" type=\"date\">
                  </div>
                  
                  <div class=\"col-md-6\">
                    <label>Contact Number</label>
                    <input name=\"contactno\" value=\"";
        // line 67
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "contactno", [], "any", false, false, true, 67), 67, $this->source), "html", null, true);
        echo "\" type=\"text\">
                  </div>
                  
                  <div class=\"col-md-6\">
                    <label>Country</label>
                    <select name=\"country\" value=\"";
        // line 72
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "country", [], "any", false, false, true, 72), 72, $this->source), "html", null, true);
        echo "\" type=\"text\" >
                      <option>ITALY</option>
                      ";
        // line 74
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($context["country"]);
        foreach ($context['_seq'] as $context["_key"] => $context["country"]) {
            // line 75
            echo "                        <option value=\"";
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["country"], "name", [], "any", false, false, true, 75), 75, $this->source), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["country"], "name", [], "any", false, false, true, 75), 75, $this->source), "html", null, true);
            echo "</option>
                      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['country'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 76
        echo "   
                      
                      
                    </select>
                  </div>
                  
                
                  
                 
             
                
              </div>
              
             <div class=\"row\">
                <div class=\"col-md-12\">
                    <button class=\"utf-centered-button button margin-top-0 margin-bottom-20\">Save Changes</button>
                </div>
             </div>
              
              
              </form>
             \t\t
          </div>
          <!-- <div class=\"utf-submit-page-inner-box\">
              <h3>Social Accounts</h3>
              <div class=\"content with-padding\">
                  <div class=\"col-md-6\">
                      <label><i class=\"icon-brand-facebook\"></i> Facebook</label>
                      <input value=\"\" type=\"text\">
                  </div>
                  <div class=\"col-md-6\">
                      <label><i class=\"icon-brand-twitter\"></i> Twitter</label>
                      <input value=\"\" type=\"text\">
                  </div>
                  <div class=\"col-md-6\">
                      <label><i class=\"icon-brand-linkedin\"></i> Linkedin</label>
                      <input value=\"\" type=\"text\">
                  </div>
                  <div class=\"col-md-6\">
                      <label><i class=\"icon-brand-google-plus-g\"></i> Google+</label>
                      <input value=\"\" type=\"text\">\t
                  </div>
                  <div class=\"col-md-6\">
                      <label><i class=\"icon-brand-pinterest\"></i> Pinterest</label>
                      <input value=\"\" type=\"text\">\t
                  </div>
                  <div class=\"col-md-6\">
                      <label><i class=\"icon-feather-instagram\"></i> Instagram</label>
                      <input value=\"\" type=\"text\">\t
                  </div>
              </div>\t\t\t\t\t
          </div>\t -->
         
        </div>          
    </div>
  </div>
</div>";
    }

    public function getTemplateName()
    {
        return "/home2/demowesl/public_html/weslnew2/themes/WESL/pages/userprofile.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  176 => 76,  165 => 75,  161 => 74,  156 => 72,  148 => 67,  140 => 62,  133 => 58,  126 => 54,  102 => 33,  98 => 32,  94 => 31,  62 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<style>
  input:disabled {
    background: #dddddd;
  }
</style>

<div class=\"clearfix\"></div>
<!-- Header Container / End --> 

<!-- Titlebar -->


<!-- Content -->
<div class=\"container\">
  <div class=\"row\"> 
    <!-- Widget -->
    <div class=\"col-md-3\">
      <div class=\"margin-bottom-20 \"> 
          <!-- <div class=\"utf-edit-profile-photo-area\"> <img src=\"#\" alt=\"\"> -->
            <!-- <div class=\"utf-change-photo-btn-item\">
              <div class=\"utf-user-photo-upload\"> <span><i class=\"fa fa-upload\"></i> Upload Photo</span>
                <input type=\"file\" class=\"upload tooltip top\" title=\"Upload Photo\" />
              </div>
            </div> -->
          <!-- </div> -->
      </div>
      <div class=\"clearfix\"></div>
      <div class=\"sidebar margin-top-20\">
        <div class=\"user-smt-account-menu-container\">
          <ul class=\"user-account-nav-menu\">
            <li><a href=\"{{'userdashboard'|page}}\"><i class=\"sl sl-icon-user\"></i> My Dashboard</a></li>
            <li><a href=\"{{'userprofile'|page}}\"><i class=\"sl sl-icon-user\"></i> My Profile</a></li>
            <li><a href=\"{{'passwordchange'|page}}\"><i class=\"sl sl-icon-user\"></i> Change Password</a></li>
           
              <li><a data-request=\"onLogOut\"><i class=\"sl sl-icon-power\"></i>Log Out</a></li>
        
            
          </ul>            
        </div>
      </div>
      <div class=\"widget utf-sidebar-widget-item\">

      </div>
    </div>
    <div class=\"col-md-9\">
        <div class=\"utf-user-profile-item userpanel\">
          <div class=\"utf-submit-page-inner-box\">
              <h3>My Profile</h3>
              <form data-request=\"onSaveProfile\">
              
               <div class=\"content with-padding\">
                  <div class=\"col-md-6\" >
                      <label>Your Name</label>
                      <input name=\"login\" value=\"{{user.login}}\" type=\"text\" disabled>
                  </div> 
                  <div class=\"col-md-6\">\t
                      <label>Email Address</label>
                     <input name=\"email\" value=\"{{user.email}}\" type=\"Email\">
                  </div>
                  <div class=\"col-md-6\">\t
                     <label>DOB</label>
                     <input name=\"birth\" value=\"{{user.birth}}\" type=\"date\">
                  </div>
                  
                  <div class=\"col-md-6\">
                    <label>Contact Number</label>
                    <input name=\"contactno\" value=\"{{user.contactno}}\" type=\"text\">
                  </div>
                  
                  <div class=\"col-md-6\">
                    <label>Country</label>
                    <select name=\"country\" value=\"{{user.country}}\" type=\"text\" >
                      <option>ITALY</option>
                      {% for country in country %}
                        <option value=\"{{country.name}}\">{{country.name}}</option>
                      {% endfor %}   
                      
                      
                    </select>
                  </div>
                  
                
                  
                 
             
                
              </div>
              
             <div class=\"row\">
                <div class=\"col-md-12\">
                    <button class=\"utf-centered-button button margin-top-0 margin-bottom-20\">Save Changes</button>
                </div>
             </div>
              
              
              </form>
             \t\t
          </div>
          <!-- <div class=\"utf-submit-page-inner-box\">
              <h3>Social Accounts</h3>
              <div class=\"content with-padding\">
                  <div class=\"col-md-6\">
                      <label><i class=\"icon-brand-facebook\"></i> Facebook</label>
                      <input value=\"\" type=\"text\">
                  </div>
                  <div class=\"col-md-6\">
                      <label><i class=\"icon-brand-twitter\"></i> Twitter</label>
                      <input value=\"\" type=\"text\">
                  </div>
                  <div class=\"col-md-6\">
                      <label><i class=\"icon-brand-linkedin\"></i> Linkedin</label>
                      <input value=\"\" type=\"text\">
                  </div>
                  <div class=\"col-md-6\">
                      <label><i class=\"icon-brand-google-plus-g\"></i> Google+</label>
                      <input value=\"\" type=\"text\">\t
                  </div>
                  <div class=\"col-md-6\">
                      <label><i class=\"icon-brand-pinterest\"></i> Pinterest</label>
                      <input value=\"\" type=\"text\">\t
                  </div>
                  <div class=\"col-md-6\">
                      <label><i class=\"icon-feather-instagram\"></i> Instagram</label>
                      <input value=\"\" type=\"text\">\t
                  </div>
              </div>\t\t\t\t\t
          </div>\t -->
         
        </div>          
    </div>
  </div>
</div>", "/home2/demowesl/public_html/weslnew2/themes/WESL/pages/userprofile.htm", "");
    }
}
