<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /home2/demowesl/public_html/weslnew2/themes/WESL/pages/userdashboard.htm */
class __TwigTemplate_4483e18356a57775d26e4afca710a246df742ca0ed4037509cee23c8979d04dc extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = array();
        $filters = array("page" => 19, "escape" => 31);
        $functions = array();

        try {
            $this->sandbox->checkSecurity(
                [],
                ['page', 'escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!-- Content -->
<div class=\"container\">
    <div class=\"row\"> 
      <!-- Widget -->
      <div class=\"col-md-3\">
\t    <div class=\"margin-bottom-20\"> 
\t\t\t<!-- <div class=\"utf-edit-profile-photo-area\"> <img src=\"images/agent-02.jpg\" alt=\"\">
\t\t\t   <div class=\"utf-change-photo-btn-item\">
\t\t\t\t<div class=\"utf-user-photo-upload\"> <span><i class=\"fa fa-upload\"></i> Upload Photo</span>
\t\t\t\t  <input type=\"file\" class=\"upload tooltip top\" title=\"Upload Photo\" />
\t\t\t\t</div>
\t\t\t  </div> -->
\t\t\t<!-- </div>  -->
\t    </div>
\t\t<div class=\"clearfix\"></div>
        <div class=\"sidebar margin-top-20\">
          <div class=\"user-smt-account-menu-container\">
            <ul class=\"user-account-nav-menu\">
                <li><a href=\"";
        // line 19
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("userdashboard");
        echo "\"><i class=\"sl sl-icon-user\"></i> My Dashboard</a></li>
                <li><a href=\"";
        // line 20
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("userprofile");
        echo "\"><i class=\"sl sl-icon-user\"></i> My Profile</a></li>
                <li><a href=\"";
        // line 21
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("passwordchange");
        echo "\"><i class=\"sl sl-icon-user\"></i> Change Password</a></li> 
                <li><a data-request=\"onLogOut\"><i class=\"sl sl-icon-power\"></i> Log Out</a></li>
            </ul>
          </div>
        </div>
\t    
      </div>
      <div class=\"col-md-9 userpanel\">
        <div class=\"container\">
            <h3>My Points</h3>
            <h5> ";
        // line 31
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "point", [], "any", false, false, true, 31), 31, $this->source), "html", null, true);
        echo "</h5>
            
        </div>
        <div class=\"container\">
          <h3> Username </h3><p>";
        // line 35
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["user"] ?? null), "login", [], "any", false, false, true, 35), 35, $this->source), "html", null, true);
        echo "</p>
          
          
      </div>
       
      </div>
    </div>
  </div>";
    }

    public function getTemplateName()
    {
        return "/home2/demowesl/public_html/weslnew2/themes/WESL/pages/userdashboard.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  110 => 35,  103 => 31,  90 => 21,  86 => 20,  82 => 19,  62 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "/home2/demowesl/public_html/weslnew2/themes/WESL/pages/userdashboard.htm", "");
    }
}
