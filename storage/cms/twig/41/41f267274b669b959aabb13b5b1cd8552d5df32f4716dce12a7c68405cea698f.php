<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /home2/demowesl/public_html/weslnew2/themes/WESL/pages/contact.htm */
class __TwigTemplate_d3ead72302528cc433f78a9d1d5a59339c46d4b03b031ea6595deab4e104239e extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = array();
        $filters = array();
        $functions = array();

        try {
            $this->sandbox->checkSecurity(
                [],
                [],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"clearfix\"></div>
<!-- Header Container / End --> 

<!-- Titlebar -->


<!-- Container / Start -->
<div class=\"container\">
  <div class=\"row\">  
    <div class=\"col-md-12\">
      <div class=\"utf-contact-map margin-bottom-50\">
        <div class=\"utf-google-map-container\">
            <div id=\"propertyMap\" data-latitude=\"48.8566\" data-longitude=\"2.3522\" data-map-icon=\"im im-icon-Hamburger\"></div>
            <a href=\"#\" id=\"streetView\">Street View</a> 
        </div>
      </div>
    </div>\t
    <!-- Contact Details -->
    <div class=\"col-md-4\">
      <div class=\"utf-boxed-list-headline-item\">
          <h3><i class=\"icon-feather-map\"></i> Office Address</h3>
      </div>
      <!-- Contact Details -->
      <div class=\"utf-contact-location-info-aera sidebar-textbox margin-bottom-40\">
        <ul class=\"contact-details\">
          <li><i class=\"icon-feather-smartphone\"></i> <strong>Phone Number:</strong> <span>(+21) 124 123 4546</span></li>
          <li><i class=\"icon-material-outline-email\"></i> <strong>Email Address:</strong> <span><a href=\"#\">info@example.com</a></span></li>
          <li><i class=\"icon-feather-globe\"></i> <strong>Website:</strong> <span>www.example.com</span></li>
          <li><i class=\"icon-feather-map-pin\"></i> <strong>Address:</strong> <span>3241, Lorem ipsum dolor sit amet, consectetur adipiscing elit Proin fermentum condimentum mauris.</span></li>            
        </ul>
      </div>
    </div>
    
    <!-- Contact Form -->
    <div class=\"col-md-8\">
      <section id=\"contact\">
        <div class=\"utf-boxed-list-headline-item\">
          <h3><i class=\"icon-feather-layers\"></i> Contact Form</h3>
        </div>
        <div class=\"utf-contact-form-item\">
            <form>
              <div class=\"row\">
                <div class=\"col-md-6\">
                    <input name=\"name\" type=\"text\" placeholder=\"Frist Name\" required />                
                </div>
                <div class=\"col-md-6\">
                    <input name=\"name\" type=\"text\" placeholder=\"Last Name\" required />                
                </div>
                <div class=\"col-md-6\">
                    <input name=\"email\" type=\"email\" placeholder=\"Email Address\" required />                
                </div>
                <div class=\"col-md-6\">
                    <input name=\"name\" type=\"text\" placeholder=\"Subject\" required />                
                </div>
                <div class=\"col-md-12\">
                    <textarea name=\"comments\" cols=\"40\" rows=\"3\" placeholder=\"Message...\" spellcheck=\"true\" required></textarea>
                </div>
              </div>
              <div class=\"utf-centered-button margin-bottom-10\">\t
                  <input type=\"submit\" class=\"submit button\" id=\"submit\" value=\"Submit Message\" />
              </div>
            </form>
          </div>\t  
      </section>
    </div>
    <!-- Contact Form / End -->       
  </div>
</div>
<!-- Container / End -->";
    }

    public function getTemplateName()
    {
        return "/home2/demowesl/public_html/weslnew2/themes/WESL/pages/contact.htm";
    }

    public function getDebugInfo()
    {
        return array (  62 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "/home2/demowesl/public_html/weslnew2/themes/WESL/pages/contact.htm", "");
    }
}
