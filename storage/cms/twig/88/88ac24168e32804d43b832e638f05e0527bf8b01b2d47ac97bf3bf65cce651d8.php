<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /home2/demowesl/public_html/weslnew2/themes/WESL/pages/passwordchange.htm */
class __TwigTemplate_bd1739d970594b678e17bc3024526198f54a67e6ae837074e2dd265777e6bd03 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = array();
        $filters = array("page" => 19);
        $functions = array();

        try {
            $this->sandbox->checkSecurity(
                [],
                ['page'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"clearfix\"></div>
<!-- Header Container / End --> 

<!-- Titlebar -->


<!-- Content -->
<div class=\"container\">
  <div class=\"row\"> 
    <!-- Widget -->
    <div class=\"col-md-3\">
      <div class=\"margin-bottom-20\"> 
          
      </div>
      <div class=\"clearfix\"></div>
      <div class=\"sidebar margin-top-20\">
        <div class=\"user-smt-account-menu-container\">
          <ul class=\"user-account-nav-menu\">
            <li><a href=\"";
        // line 19
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("userdashboard");
        echo "\"><i class=\"sl sl-icon-user\"></i> My Dashboard</a></li>
            <li><a href=\"";
        // line 20
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("userprofile");
        echo "\"><i class=\"sl sl-icon-user\"></i> My Profile</a></li>
            <li><a href=\"";
        // line 21
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("passwordchange");
        echo "\"><i class=\"sl sl-icon-user\"></i> Change Password</a></li>
            <li><a data-request=\"onLogOut\"><i class=\"sl sl-icon-power\"></i> Log Out</a></li>
          </ul>
        </div>
      </div>
      <div class=\"widget utf-sidebar-widget-item\">
                
      </div>
    </div>
    <div class=\"col-md-9\">
     <form data-request=\"onPassword\">
        <div class=\"utf-user-profile-item userpanel\">
            <div class=\"utf-submit-page-inner-box\">
              <h3>Change Password</h3>
              <div class=\"content with-padding\">
    
                 
                  <div class=\"col-md-4\">
                      <label>New Password</label>
                      <input name=\"password\" type=\"password\" placeholder=\"*********\">
                  </div>
                  <div class=\"col-md-4\">
                      <label>Confirm New Password</label>
                      <input name=\"confirm_password\" type=\"password\" placeholder=\"*********\">
                  </div>
              </div>\t\t\t
            </div>  
            <div class=\"row\">
              <div class=\"col-md-12\">
                  <button type=\"submit\" class=\"utf-centered-button button\">Save Changes</button>
              </div>
            </div>\t
          </div>

        
     </form>
     
    </div>
  </div>
</div>";
    }

    public function getTemplateName()
    {
        return "/home2/demowesl/public_html/weslnew2/themes/WESL/pages/passwordchange.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 21,  86 => 20,  82 => 19,  62 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "/home2/demowesl/public_html/weslnew2/themes/WESL/pages/passwordchange.htm", "");
    }
}
